**PROGETTO:**
ITS Kennedy Gestione Corsi


**Membri Scrum Team:**
Fichera Marco
Pastorino Mirco
Solivo Matteo
Zogno Stefano (Scrum Master)

**Linguaggi per la cura del Frontend:**
HTML
SCSS
TypeScript
JavaScript

**Linguaggi Backend e DB utilizzati:**
Java
Mysql
TSql
Hibernate

**Toolkit utilizzati**
Bootstrap
PrimeNG

**Descrizione generale strumenti utilizzati:**
Per lo sviluppo del codice sorgente del progetto, abbiamo utilizzato il framework Angular dedicato alla creazione dell’interfaccia utente (lato Frontend) e SpringBoot per il lato Backend.
Il database che abbiamo utilizzato come IDE è stato Dbeaver, mentre Docker lo abbiamo utilizzato come applicazione per facilitare il deployment all’interno di un contenitore software.
Per la comunicazione del team abbiamo utilizzato l’applicazione Discord, in modo da svolgere i meeting tutti assieme, mentre per la condivisione dei file abbiamo utilizzato Google Drive e GitLab per lo storaging del Progetto.
Inizialmente abbiamo analizzato il lavoro e le richieste che ci erano state sottoposte per poi suddividerle in Milestone e, a loro volta, in Issues, in modo da rendere più facile lo sviluppo del progetto e la suddivisione delle mansioni.

**AVVIO APPLICAZIONE:**

Link GitLab:
SSH: git@gitlab.com:itskennedymmmm/itskennedymmmm.git

HTTPS: https://gitlab.com/itskennedymmmm/itskennedymmmm.git

Passo 1:

Dentro cartella presente nel branch: jarAvvio

Apro la powershell e digito il comando: java -jar .\Project-Work-0.0.1-SNAPSHOT.jar

Passo 2:

Dentro cartella Docker del progetto, eseguire il cmd e digitare:

docker-compose up -d

**AVVIO APPLICAZIONE DA SVILUPPATORE:**

Link GitLab:

SSH: git@gitlab.com:itskennedymmmm/itskennedymmmm.git

HTTPS: https://gitlab.com/itskennedymmmm/itskennedymmmm.git

Cartella Docker:

Dentro cartella Docker del progetto, eseguire il cmd e digitare:
docker-compose up -d
Cartella Angular:
Aprire la cartella Angular con un IDE e sul terminale(Ctrl + o)

npm i (installazione node_modules)
npm start (Avvio progetto)

Cartella Spring-frontend:

Aprire con un IDE e importare il progetto
Installare le dipendenze Maven
Avviare l’applicazione come: Springboot Application
Aprire il Browser con URL http://localhost:4200/#/dashboard

**FEATURES IMPLEMENTATE:**
*DOCENTI:*

Visualizza Tabella con elenco Docenti (Tabella suddivisa in Interni & Esterni)
Inserimento Docente
Modifica Docente
Elimina Docente
Possibilità di consultazione per informazioni aggiuntive con l’Expanded Row
(Freccetta A SX della singola riga)

*DIPENDENTI:*

Visualizzazione tabella con elenco dei dipendenti
Visualizzazione tabella con elenco dei dipendenti gestita con paginazione
Possibilità di Consultazione per informazioni aggiuntive con l’Expanded Row
(Freccetta A SX della singola riga)

*CORSI:*

Visualizza Tabella con elenco Corsi (Tabella suddivisa in Interni & Esterni)
Inserimento Corsi con un Wizard di inserimento dei vari campi e la scelta del
Docente e dei Dipendenti associati al corso
Modifica Corsi con modifica dei campi riferiti al singolo Corso
Elimina Corsi
Possibilità di Consultazione per informazioni aggiuntive con l’Expanded Row
(Freccetta A SX della singola riga)
Possibilità di Valutazione del singolo corso e successivamente dei docenti e dei
dipendenti associati.

*VALUTAZIONI :*

Creazione valutazioni per i docenti
Creazione valutazioni per i dipendenti
Creazione valutazioni per i corsi
Media delle valutazioni per i corsi
Media delle valutazioni per i docenti