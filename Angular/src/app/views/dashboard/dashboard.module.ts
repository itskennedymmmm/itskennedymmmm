
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';



import { DashboardRoutingModule } from './dashboard-routing.module';
import { HomeCarouselComponent } from '../home-carousel/home-carousel.component';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard.component';

import {CarouselModule} from 'primeng/carousel';
@NgModule({
  imports: [
    FormsModule,   
    DashboardRoutingModule,
    CommonModule,
    CarouselModule

  ],
  declarations: [
    DashboardComponent,
    HomeCarouselComponent,

    
  ]
})
export class DashboardModule { }
