import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorsoModificaComponent } from './corso-modifica.component';

describe('CorsoModificaComponent', () => {
  let component: CorsoModificaComponent;
  let fixture: ComponentFixture<CorsoModificaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorsoModificaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorsoModificaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
