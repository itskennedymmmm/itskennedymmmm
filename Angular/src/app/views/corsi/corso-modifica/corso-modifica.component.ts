import { Component, OnInit } from '@angular/core';
import {MisureService} from "../../../services/misure.service";
import {TipologieService} from "../../../services/tipologie.service";
import {TipiService} from "../../../services/tipi.service";
import {ActivatedRoute, Router} from "@angular/router";
import {CorsiService} from "../../../services/corsi.service";
import {ApiService} from "../../../services/api.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MenuItem} from "primeng";

interface TipiInt {
  forEach(arg0: (element: any) => void);
  tipo_id: number;
  descrizione: string;
}
@Component({
  selector: 'app-corso-modifica',
  templateUrl: './corso-modifica.component.html',
  styleUrls: ['./corso-modifica.component.css']
})
export class CorsoModificaComponent implements OnInit {

  constructor(public misuraService: MisureService, public tipologieService : TipologieService, public tipiService: TipiService, public router: Router, public corsiService: CorsiService, private route: ActivatedRoute, public api: ApiService, public fb: FormBuilder) { }

  //variabili
  previsto: any;
  erogato: any;
  dataErogazione: Date;
  dataChiusura: Date;
  dataCensimento: Date;
  personaleEsterno: boolean = false;
  formGroupDocente: FormGroup;
  risposta: { success: boolean; text: string; };
  loading: boolean;
  submitted: boolean;
  prev: boolean = true;
  erog: boolean = false;
  activeIndex: number = 0;
  value: number = 33;
  text: String;
  itemId: any;
  items: MenuItem[];
  tipi: TipiInt[];
  selectedTipi: TipiInt ;
  selectedTipologia;
  selectedMisura;
  id: string;

  ngOnInit(): void {

    //formGroup
    this.formGroupDocente = this.fb.group({
      corso_id: new FormControl('', Validators.required),
      tipo: new FormControl('', Validators.required),
      tipologia: new FormControl('', Validators.required),
      descrizione: new FormControl(''),
      misura: new FormControl('', Validators.required),

      luogo: new FormControl(''),
      edizione: new FormControl(''),/*
      previsto: new FormControl(''),//non gestiti
      erogato: new FormControl(''),*/
      durata: new FormControl(''),

      data_erogazione: new FormControl(''),
      data_chiusura: new FormControl(''),
      data_censimento: new FormControl(''),

      ente: new FormControl(''),/*
      interno: new FormControl(''),*/
      note: new FormControl(''),




    });

    //assegnazione Tipi,Tipologia,Misura
    this.assegnaTipi();
    this.assegnaTipologie();
    this.assegnaMisura();

    this.id = this.route.snapshot.params['id'];

    this.reloadData();
  }//fine ngOnInit


  //dati per Update al caricamento
  reloadData() {
    this.corsiService.getById(this.id).subscribe(resp => {
      if (resp.status === 200) {

        this.formGroupDocente = this.fb.group({
          corso_id:  resp.response.corso_id,
          tipo:  resp.response.tipo,
          tipologia: resp.response.tipologia,
          descrizione: resp.response.descrizione,
          misura: resp.response.misura,

          luogo: resp.response.luogo,
          edizione: resp.response.edizione,
          durata: resp.response.durata,


          data_erogazione:  resp.response.data_erogazione,
          data_chiusura:  resp.response.data_chiusura,
          data_censimento: resp.response.data_censimento,

          ente: resp.response.ente,
          interno: resp.response.interno,
          note: resp.response.note,
        });
        this.formGroupDocente.clearValidators();
      } //response
    });
  }//funzione


  assegnaTipi() {
    this.tipiService.getAll().subscribe((resp: any) => {
      if (resp.status === 200) {
        this.tipi = resp.response;
      } else {
        this.generateError('Formato inserimento non valido o errore lato server (500)');
        this.loading = false;
      }
    });
  }
  public tipologie: any[];
  assegnaTipologie(){
    this.tipologieService.getAll().subscribe((resp: any) => {
      if (resp.status === 200) {
        this.tipologie = resp.response;
      } else {
        this.generateError('Formato inserimento non valido o errore lato server (500)');
        this.loading = false;
      }
    });
  }
  public misura: any[];
  assegnaMisura(){
    this.misuraService.getAll().subscribe((resp: any) => {
      if (resp.status === 200) {
        this.misura = resp.response;
      } else {
        this.generateError('Formato inserimento non valido o errore lato server (500)');
        this.loading = false;
      }
    });
  }

  invia(): void {
  const value=this.formGroupDocente.value;
    const values = this.formGroupDocente.value;
    const valor = this.formGroupDocente.get('tipo').value;
    const valor2 = this.formGroupDocente.get('tipologia').value;
    const valor3 = this.formGroupDocente.get('misura').value;
    values.tipo=valor.tipo_id;
    values.tipologia=valor2.tipologia_id;
    values.misura=valor3.misura_id;

    this.corsiService.update(this.id, value).subscribe((resp: any) => {
      if (resp.status === 200) {
        this.risposta = { success: true, text: 'INSERIMENTO CORSO AVVENUTO CON SUCCESSO' };
        this.formGroupDocente.reset();
        let id = resp.response;
        this.router.navigate(['/corsoList']);
      } else {
        this.generateError('Formato inserimento non valido o errore lato server (500)');
        this.loading = false;
      }
    });

  }

  generateError(text: string = ''): void {
    console.log(text);
  }
  annulla() {
    this.router.navigate(['/dashboard']);
  }
}
