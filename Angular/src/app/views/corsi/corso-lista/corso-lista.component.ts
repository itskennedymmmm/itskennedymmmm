import { CorsiDocentiService } from './../../../services/corsi-docenti.service';
import { CorsiUtentiService } from './../../../services/corsi-utenti.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataTableOptions } from '../../../api/datatable-options';
import { MenuItem } from 'primeng';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MessageService } from 'primeng/api';
import { CorsiService } from '../../../services/corsi.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-corso-lista',
  templateUrl: './corso-lista.component.html',
  styleUrls: ['./corso-lista.component.css'],

  styles: [`
  :host ::ng-deep button {
      margin-right: .25em;
  }

  :host ::ng-deep .custom-toast .ui-toast-message {
      background: #FC466B;
      background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
      background: linear-gradient(to right, #3F5EFB, #FC466B);
  }

  :host ::ng-deep .custom-toast .ui-toast-message div {
      color: #ffffff;
  }

  :host ::ng-deep .custom-toast .ui-toast-message.ui-toast-message-info .ui-toast-close-icon {
      color: #ffffff;
  }
`],

  providers: [MessageService],

  animations: [
    trigger('rowExpansionTrigger', [
      state('void', style({
        transform: 'translateX(-10%)',
        opacity: 0
      })),
      state('active', style({
        transform: 'translateX(0)',
        opacity: 1
      })),
      transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])]

})
export class CorsoListaComponent implements OnInit {
  info: any;
  public lista: any;
  public attributo: string;
  public selectedCorso: any;
  items: MenuItem[];
  public presenza:boolean=true;
  cols= [

    { header: 'cognome', field: 'cognome' },
    { header: 'nome', field: 'nome' },
    { header: 'titolo_studio', field: 'titolo_studio' },

  ]

  cols2= [

    { header: 'RAGIONE SOCIALE', field: 'ragione_sociale' },
    { header: 'TITOLO', field: 'titolo' },
    { header: 'TELEFONO', field: 'telefono' },
    { header: 'LOCALITA\'', field: 'localita' }
  ]


  constructor(
    public corsiDocenti:CorsiDocentiService, public corsiUtente:CorsiUtentiService,  public corsiService: CorsiService, public router: Router, public api: ApiService, private messageService: MessageService)
    { }

    first: number = 0;

  public elenco: DataTableOptions = {
    campo: [
      { header: 'DESCRIZIONE', field: 'descrizione' },
      /* { header: 'EDIZIONE', field: 'edizione' }, */
      { header: 'DURATA', field: 'durata' },
      /* { header: 'NOTE', field: 'note' }, */
      { header: 'LUOGO', field: 'luogo' },
      { header: 'DATA EROGAZIONE', field: 'data_erogazione' }
    ]
  };

  ngOnInit() {
    this.getConta();
    this.reloadData();
    this.items = [  //tasto DX
      { label: 'Edit', icon: 'pi pi-user-edit', command: (event) => this.modifica(this.selectedCorso.corso_id) },
      { label: 'Delete', icon: 'pi pi-times', command: (event) => this.showConfirm(this.selectedCorso) }
    ];
  }


  display: boolean = false;
  visualizza:any[];
  visua : any[];
  showDialog(oggetto) {

    this.corsiUtente.findDipendente(oggetto.corso_id).subscribe((resp: any) => {
      if (resp.status === 200) {
        this.presenza=false;
        this.visualizza=resp.response;
        if (this.visualizza.length==0 )
        {
          this.presenza=true;
        }
      } else {
        this.generateError('Formato numero non valido o errore lato server (500)');
      }
    });

    this.corsiDocenti.findCorso(oggetto.corso_id).subscribe((resp: any) => {
      if (resp.status === 200) {
        this.visua=resp.response;
      } else {
        this.generateError('Formato numero non valido o errore lato server (500)');
      }
    });

    this.display = true;
}
//pulsante valuta
  valutazioni(id: any){
    this.router.navigate(['/valutazioni', id]);
  }


  showConfirm(obje:any) {
    this.messageService.clear();
    this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Sei sicuro?', detail: 'Conferma per procedere' });
    this.selectedCorso=obje;
  }

  onConfirm() {
    this.deleteCorso(this.selectedCorso);//elimina
    this.messageService.clear('c');
  }

  onReject() {
    this.messageService.clear('c');
  }

  clear() {
    this.messageService.clear();
  }

  reloadData() {
    this.corsiService.getAll(0,"").subscribe(resp => {
      if (resp.status === 200) {
        this.lista = resp.response;
      }
      else {
        this.generateError();
      }

    }
    );
  } // creo una richiesta http

  numPage: number;
  getConta() {
    this.corsiService.count().subscribe(resp => {
      if (resp.status === 200) {
        this.numPage = resp.response;
      }
      else {
        this.generateError();
      }
    }
    );
  }

  paginate(event) {
    event.rows = 10;
    event.page;
    event.pageCount = 10
    this.corsiService.getAll(event.page, "").subscribe(resp => {
      if (resp.status === 200) {
        this.lista = resp.response;
      }
      else {
        this.generateError();
      }

    }
    );
  }

  deleteCorso(lis: any) {
    this.corsiService.deleteById(lis.corso_id).subscribe(resp => {
      if (resp.status === 200) {
        this.reloadData;
        this.deleteRigaHtml(lis);
        this.getConta();
      }
      else {
        this.generateError();
      }

    }
    );
  }

  generateError(text: string = ''): void {
    this.lista = { text: 'Impossibile caricare la lista' };
  }

  reset() {
    this.first = 0;
  }

  deleteRigaHtml(lis: any) {
    let index = -1;
    for (let i = 0; i < this.lista.length; i++) {
      if (this.lista[i].corso_id == lis.corso_id) {
        index = i;
        break;
      }
    }
    this.lista.splice(index, 1);
  }

    //new docenti
    handleClick() {
      this.router.navigate(['corsiCreate']);
    }

    modifica(id: number) {
      this.router.navigate(['/corsoUpdate', id]);
    }


    nuovocorso(){
      this.router.navigate(['/corsoInsert']);
    }

    annulla(){
      this.router.navigate(['/dashboard']);
    }



};
