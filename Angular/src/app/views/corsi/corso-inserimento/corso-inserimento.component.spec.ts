import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorsoInserimentoComponent } from './corso-inserimento.component';

describe('CorsoInserimentoComponent', () => {
  let component: CorsoInserimentoComponent;
  let fixture: ComponentFixture<CorsoInserimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorsoInserimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorsoInserimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
