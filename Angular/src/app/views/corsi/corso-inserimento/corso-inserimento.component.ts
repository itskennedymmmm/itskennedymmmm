import { TipologieService } from './../../../services/tipologie.service';
import { element } from 'protractor';
import { TipiService } from './../../../services/tipi.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { CorsiService } from '../../../services/corsi.service';
import { MenuItem } from 'primeng';
import { MisureService } from '../../../services/misure.service';


interface TipiInt {
  forEach(arg0: (element: any) => void);
  tipo_id: number;
  descrizione: string;
}

@Component({
  selector: 'app-corso-inserimento',
  templateUrl: './corso-inserimento.component.html',
  styleUrls: ['./corso-inserimento.component.css'],
  styles: [`
        .ui-steps .ui-steps-item {
            width:50%;
        }
    `],
  encapsulation: ViewEncapsulation.None

})
export class CorsoInserimentoComponent implements OnInit {

  previsto: any;
  erogato: any;
  dataErogazione: Date;
  dataChiusura: Date;
  dataCensimento: Date;
  personaleEsterno: boolean = false;
  formGroupDocente: FormGroup;
  risposta: { success: boolean; text: string; };
  loading: boolean;
  submitted: boolean;
  prev: boolean = true;
  erog: boolean = false;
  activeIndex: number = 0;
  value: number = 33;
  text: String;
  itemId: any;

  constructor(public misuraService: MisureService, public tipologieService : TipologieService, public tipiService: TipiService, public router: Router, public corsiService: CorsiService, private route: ActivatedRoute, public api: ApiService, public fb: FormBuilder) { }

  items: MenuItem[];
  tipi: TipiInt[];
  selectedTipi: TipiInt ;
  selectedTipologia;
  selectedMisura;
  ngOnInit(): void {

    this.formGroupDocente = this.fb.group({
      tipo: new FormControl('', Validators.required),
      tipologia: new FormControl('', Validators.required),
      descrizione: new FormControl('', Validators.required),
      misura: new FormControl('', Validators.required),

      luogo: new FormControl('', Validators.required),
      edizione: new FormControl('', Validators.required),
      durata: new FormControl(''),

      data_erogazione: new FormControl('', Validators.required),
      data_chiusura: new FormControl('', Validators.required),
      data_censimento: new FormControl('', Validators.required),

      ente: new FormControl('', Validators.required),
      note: new FormControl(''),


    });
    this.assegnaTipi();
    this.assegnaTipologie();
    this.assegnaMisura();

    this.items = [
      {
        label: 'INSERISCI CORSO',
        command: (event: any) => {
          this.activeIndex = 0;
          this.router.navigate(['/corsoInsert']);
        }
      },
      {
        label: 'SCELTA DOC & DIP',
        command: (event: any) => {
          this.activeIndex = 1;
          this.router.navigate(['/sceltaDocDip']);
        }
      },
    ];

  }//fine ngOnInit

  get contenitore() { return JSON.stringify(this.formGroupDocente.value); }

  invia(): void {
    const values = this.formGroupDocente.value;
    const valor = this.formGroupDocente.get('tipo').value;
    const valor2 = this.formGroupDocente.get('tipologia').value;
    const valor3 = this.formGroupDocente.get('misura').value;
    values.tipo=valor.tipo_id;
    values.tipologia=valor2.tipologia_id;
    values.misura=valor3.misura_id;
    console.log(values)
    this.corsiService.add(values).subscribe((resp: any) => {
      if (resp.status === 200) {
        this.risposta = { success: true, text: 'INSERIMENTO CORSO AVVENUTO CON SUCCESSO' };
        this.formGroupDocente.reset();
        let id = resp.response;
        this.router.navigate(['/sceltaDocDip', id]);
      } else {
        this.generateError('Formato inserimento non valido o errore lato server (500)');
        this.loading = false;
      }
    });

  }

  assegnaTipi() {
    this.tipiService.getAll().subscribe((resp: any) => {
      if (resp.status === 200) {
       this.tipi = resp.response;
      } else {
        this.generateError('Formato inserimento non valido o errore lato server (500)');
        this.loading = false;
      }
    });
  }
  public tipologie: any[];
  assegnaTipologie(){
    this.tipologieService.getAll().subscribe((resp: any) => {
      if (resp.status === 200) {
       this.tipologie = resp.response;
      } else {
        this.generateError('Formato inserimento non valido o errore lato server (500)');
        this.loading = false;
      }
    });
  }
  public misura: any[];
  assegnaMisura(){
    this.misuraService.getAll().subscribe((resp: any) => {
      if (resp.status === 200) {
       this.misura = resp.response;
      } else {
        this.generateError('Formato inserimento non valido o errore lato server (500)');
        this.loading = false;
      }
    });
  }

  generateError(text: string = ''): void {
    console.log(text);
  }
  annulla() {
    this.router.navigate(['/dashboard']);
  }

  booleanPrevisto(e) {
    /*
        this.prev = e.checked;
        console.log(this.previsto)
        if (this.prev === true) {
          this.erog = false;
          this.erogato="No";
        }
        else {
          this.erog = true;
          this.erogato="Si";
        }
    */
  }

  booleanErogato(e) {
    /*
    this.erog = e.checked;
    console.log(this.erogato)
    if (this.erog === false) {
      this.prev = true;
      this.previsto="No"
    }
    else {
      this.prev = false;
      this.previsto = "Si";
    }

  }
  */

  }
}
