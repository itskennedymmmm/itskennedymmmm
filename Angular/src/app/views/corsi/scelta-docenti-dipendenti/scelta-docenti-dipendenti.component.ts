import { CorsiUtentiService } from './../../../services/corsi-utenti.service';
import { CorsiDocentiService } from './../../../services/corsi-docenti.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DipendentiService } from '../../../services/dipendenti.service';
import { DocentiService } from '../../../services/docenti.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { MenuItem } from 'primeng';


@Component({
  selector: 'app-scelta-docenti-dipendenti',
  templateUrl: './scelta-docenti-dipendenti.component.html',
  styleUrls: ['./scelta-docenti-dipendenti.component.css'],
  styles: [`
        .ui-steps .ui-steps-item {
            width: 50%;
        }
    `],
  encapsulation: ViewEncapsulation.None
})
export class SceltaDocentiDipendentiComponent implements OnInit {
  risposta: any;
  elenco: any[];
  elenc: any[];
  pagina: any;
  loading: boolean;
  rispo: any;
  pagin: any;
  loadin: boolean;
  lista: any;
  activeIndex: number = 1;
  risp: any;
  displayMaximizable: boolean;

  position: string;
  selectedAlunni: any[];
  selectedInterno= {
    "dipendenti_id":null,
    "titolo_studio": null,
    "cognome": null,
    "nome": null
  };
  selectDocente= {
    "docente_id":null,
    "titolo": null,
    "ragione_sociale": null,
    "indirizzo": null
  };
  value: number = 66;
  messageService: any;

  constructor(public corsiDocente: CorsiDocentiService, public corsiUtente: CorsiUtentiService, private route: ActivatedRoute, public docentiService: DocentiService, public dipService: DipendentiService, public router: Router, public api: ApiService, ) { }
  items: MenuItem[];
  public response: any;

  ngOnInit(): void {

    this.reloadDataA();
    this.reloadDataD();
    this.reloadDataDocI();

    this.items = [
      {
        label: 'INSERISCI CORSO',
        command: (event: any) => {
          this.activeIndex = 0;
          this.router.navigate(['/corsoInsert']);
        }
      },
      {
        label: 'SCELTA DOC & DIP',
        command: (event: any) => {
          this.activeIndex = 1;
          this.router.navigate(['/sceltaDocDip']);

        }
      },
    ];

    this.elenco = [
      { header: 'COGNOME', field: 'cognome' },
      { header: 'NOME', field: 'nome' },
      { header: 'TITOLO', field: 'titolo_studio' }
    ]

    this.elenc = [
      { header: 'RAGIONE SOCIALE', field: 'ragione_sociale' },
      { header: 'TITOLO', field: 'titolo' },
      { header: 'TELEFONO', field: 'telefono' }
    ]



  }

  showMaximizableDialog() {
    this.displayMaximizable = true;
}

  reloadDataD() {
    this.docentiService.getAll(0, "").subscribe(resp => {
      if (resp.status === 200) {
        this.lista = resp.response;
      }
      else {
        this.generateError();
      }

    }
    );
  }// creo una richiesta http


  reloadDataDocI() { //docenti Interni
    this.dipService.getAll(0, "").subscribe(resp => {
      if (resp.status === 200) {
        this.risp = resp.response;
      }
      else {
        this.generateError();
      }

    }
    );
  }// creo una richiesta http


  reloadDataA() {
    this.dipService.getAll(0, "").subscribe(resp => {
      if (resp.status === 200) {
        this.risposta = resp.response;
      }
      else {
        this.generateError();
      }

    }
    );
  }// creo una richiesta http



  oggettoLista = {
    // "corsi_utenti_id": null,
    "corso": null,
    "dipendente": null,
    "durata": null
  };

  public id: number;

  public arraDipId:number[];
  //salvare dipendenti
  public invia(selectedLista,selectedDocente,selectedDipendente) {

    console.log("docente esterno "+selectedDocente.docente_id)
    console.log("docente esterno "+selectedDipendente.dipendenti_id)
    this.oggettoDocenteE = {
      // "corsi_utenti_id": null,
      "corso": this.route.snapshot.params['id'],
      //"docente": oggetto.docente_id,
      "docente": selectedDocente.docente_id,
      "interno": selectedDipendente.dipendenti_id,
    };
    console.log(this.oggettoDocenteE)
    this.corsiDocente.add(this.oggettoDocenteE).subscribe((resp: any) => {
      if (resp.status === 200) {
        this.risposta = { success: true, text: 'INSERIMENTO DOCENTE AVVENUTO CON SUCCESSO' };
        console.log("SALVATAGGIO DOCENTE")

      } else {
        this.generateError('Formato numero non valido o errore lato server (500)');
        this.loading = false;
      }
    });

    this.id = this.route.snapshot.params['id']; //prendo ID corso
    console.log('id è ' + this.id);

    selectedLista.forEach((elem, index) => {
      console.log('id-dipendente = ' + elem.dipendenti_id)
      console.log(index);
      this.oggettoLista = {
        // "corsi_utenti_id": null,
        "corso": this.route.snapshot.params['id'],
        "dipendente": elem.dipendenti_id,
        "durata": 2,
      };
      console.log(this.oggettoLista)
      this.corsiUtente.add(this.oggettoLista).subscribe((resp: any) => {
        if (resp.status === 200) {
          this.risposta = { success: true, text: 'INSERIMENTO CORSO AVVENUTO CON SUCCESSO' };
          console.log("SALVATAGGIO CORSO")

        } else {
          this.generateError('Formato numero non valido o errore lato server (500)');
          this.loading = false;
        }
      });
    });
    this.corsi();
  }

  oggettoDocenteE = {
    // "corsi_utenti_id": null,
    "corso": null,
    "docente": null,
    "interno": null
  };

  paginate(event) {
    event.rows = 10;
    event.page;
    event.pageCount = 10

    this.dipService.getAll(event.page, "").subscribe(resp => {
      if (resp.status === 200) {
        this.risposta = resp.response;
      }
      else {
        this.generateError();
      }

    }
    );
  }

  paginateDocI(event) {
    event.rows = 10;
    event.page;
    event.pageCount = 10

    this.dipService.getAll(event.page, "").subscribe(resp => {
      if (resp.status === 200) {
        this.risp = resp.response;
      }
      else {
        this.generateError();
      }

    }
    );
  }

  paginateD(event) {
    event.rows = 10;
    event.page;
    event.pageCount = 10;

    this.docentiService.getAll(event.page, "").subscribe(resp => {
      if (resp.status === 200) {
        this.lista = resp.response;
      }
      else {
        this.generateError();
      }

    }
    );
  }


  generateError(text: string = ''): void {
    this.risposta = { error: true, text: 'Impossibile caricare la lista. Codice diverso da \'200\'' + text };
    console.log(this.risposta);
  }

  docenti() {
    this.router.navigate(['/docenti']);
  }

  home() {
    this.router.navigate(['/dashboard']);
  }

  dipendenti() {
    this.router.navigate(['/dipendenti']);
  }

  corsi() {
    this.router.navigate(['/corsoList']);
  }

  continua() {
    this.router.navigate(['/riepilogo']);
  }

}
