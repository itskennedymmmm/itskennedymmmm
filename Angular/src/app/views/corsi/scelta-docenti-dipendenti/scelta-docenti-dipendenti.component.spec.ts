import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SceltaDocentiDipendentiComponent } from './scelta-docenti-dipendenti.component';

describe('SceltaDocentiDipendentiComponent', () => {
  let component: SceltaDocentiDipendentiComponent;
  let fixture: ComponentFixture<SceltaDocentiDipendentiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SceltaDocentiDipendentiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SceltaDocentiDipendentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
