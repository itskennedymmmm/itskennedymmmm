import { Component, OnInit } from '@angular/core';
import { DocentiService } from '../../../services/docenti.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-docenti-create',
  templateUrl: './docenti-create.component.html',
  styleUrls: ['./docenti-create.component.css']
})
export class DocentiCreateComponent implements OnInit {
  submitted: boolean;

  constructor(public docentiService: DocentiService, public router: Router, private route: ActivatedRoute, public api: ApiService, public fb: FormBuilder) { }
  id: number = null;
  risposta: any = null;
  error: any = null;
  loading = false;
  formGroup: FormGroup;

  ngOnInit() {

    this.formGroup = this.fb.group({
      titolo: new FormControl('', Validators.required),
      ragione_sociale: new FormControl('', Validators.required),
      indirizzo: new FormControl('', Validators.required),
      provincia:  new FormControl('', Validators.required),
      nazione: new FormControl(''),
      telefono: new FormControl('', Validators.required),
      fax: new FormControl(''),
      partita_iva: new FormControl(''),
      localita: new FormControl('', Validators.required),
      referente: new FormControl('', Validators.required)

    });
  }

  onSubmit(value: string) {
    this.submitted = true;
}

  get contenitore() { return JSON.stringify(this.formGroup.value); }



invia(): void {
const values = this.formGroup.value;
//add Docente "http://localhost:8090/project/save",
this.docentiService.add(this.formGroup.value).subscribe((resp: any) => {
  if (resp.status === 200 ) {
    this.risposta = { success: true, text: 'INSERIMENTO DOCENTE AVVENUTO CON SUCCESSO' };
    this.formGroup.reset();
    this.router.navigate(['/docenti']);

  } else {
    this.generateError('Formato numero non valido o errore lato server (500)');
    this.loading = false;
  }
});
}
 generateError(text: string = ''): void {
    text: 'Impossibile caricare la lista'
  }
annulla(){
  this.router.navigate(['/docenti']);
}

home(){
  this.router.navigate(['/dashboard']);
}

}
