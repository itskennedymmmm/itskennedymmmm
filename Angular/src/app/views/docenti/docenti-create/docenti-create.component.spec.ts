import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocentiCreateComponent } from './docenti-create.component';

describe('DocentiCreateComponent', () => {
  let component: DocentiCreateComponent;
  let fixture: ComponentFixture<DocentiCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocentiCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocentiCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
