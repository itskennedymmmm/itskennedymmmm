import { Component, OnInit } from '@angular/core';
import { DocentiService } from '../../../services/docenti.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-docente',
  templateUrl: './update-docente.component.html',
  styleUrls: ['./update-docente.component.css']

})
export class UpdateDocenteComponent implements OnInit {
  submitted: boolean;

  constructor(public docentiService: DocentiService, public router: Router, private route: ActivatedRoute, public api: ApiService, public fb: FormBuilder) { }

  id: string;
  risposta: any = null;
  formGroup: FormGroup;
  lista: any = null;

  ngOnInit(): void {
    

    this.id = this.route.snapshot.params['id'];
    
    this.reloadData();

    this.formGroup = this.fb.group({
      titolo: new FormControl('', Validators.required),
      ragione_sociale: new FormControl('', Validators.required),
      indirizzo: new FormControl('', Validators.required),
      provincia: new FormControl('', Validators.required),
      nazione: new FormControl(''),
      telefono: new FormControl('', Validators.required),
      fax: new FormControl(''),
      partita_iva: new FormControl(''),
      localita: new FormControl('', Validators.required),
      referente: new FormControl('', Validators.required)

    })

  }//fine ngOnInit

  onSubmit() {
    this.submitted = false;
  }

  get contenitore() { return JSON.stringify(this.formGroup.value); }


  reloadData() {
    this.docentiService.getById(this.id).subscribe(resp => {
      if (resp.status === 200) {

        this.formGroup = this.fb.group({
          docente_id: resp.response.docente_id,
          titolo: resp.response.titolo,
          ragione_sociale: resp.response.ragione_sociale,
          indirizzo: resp.response.indirizzo,
          provincia: resp.response.provincia,
          nazione: resp.response.nazione,
          telefono: resp.response.telefono,
          fax: resp.response.fax,
          partita_iva: resp.response.partita_iva,
          localita: resp.response.localita,
          referente: resp.response.referente,
        });
        this.formGroup.clearValidators();
      }
      else {
        console.log("ERRORE NELLA SELEZIONE DELL'ID DOCENTE (ERRORE INTERNO)")
      }

    }
    );
  }// creo una richiesta http


  invia() {
    if (this.formGroup.valid === true) {
      this.docentiService.update(this.id, this.formGroup.value).subscribe((resp: any) => {
        if (resp.status === 200 && resp.response === true) {
          this.risposta = { success: true, text: 'Docente aggiornato!' };
          this.router.navigate(['/docenti']);
        } else {
          this.generateError();
        }
      });
    }

  }

  annulla() {
    this.router.navigate(['/docenti']);
  }

  home() {
    this.router.navigate(['/dashboard']);
  }

  generateError() {
    throw new Error("Method not implemented.");
  }




}