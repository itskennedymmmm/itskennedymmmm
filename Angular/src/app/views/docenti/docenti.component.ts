import { CorsiDocentiService } from './../../services/corsi-docenti.service';
import { DocentiService } from './../../services/docenti.service';

import { Component, OnInit } from '@angular/core';


import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { DataTableOptions } from '../../api/datatable-options';
import { MenuItem } from 'primeng';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-docenti',
  templateUrl: './docenti.component.html',
  styleUrls: ['./docenti.component.css'],

  styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }

        :host ::ng-deep .custom-toast .ui-toast-message {
            background: #FC466B;
            background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
            background: linear-gradient(to right, #3F5EFB, #FC466B);
        }

        :host ::ng-deep .custom-toast .ui-toast-message div {
            color: #ffffff;
        }

        :host ::ng-deep .custom-toast .ui-toast-message.ui-toast-message-info .ui-toast-close-icon {
            color: #ffffff;
        }
    `],

  providers: [MessageService],

  animations: [
    trigger('rowExpansionTrigger', [
      state('void', style({
        transform: 'translateX(-10%)',
        opacity: 0
      })),
      state('active', style({
        transform: 'translateX(0)',
        opacity: 1
      })),
      transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])]
})
export class DocentiComponent implements OnInit {
  info: any;
  public lista: any;
  public attributo: string;
  public selectedDocente: any;
  items: MenuItem[];
  public presenza: boolean = false;
  public selectedDipendente: any;


  constructor(
    public corsiDocente: CorsiDocentiService, public docentiService: DocentiService, public router: Router, public api: ApiService, private messageService: MessageService) { }



  first: number = 0;

  public elenco: DataTableOptions = {
    campo: [
      { header: 'RAGIONE SOCIALE', field: 'ragione_sociale' }, //name: attributi rif dao
      { header: 'TITOLO', field: 'titolo' },
      { header: 'TELEFONO', field: 'telefono' },
      { header: 'LOCALITA\'', field: 'localita' }
    ]
  };

  cols = [
    { header: 'DESCRIZIONE CORSO', field: 'descrizione' },
    { header: 'ENTE DEL CORSO', field: 'ente' },
    { header: 'DATA INIZIO', field: 'data_erogazione' },
    { header: 'DURATA CORSO', field: 'durata' },
  ]

  cols3 = [

    { header: 'COGNOME', field: 'cognome' },
    { header: 'NOME', field: 'nome' },
    { header: 'TITOLO DI STUDIO', field: 'titolo_studio' },
    { header: 'DESCRIZIONE', field: 'descrizione' },

  ]


  ngOnInit() {
    this.reloadData();
    this.items = [  //tasto DX
      { label: 'Edit', icon: 'pi pi-user-edit', command: (event) => this.modifica(this.selectedDocente.docente_id) },
      { label: 'Delete', icon: 'pi pi-times', command: (event) => this.showConfirm(this.selectedDocente) }
    ];

    this.caricamentoDocentiInt();
    this.getConta();


  }
  showConfirm(obje: any) {
    this.messageService.clear();
    this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Sei sicuro?', detail: 'Conferma per procedere' });
    this.selectedDocente = obje;

  }


  onConfirm() {
    this.deleteDocenti(this.selectedDocente);//elimina
    this.messageService.clear('c');
  }

  onReject() {
    this.messageService.clear('c');
  }

  clear() {
    this.messageService.clear();
  }


  reloadData() {
    this.docentiService.getAll(0, "").subscribe(resp => {
      if (resp.status === 200) {
        this.lista = resp.response;
      }
      else {
        this.generateError();
      }

    }
    );
  }// creo una richiesta http



  paginate(event) {
    event.rows = 10;
    event.page;
    event.pageCount = this.numPage/10

    this.docentiService.getAll(event.page, "").subscribe(resp => {
      if (resp.status === 200) {
        this.lista = resp.response;
      }
      else {
        this.generateError();
      }
    }
    );
  }

  deleteDocenti(lis: any) {
    this.docentiService.deleteById(lis.docente_id).subscribe(resp => {
      if (resp.status === 200) {
        this.reloadData;
        this.deleteRigaHtml(lis);
        //this.paginate(event);
        this.getConta();
      }
      else {
        this.generateError();
      }
    }
    );
  }
  numPage: number;
  getConta() {
    this.docentiService.count().subscribe(resp => {
      if (resp.status === 200) {
        this.numPage = resp.response;

      }
      else {
        this.generateError();
      }
    }
    );
  }
  display: boolean = false;
  visualizza: any[];

  //dialog
  showDialog(oggetto) {
    let idCorso;
    this.corsiDocente.findDocente(oggetto.docente_id).subscribe((resp: any) => {
      if (resp.status === 200) {
        this.presenza = false;
        this.visualizza = resp.response;

        //idCorso=this.visualizza[0].corso;
       // console.log("id corso "+idCorso)
        if (this.visualizza.length == 0)//se l'oggetto è vuoto
        {
          this.presenza = true;
        }
        this.display = true;
      } else {
        this.generateError('Formato numero non valido o errore lato server (500)');
      }
    });

    //getCount
    /*
    this.corsiDocente.countDip().subscribe((resp: any) => {
      if (resp.status === 200) {
        this.presenza = false;
        this.visualizza = resp.response;
        if (this.visualizza.length == 0)//se l'oggetto è vuoto
        {
          this.presenza = true;
        }
        this.display = true;
      } else {
        this.generateError('Formato numero non valido o errore lato server (500)');
      }
    });*/
  }

  generateError(text: string = ''): void {
    this.lista = { text: 'Impossibile caricare la lista' };
  }

  reset() {
    this.first = 0;
  }

  deleteRigaHtml(lis: any) {
    let index = -1;
    for (let i = 0; i < this.lista.length; i++) {
      if (this.lista[i].docente_id == lis.docente_id) {
        index = i;
        break;
      }
    }
    this.lista.splice(index, 1);
  }

  public visua: any[];

  caricamentoDocentiInt() {
    this.corsiDocente.getAll(0, "").subscribe((resp: any) => {
      if (resp.status === 200) {
        this.visua = resp.response;
      } else {
        this.generateError('Formato numero non valido o errore lato server (500)');
      }
    });
  }


  //new docenti
  handleClick() {
    this.router.navigate(['/docenteInsert']);
  }

  modifica(id: number) {
    this.router.navigate(['/docenteUpdate', id]);
  }

  annulla() {
    this.router.navigate(['/dashboard']);
  }

  dip(){
    this.router.navigate(['/dipendenti']);
  }


}
