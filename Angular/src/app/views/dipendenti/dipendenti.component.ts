import { CorsiDocentiService } from './../../services/corsi-docenti.service';
import { DipendentiService } from './../../services/dipendenti.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dipendenti',
  templateUrl: './dipendenti.component.html',
  styleUrls: ['./dipendenti.component.css']
})

export class DipendentiComponent implements OnInit {


  risposta: any;
  elenco: any[];
  loading = true;
  first: number = 0;
  yearTimeout: any;
  pagina: any;
  public presenza:boolean =false;

  cols= [
    { header: 'Corso Descrizione', field: 'descrizione' },
    { header: 'Corso Ente', field: 'ente' },
    { header: 'Data Erogazione', field: 'data_erogazione' },

  ]

  constructor(private corsiDocente:CorsiDocentiService, private dipService: DipendentiService, public router: Router) { }
  ngOnInit() {
    this.reloadData();
    this.getConta();

    this.elenco = [
      { field: 'cognome', header: 'COGNOME' },
      { field: 'nome', header: 'NOME' },
      { field: 'matricola', header: 'MATRICOLA' },
      { field: 'tipo_dipendente', header: 'TIPO DIPENDENTE' },
      { field: 'titolo_studio', header: 'TITOLO DI STUDIO' }

    ];

  }
  generateError(text: string = ''): void {
    this.risposta = { error: true, text: 'Impossibile caricare la lista. Codice diverso da \'200\'' + text };
    /* console.log(this.risposta); */
  }


  reloadData() {
    this.dipService.getAll(0, "").subscribe(resp => {
      if (resp.status === 200) {
        this.risposta = resp.response;
      }
      else {
        this.generateError();
      }

    }
    );
  }// creo una richiesta http
  display: boolean = false;

  visualizza:any[]
  //dialog
  showDialog(oggetto) {

    this.corsiDocente.findInterno(oggetto.dipendenti_id).subscribe((resp: any) => {
      if (resp.status === 200) {
        this.presenza=false;
        this.visualizza=resp.response;
        if (this.visualizza.length==0 )
        {
          /* console.log("ooooo") */
          this.presenza=true;
        }
        this.display = true;
      } else {
        this.generateError('Formato numero non valido o errore lato server (500)');
      }
    });
}

  reset() {
    this.first = 0;
  }

  paginate(event) {
    /* console.log("CONSOLE LOG DENTRO IL PAGINATOR FUNCTION"); */
    event.rows = 10;
    event.page;
    event.pageCount = 10

    /* console.log("PAGINA: " + event.page); */
    this.dipService.getAll(event.page, "").subscribe(resp => {
      if (resp.status === 200) {
       /*  console.log("dentrosi") */
        this.risposta = resp.response;
      }
      else {
        this.generateError();
      }

    }
    );
  }

  numPage: number;
  getConta() {
    this.dipService.count().subscribe(resp => {
      if (resp.status === 200) {
        console.log(resp.response);
        this.numPage = resp.response;
      }
      else {
        this.generateError();
      }
    }
    );
  }


  home(){
    this.router.navigate(['/dashboard']);
  }
}
