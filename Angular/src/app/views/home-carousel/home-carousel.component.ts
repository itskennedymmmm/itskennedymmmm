import { Component, OnInit } from '@angular/core';
import { DocentiService } from '../../services/docenti.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-carousel',
  templateUrl: './home-carousel.component.html',
  styleUrls: ['./home-carousel.component.css']
})
export class HomeCarouselComponent implements OnInit {

  elenco: any[];
  risposta: any;
  responsiveOptions: { breakpoint: string; numVisible: number; numScroll: number; }[];

  constructor(private docService: DocentiService, public router: Router) {
    this.responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 3
      },
      {
        breakpoint: '768px',
        numVisible: 2,
        numScroll: 2
      },
      {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1
      }
    ];

  }

  ngOnInit(): void {

    /*  this.docService.getAll(0,"").subscribe((resp: any) => {
         this.risposta = resp.response;
         console.log(this.risposta);
     }); */

    this.elenco = [
      { id: 1, titolo: "VALUTAZIONI", tit: "VALUTA", foto: "valutazioni.png", desc: "Valuta subito i tuoi alunni", sot: "3 tipi di Valutazioni", val: false },
      { id: 2, titolo: "CORSI", tit: "ELENCO CORSI", foto: "corsi.png", desc: "Consulta subito l'elenco dei corsi", sot: "3 Tipi di Corsi", ins: "NUOVO CORSO", val: true },
      { id: 3, titolo: "DOCENTI", tit: "ELENCO DOCENTI", foto: "docenti.png", desc: "Consulta subito l'elenco dei Docenti", sot: "Docenti interni e Esterni", ins: "NUOVO DOCENTE", val: true },
      { id: 4, titolo: "DIPENDENTI", tit: "ELENCO DIPENDENTI", foto: "studenti.png", desc: "Consulta subito l'elenco dei Dipendenti", sot: "Tantissimi Dipendenti", val: false }

    ];
  }

  scelta(id: number) {
    if (id === 1) {
      this.valutazioni();
    }
    if (id === 2) {
      this.corsi();
    }
    if (id === 3) {
      this.docenti();
    }
    if (id === 4) {
      this.dipendenti();
    }
  }

  valutazioni() {
    this.router.navigate(['/corsoList']);
  }

  corsi() {
    this.router.navigate(['/corsoList']);
    console.log("");
  }

  docenti() {
    this.router.navigate(['/docenti']);
  }

  dipendenti() {
    this.router.navigate(['/dipendenti']);
  }

  smista(id: number) {
    if (id === 2) {
      this.corsiins();
    }
    if (id === 3) {
      this.docentiins();
    }
  }

  corsiins() {
    this.router.navigate(['/corsoInsert']);
    console.log("");
  }

  docentiins() {
    this.router.navigate(['/docenteInsert']);
  }



}
