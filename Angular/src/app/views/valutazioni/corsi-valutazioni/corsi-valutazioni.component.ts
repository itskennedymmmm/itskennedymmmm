import {Component, OnInit} from '@angular/core';
import {ValutazioniCorsiService} from "../../../services/valutazioni-corsi.service";
import {FormBuilder} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-corsi-valutazioni',
  templateUrl: './corsi-valutazioni.component.html',
  styleUrls: ['./corsi-valutazioni.component.css']
})
export class CorsiValutazioniComponent implements OnInit {

  public oggetto: any[35] = [];
  public idCorso: number;
  public visualizza: any[];
  public label2: any[] = [];
  public value2: any[] = [];
  public number: any[];
  public criteri = [
    "Utilita' per la crescita personale", "Utilita' del corso nell'ambito lavorativo attuale",
    "Scelta degli argomenti specifici","Livello di approfondimento degli argomenti","Durata del Corso",
    "Corretta suddivisione tra teoria e pratica", "Efficacia esercitazioni"
  ]

  public mediaV:number[]=[0,0,0,0,0,0,0];

  constructor(private valutazioniCorsi: ValutazioniCorsiService, public fb: FormBuilder, private route: ActivatedRoute) {
  }


  ngOnInit(): void {
    this.oggetto = [];
    this.idCorso = this.route.snapshot.params['id'];
    this.loadValori();


  }

  arrayOne(n: number): any[] {
    return Array(n);
  }


  loadValoripiccolo(resp: any, a: number) {
    //inizio pt.1
    this.label2 = Object.keys(resp.response[a].valore); //metto dentro l'indice di valore
    this.value2 = Object.values(resp.response[a].valore);//metto dentro il valore di valore
    for (let i = 0; i < this.label2.length; i++) {
      let temp = [];
      let q: string = this.value2[i];
      temp = q.split(",", 5);
      this.oggetto.push({label: this.label2[i], value: temp}); //dentro ho array(label,value)
    }
  }


  //carica valori nella tabella
  loadValori() {
    this.idCorso = this.route.snapshot.params['id'];
    this.valutazioniCorsi.findCorso(this.idCorso).subscribe((resp: any) => {//ricevo righe di un CORSO, (più criteri)
      try {
        if (resp.response[0].valore != null) {//ci deve andare
          console.log("Corso caricoo")
          this.loadValoripiccolo(resp, 0)
          this.loadValoripiccolo(resp, 1)
          this.loadValoripiccolo(resp, 2)
          this.loadValoripiccolo(resp, 3)
          this.loadValoripiccolo(resp, 4)
          this.loadValoripiccolo(resp, 5)
          this.loadValoripiccolo(resp, 6)
          this.media();
        }
      } catch (e) {
        console.log("Corso non c'e nulla")
        for (let i = 0; i < 35; i++) {
          this.oggetto.push({label: "", value: ""});
        }
      }
    });
  }//fine LoadValori
  private delay(ms: number)
  {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  public async invia($event: MouseEvent) {

    let y = 0;
    for (let i = 0; i < 7; i++) { //per salvare

      if (i == 0) {
        y = 0;
      }
      if (i == 1) {
        y = 5;
      }
      if (i == 2) {
        y = 10;
      }
      if (i == 3) {
        y = 15;
      }
      if (i == 4) {
        y = 20;
      }
      if (i == 5) {
        y = 25;
      }
      if (i == 6) {
        y = 30;
      }


      let valutazione = {
        "corso": this.idCorso,
        //"criterio": this.visualizza[i].criterio,
        "criterio": this.criteri[i],
        "valore": {
          "1": this.oggetto[y].value + "",
          "2": this.oggetto[y + 1].value + "",
          "3": this.oggetto[y + 2].value + "",
          "4": this.oggetto[y + 3].value + "",
          "5": this.oggetto[y + 4].value + ""
        }
      }


      this.valutazioniCorsi.add(valutazione).subscribe((resp: any) => {
        if (resp.status === 200 && resp.response === true) {
          console.log("salvataggio")
        } else {
          this.generateError();
        }
      });

      await this.delay(500);//dormi un pò

    }//fine for

    this.media();//richiamo media
  }
  calcolaMedia(inizio,posMedia)
  {
    if(this.oggetto[4]!=null){
      let count=0;

      for(let i=inizio;i<inizio+5;i++){
        if(inizio==i){
          this.mediaV[posMedia]+=1*this.oggetto[i].value;
          count=count+this.oggetto[i].value*1;
        }
        if(inizio+1==i){
          this.mediaV[posMedia]+=2*this.oggetto[i].value;
          count=count+this.oggetto[i].value*1;
        }
        if(inizio+2==i){
          this.mediaV[posMedia]+=3*this.oggetto[i].value;
          count=count+this.oggetto[i].value*1;
        }
        if(inizio+3==i){
          this.mediaV[posMedia]+=4*this.oggetto[i].value;
          count=count+this.oggetto[i].value*1;
        }

      }
      console.log("count "+count+" media "+this.mediaV[posMedia])
      this.mediaV[posMedia]=this.mediaV[posMedia]/count;
    }else {
      console.log("oggetto not value")
    }
  }
  media(){


    this.calcolaMedia(0,0)
    this.calcolaMedia(5,1)
    this.calcolaMedia(10,2)
    this.calcolaMedia(15,3)
    this.calcolaMedia(20,4)
    this.calcolaMedia(25,5)
    this.calcolaMedia(30,6)
  }

  generateError(text: string = ''): void {
    console.log("errore generico")
  }


}
