import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorsiValutazioniComponent } from './corsi-valutazioni.component';

describe('CorsiValutazioniComponent', () => {
  let component: CorsiValutazioniComponent;
  let fixture: ComponentFixture<CorsiValutazioniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorsiValutazioniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorsiValutazioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
