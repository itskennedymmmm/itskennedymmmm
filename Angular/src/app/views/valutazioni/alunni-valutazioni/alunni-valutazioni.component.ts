import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CorsiUtentiService} from "../../../services/corsi-utenti.service";
import {ValutazioniAlunniService} from "../../../services/valutazioni-alunni.service";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-alunni-valutazioni',
  templateUrl: './alunni-valutazioni.component.html',
  styleUrls: ['./alunni-valutazioni.component.css']
})
export class AlunniValutazioniComponent implements OnInit {

  //dichiarazione variabili
  displayMaximizable: boolean;
  public presenza:boolean=true;
  public id: number;
  public idDipendente:number;
  visualizza:any[];
  errore:any[];
  valutazioniAlunno:any[];
  valoreJson: any[]=[];
  val:any[];
  cols= [
    { header: 'cognome', field: 'cognome' },
    { header: 'nome', field: 'nome' },
    { header: 'titolo_studio', field: 'titolo_studio' },
    ]

  cols2 = [
    { header: 'Criterio', field: 'criterio' },
  ]
  formGroup: FormGroup;







  constructor( public fb: FormBuilder, private corsiUtente: CorsiUtentiService, private route: ActivatedRoute, private valuAlunni:ValutazioniAlunniService)
  {}

  ngOnInit(): void {
    this.trovaId();//mi prendo ID
    this.trovaDipendente();


    this.formGroup = this.fb.group({
      logica: [''],
      impegno: [''],
      apprendimento: [''],
      velocita: [''],
      ordine: [''],
    })

  }//fine ngOnInit

  invia() {

    let logica =[
      {
        "corso": this.id,
        "dipendente":this.idDipendente,
        "criterio":"Logica",
        "valore":this.formGroup.get('logica').value,
      },
      {
        "corso": this.id,
        "dipendente":this.idDipendente,
        "criterio":"Impegno",
        "valore":this.formGroup.get('impegno').value,
      },
      {
        "corso": this.id,
        "dipendente":this.idDipendente,
        "criterio":"Apprendimento",
        "valore":this.formGroup.get('apprendimento').value,
      },
      {
        "corso": this.id,
        "dipendente":this.idDipendente,
        "criterio":"Velocita'",
        "valore":this.formGroup.get('velocita').value,
      },
      {
        "corso": this.id,
        "dipendente":this.idDipendente,
        "criterio":"Ordine e precisione",
        "valore":this.formGroup.get('ordine').value,
      }
    ]

    logica.forEach((elem,index) => {
      this.valuAlunni.add(elem).subscribe((resp: any) => {
        if (resp.status === 200 && resp.response === true) {
        } else {
          this.generateError();
        }
      });
    })//fine ForEach
    this.displayMaximizable=false;

  }//fine invia

  trovaDipendente() {//non guardare
    this.corsiUtente.findDipendente(this.id).subscribe((resp: any) => {
      if (resp.status === 200) {
        this.presenza = false;
        this.visualizza = resp.response;
        if (this.visualizza.length == 0) {
          this.presenza = true;
        }
      } else {
        this.generateError('Formato numero non valido o errore lato server (500)');
      }
    });
  } //fine trovaDipendente

  trovaId(){
    this.id = this.route.snapshot.params['id'];
  }


  //icona-valutazioni, ricevo ID del dipendente- APRO FINESTRA DIPENDENTE
  valutaAlunno(idD:number) {
    this.idDipendente=idD;
    console.log("id dipendente "+idD)
    this.valuAlunni.findDipendente(idD).subscribe((resp: any) => { //per questo dipendente
      console.log("idCorso "+this.id)
      if (resp.status === 200) {

        this.valutazioniAlunno=resp.response;
        let logicaV, impegnoV , apprendimentoV  , velocitaV , ordineV ;


        this.valutazioniAlunno.forEach((elem,index) => {
          console.log("corso che segue dipe"+elem.corso)
          if(elem.corso==this.id)
          {
            if(elem.criterio=="Logica")
            {
              logicaV=elem.valore;
            }
            if(elem.criterio=="Impegno")
            {
              impegnoV=elem.valore;
            }
            if(elem.criterio=="Apprendimento")
            {
              apprendimentoV=elem.valore;
            }
            if(elem.criterio=="Velocita'")
            {
              velocitaV=elem.valore;
            }
            if(elem.criterio=="Ordine e precisione")
            {
              ordineV=elem.valore;
            }

            this.formGroup = this.fb.group({
              logica: logicaV,
              impegno:impegnoV,
              apprendimento: apprendimentoV,
              velocita: velocitaV,
              ordine: ordineV
            });
          }

        })//fine forEach

      } else {
        this.generateError('Formato numero non valido o errore lato server (500)');
      }
    });




    this.displayMaximizable = true;
  }

  generateError(text: string = ''): void {
    console.log("errore generico")
  }

}
