import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlunniValutazioniComponent } from './alunni-valutazioni.component';

describe('AlunniValutazioniComponent', () => {
  let component: AlunniValutazioniComponent;
  let fixture: ComponentFixture<AlunniValutazioniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlunniValutazioniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlunniValutazioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
