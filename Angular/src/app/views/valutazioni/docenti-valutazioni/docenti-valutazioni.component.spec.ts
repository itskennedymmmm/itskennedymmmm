import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocentiValutazioniComponent } from './docenti-valutazioni.component';

describe('DocentiValutazioniComponent', () => {
  let component: DocentiValutazioniComponent;
  let fixture: ComponentFixture<DocentiValutazioniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocentiValutazioniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocentiValutazioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
