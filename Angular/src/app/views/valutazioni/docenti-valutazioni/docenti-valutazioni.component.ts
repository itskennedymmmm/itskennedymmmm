import {Component, OnInit} from '@angular/core';
import {ValutazioniDocentiService} from "../../../services/valutazioni-docenti.service";
import {ActivatedRoute} from "@angular/router";
import {CorsiDocentiService} from "../../../services/corsi-docenti.service";
import {tryCatch} from "rxjs/internal-compatibility";

@Component({
  selector: 'app-docenti-valutazioni',
  templateUrl: './docenti-valutazioni.component.html',
  styleUrls: ['./docenti-valutazioni.component.css']
})
export class DocentiValutazioniComponent implements OnInit {
  public oggetto: any[15] = [];
  public idCorso: number;
  public visualizza: any[];
  public label2: any[] = [];
  public value2: any[] = [];
  public number: any[];
  public criteri = [
    "Completezza", "Chiarezza Esposizione","Disponibilita'"
  ]
  public docenteIntEst:number;
  public docenteDatiI:any;
  public docenteDatiE: any;
  public mediaV:number[]=[0,0,0];
  public visual:boolean;


  constructor(private valutazioniDocenti: ValutazioniDocentiService, private route: ActivatedRoute, private corsiDocentiService: CorsiDocentiService) {
  }
  arrayOne(n: number): any[] {
    return Array(n);
  }

  ngOnInit(): void {
    this.oggetto = [];
    this.idCorso = this.route.snapshot.params['id'];
    this.loadValori();
    this.loadDocenti();
  }

  loadValoripiccolo(resp: any, a: number) {
    //inizio pt.1
    this.label2 = Object.keys(resp.response[a].valore); //metto dentro l'indice di valore
    this.value2 = Object.values(resp.response[a].valore);//metto dentro il valore di valore
    for (let i = 0; i < this.label2.length; i++) {
      let temp = [];
      let q: string = this.value2[i];
      temp = q.split(",", 5);
      this.oggetto.push({label: this.label2[i], value: temp}); //dentro ho array(label,value)
    }
  }
  //carica valori nella tabella
  loadValori() {
    this.idCorso = this.route.snapshot.params['id'];
    this.valutazioniDocenti.findCorso(this.idCorso).subscribe((resp: any) => {//ricevo righe di un CORSO, (più criteri)
      try {
        if (resp.response[0].valore != null) {//ci deve andare
          console.log("Docente caricoo")

          this.loadValoripiccolo(resp, 0)
          this.loadValoripiccolo(resp, 1)
          this.loadValoripiccolo(resp, 2)
          this.media();
        }
      } catch (e) {
        console.log("Docente non c'e nulla")
        for (let i = 0; i < 15; i++) {
          this.oggetto.push({label: "", value: ""});
        }
      }
    });
  }//fine LoadValori
  private delay(ms: number)
  {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  public async invia($event: MouseEvent) {

    let y = 0;
    for (let i = 0; i < 3; i++) { //per salvare

      if (i == 0) {
        y = 0;
      }
      if (i == 1) {
        y = 5;
      }
      if (i == 2) {
        y = 10;
      }
      if (i == 3) {
        y = 15;
      }


      let valutazione = {
        "corso": this.idCorso,
        "docente":this.docenteIntEst,
        //"criterio": this.visualizza[i].criterio,
        "criterio": this.criteri[i],
        "valore": {
          "1": this.oggetto[y].value + "",
          "2": this.oggetto[y + 1].value + "",
          "3": this.oggetto[y + 2].value + "",
          "4": this.oggetto[y + 3].value + "",
          "5": this.oggetto[y + 4].value + ""
        }
      }


      this.valutazioniDocenti.add(valutazione).subscribe((resp: any) => {
        if (resp.status === 200 && resp.response === true) {
          console.log("salvataggio")
        } else {
          this.generateError();
        }
      });

      await this.delay(500);//dormi un pò

    }//fine for

    this.media();//richiamo media
  }
  calcolaMedia(inizio,posMedia)
  {
    if(this.oggetto[4]!=null){
      let count=0;

      for(let i=inizio;i<inizio+5;i++){
        if(inizio==i){
          this.mediaV[posMedia]+=1*this.oggetto[i].value;
          count=count+this.oggetto[i].value*1;
        }
        if(inizio+1==i){
          this.mediaV[posMedia]+=2*this.oggetto[i].value;
          count=count+this.oggetto[i].value*1;
        }
        if(inizio+2==i){
          this.mediaV[posMedia]+=3*this.oggetto[i].value;
          count=count+this.oggetto[i].value*1;
        }
        if(inizio+3==i){
          this.mediaV[posMedia]+=4*this.oggetto[i].value;
          count=count+this.oggetto[i].value*1;
        }
      }
      this.mediaV[posMedia]=this.mediaV[posMedia]/count;
    }else {
      console.log("oggetto not value")
    }
  }

  media(){
    this.calcolaMedia(0,0)
    this.calcolaMedia(5,1)
    this.calcolaMedia(10,2)

  }

  loadDocenti(){ //capire il docente se interno o esterno

    this.corsiDocentiService.findCorso(this.idCorso).subscribe((resp: any) => {
      if (resp.status === 200) {
        try {
          if(resp.response[0].interno!=null)  //entra se esiste DocenteInterno
          {
            this.docenteIntEst=resp.response[0].interno; //non toccare
            this.docenteDatiI=resp.response;
            this.visual=true;  //vero se interno
          }else {
            this.docenteIntEst=resp.response[0].docente; //non toccare
            this.docenteDatiE=resp.response;
            this.visual=false;  //falso se esterno
          }

        }catch (e) {
          console.log("Non c'e' il docente nel corso")
        }


      } else {
        this.generateError('Formato numero non valido o errore lato server (500)');
      }
    });

  }

  generateError(text: string = ''): void {
    console.log("errore generico")
  }

}
