import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'MENU\'',
    url: '/dashboard',
    icon: 'icon-speedometer',
    children: [
      {
        name: 'CORSI',
        icon: '',
        /* title: true, */
        children: [
          {
            name: 'Elenco Corsi',
            url: '/corsiLista',
            icon: 'icon-list'
          },
          {
            name: 'Nuovo Corso',
            url: '/corsoInsert',
            icon: 'icon-plus'
          },
        ]
      },
      {
        name: 'DIPENDENTI',
        url: '/',
        icon: '',
        children: [
          {
            name: 'Elenco Dipendenti',
            url: '/dipendenti',
            icon: 'icon-list'
          },
        ]
      },
      {
        name: 'DOCENTI',
        url: '/',
        icon: '',
        children: [
          {
            name: 'Elenco Docenti',
            url: '/docenti',
            icon: 'icon-list'
          },
          {
            name: 'Nuovo Docente',
            url: '/docenteInsert',
            icon: 'icon-user-follow'
          },
        ]
      },
      {
        name: 'VALUTAZIONI',
        url: '/',
       /*  icon: 'icon-notebook', */
        /* title: true, */
        children: [
          {
            name: 'Valutazione Corso',
            url: '/corsiLista',
            icon: 'icon-book-open'
          },
        ]
      },
      /* {
        name: 'Utenti & Corsi',
        url: '/',
        icon: 'icon-directions',
      },
      {
        name: 'Gestione Stampa',
        url: '/',
        icon: 'icon-printer'
      }, */
    ]
  }
];
