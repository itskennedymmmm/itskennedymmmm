import { CorsoListaComponent } from './views/corsi/corso-lista/corso-lista.component';
import { UpdateDocenteComponent } from './views/docenti/update-docente/update-docente.component';
import { DocentiCreateComponent } from './views/docenti/docenti-create/docenti-create.component';
import { DocentiComponent } from './views/docenti/docenti.component';
import { DipendentiComponent } from './views/dipendenti/dipendenti.component';
import { CorsoModificaComponent } from "./views/corsi/corso-modifica/corso-modifica.component";
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';
import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { CorsoInserimentoComponent } from './views/corsi/corso-inserimento/corso-inserimento.component';
import { SceltaDocentiDipendentiComponent } from './views/corsi/scelta-docenti-dipendenti/scelta-docenti-dipendenti.component';
import { ValutazioniComponent } from "./views/valutazioni/valutazioni.component";

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },

  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'dipendenti',
        component: DipendentiComponent,
      },
      {
        path: 'docenti',
        component: DocentiComponent,
      },

      {
        path: 'docenteInsert',
        component: DocentiCreateComponent,
      },

      {
        path: 'docenteUpdate/:id',
        component: UpdateDocenteComponent,
      },

      {
        path: 'corsoList',
        component: CorsoListaComponent,
      },

      {
        path: 'valutazioni/:id',
        component: ValutazioniComponent,

      },


      {
        path: 'corsoInsert',
        component: CorsoInserimentoComponent,
      },
      {
        path: 'corsiLista',
        component: CorsoListaComponent,
      },
      {
        path: 'sceltaDocDip/:id',
        component: SceltaDocentiDipendentiComponent,
      },
      {
        path: 'corsoUpdate/:id',
        component: CorsoModificaComponent,
      },
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
