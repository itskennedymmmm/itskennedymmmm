import { TestBed } from '@angular/core/testing';

import { CorsiUtentiService } from './corsi-utenti.service';

describe('CorsiUtentiService', () => {
  let service: CorsiUtentiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CorsiUtentiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
