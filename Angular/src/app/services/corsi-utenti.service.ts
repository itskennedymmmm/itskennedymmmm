import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CorsiUtentiService {
  private readonly path = environment.endpoint.Controller;
  public listSoggetti: any[] = [];
  public s: string;

  constructor(private api: ApiService) { }
  //creiamo alcuni metodi utili
  public getAll(page: number, paramater: string): Observable<any> {
    return this.api.get(this.path + "/corsiUtenti/page/" + page);//localhost:8090/project/list
  }

  public getById(id: string): any {//restituisce un solo id
    return this.api.get(this.path + '/corsiUtenti/' + id);
  }

  public add(item: any): Observable<any> {
    console.log(this.path + '/corsiUtenti/save')
    return this.api.post(this.path + '/corsiUtenti/save', item);
  }

  public deleteById(id: number): Observable<any> {
    return this.api.delete(this.path + '/corsiUtenti/delete/' + id);
  }

  public update(id: string, value: any): Observable<any> {
    return this.api.patch(this.path + '/corsiUtenti/update/' + id, value);
  }

  public findDipendente(id:number) : Observable<any> {
    return this.api.get(this.path + '/corsiUtenti/findD?parola=' + id);
  }
}
