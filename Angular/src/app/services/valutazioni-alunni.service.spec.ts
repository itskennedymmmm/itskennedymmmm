import { TestBed } from '@angular/core/testing';

import { ValutazioniAlunniService } from './valutazioni-alunni.service';

describe('ValutazioniAlunniService', () => {
  let service: ValutazioniAlunniService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValutazioniAlunniService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
