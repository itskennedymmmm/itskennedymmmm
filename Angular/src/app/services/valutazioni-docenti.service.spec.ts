import { TestBed } from '@angular/core/testing';

import { ValutazioniDocentiService } from './valutazioni-docenti.service';

describe('ValutazioniDocentiService', () => {
  let service: ValutazioniDocentiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValutazioniDocentiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
