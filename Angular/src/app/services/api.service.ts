import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  host = environment.host;
  constructor(private http: HttpClient) { }

    //creo il metodo GET HTTP
    public get(path: string): Observable<any> {
      return this.http.get(this.host + '/' + path);//localhost:8090/project/????
    }
  
    public post(path: string, body: any): Observable<any> {
      return this.http.post(this.host + '/' + path, body);//arriva oggetto
    }
  
    public delete(path: string): Observable<any> {
      return this.http.delete(this.host + '/' + path);
    }
  
    public patch(path: string, body: any): Observable<any> {
      return this.http.patch(this.host + '/' + path, body);
    }
  
}
