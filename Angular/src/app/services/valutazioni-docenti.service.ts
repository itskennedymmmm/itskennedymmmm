import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {ApiService} from "./api.service";

@Injectable({
  providedIn: 'root'
})
export class ValutazioniDocentiService {
  private readonly path = environment.endpoint.Controller;
  constructor(private api: ApiService) { }
  //creiamo alcuni metodi utili
  public getAll(): Observable<any> {
    return this.api.get(this.path + "/valutazioni/docenti/list");//localhost:8090/project/
  }

  //aggiunta
  public add(item: any): Observable<any> {
    return this.api.post(this.path + '/valutazioni/docenti/save', item);//da verificare
  }

  //findByDipendente
  public findCorso(id:number) : Observable<any> {
    return this.api.get(this.path + '/valutazioni/docenti/findC?parola=' + id);
  }
}
