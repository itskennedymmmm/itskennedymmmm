import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {ApiService} from "./api.service";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ValutazioniAlunniService {
  private readonly path = environment.endpoint.Controller;
  constructor(private api: ApiService) { }

  //creiamo alcuni metodi utili
  public getAll(): Observable<any> {
    return this.api.get(this.path + "/valutazioni/utenti/list");//localhost:8090/project/
  }

  //aggiunta
  public add(item: any): Observable<any> {
    return this.api.post(this.path + '/valutazioni/utenti/save', item);//da verificare
  }

  //findByDipendente
  public findDipendente(id:number) : Observable<any> {
    return this.api.get(this.path + '/valutazioni/utenti/findD?parola=' + id);
  }
}
