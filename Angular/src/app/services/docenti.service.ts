import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DocentiService {
  private readonly path = environment.endpoint.Controller;
  public listSoggetti: any[] = [];
  public s: string;

  constructor(private api: ApiService) { }
  //creiamo alcuni metodi utili
  public getAll(page: number, paramater: string): Observable<any> {
    return this.api.get(this.path + "/docenti/page/" + page );//localhost:8090/project/list
  }

  public getById(id: string): any {//restituisce un solo id
    return this.api.get(this.path + '/docenti/' + id);
  }

  public add(item: any): Observable<any> {
    return this.api.post(this.path + '/docenti/save', item);
  }

  public deleteById(id: number): Observable<any> {
    return this.api.delete(this.path + '/docenti/delete/' + id);
  }

  public update(id: string, value: any): Observable<any> {
    return this.api.patch(this.path + '/docenti/update/' + id, value);
  }

  public count(): Observable<any> {
    return this.api.get(this.path + '/docenti/getCount');
  }

}
