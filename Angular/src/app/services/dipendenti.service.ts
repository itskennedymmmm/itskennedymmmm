import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DipendentiService {
  private readonly path=environment.endpoint.Controller;
  public listSoggetti : any[] = [];
  public s:string;

  constructor(private api:ApiService) { }

  public getAll(page: number, paramater: string):Observable<any>{
    return this.api.get(this.path + "/dipendenti/page/" + page);
  }

  public count(): Observable<any> {
    return this.api.get(this.path + '/dipendenti/getCount');
  }

/*
  public getById(id:string):any{//restituisce un solo id
    return this.api.get(this.path + "/list/" + id);
  } */

}
