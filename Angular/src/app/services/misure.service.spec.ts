import { TestBed } from '@angular/core/testing';

import { MisureService } from './misure.service';

describe('MisureService', () => {
  let service: MisureService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MisureService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
