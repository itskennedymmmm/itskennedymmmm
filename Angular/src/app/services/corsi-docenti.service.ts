import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CorsiDocentiService {
  private readonly path = environment.endpoint.Controller;
  public listSoggetti: any[] = [];
  public s: string;

  constructor(private api: ApiService) { }
  //creiamo alcuni metodi utili
  public getAll(page: number, paramater: string): Observable<any> {
    return this.api.get(this.path + "/corsiDocenti/page/" + page);//localhost:8090/project/list
  }

  public getById(id: string): any {//restituisce un solo id
    return this.api.get(this.path + '/corsiDocenti/' + id);
  }

  public add(item: any): Observable<any> {
    return this.api.post(this.path + '/corsiDocenti/save', item);
  }

  public deleteById(id: number): Observable<any> {
    return this.api.delete(this.path + '/corsiDocenti/delete/' + id);
  }

  public update(id: string, value: any): Observable<any> {
    return this.api.patch(this.path + '/corsiDocenti/update/' + id, value);
  }

  public findDocente(id:number) : Observable<any> {
    return this.api.get(this.path + '/corsiDocenti/findD?parola=' + id);
  }

  public findInterno(id:number) : Observable<any> {
    return this.api.get(this.path + '/corsiDocenti/findI?parola=' + id);
  }

  public findCorso(id:number) : Observable<any> {
    return this.api.get(this.path + '/corsiDocenti/findC?parola=' + id);
  }

  public countDip(id:number) : Observable<any> {
    return this.api.get(this.path + '/corsiDocenti/getCount/' + id);
  }

}
