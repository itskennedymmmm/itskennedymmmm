import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {ApiService} from "./api.service";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ValutazioniCorsiService {

  constructor(private api: ApiService) { }
  private readonly path = environment.endpoint.Controller;

  //creiamo alcuni metodi utili
  public getAll(): Observable<any> {
    return this.api.get(this.path + "/valutazioni/corsi/list");//localhost:8090/project/
  }

  //aggiunta
  public add(item: any): Observable<any> {
    return this.api.post(this.path + '/valutazioni/corsi/save', item);//da verificare
  }

  //findByDipendente
  public findCorso(id:number) : Observable<any> {
    return this.api.get(this.path + '/valutazioni/corsi/findC?parola=' + id);
  }
}
