import { TestBed } from '@angular/core/testing';

import { ValutazioniCorsiService } from './valutazioni-corsi.service';

describe('ValutazioniCorsiService', () => {
  let service: ValutazioniCorsiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValutazioniCorsiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
