import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipiService {
  private readonly path = environment.endpoint.Controller;
  constructor(private api: ApiService) { }

  //creiamo alcuni metodi utili
  public getAll(): Observable<any> {
    return this.api.get(this.path + "/tipi/list");//localhost:8090/project/list
  }
}
