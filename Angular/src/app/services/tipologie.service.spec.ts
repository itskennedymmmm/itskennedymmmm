import { TestBed } from '@angular/core/testing';

import { TipologieService } from './tipologie.service';

describe('TipologieService', () => {
  let service: TipologieService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipologieService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
