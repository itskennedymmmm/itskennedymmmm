import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TipologieService {

  private readonly path = environment.endpoint.Controller;
  constructor(private api: ApiService) { }

  //creiamo alcuni metodi utili
  public getAll(): Observable<any> {
    return this.api.get(this.path + "/tipologie/list");//localhost:8090/project/list
  }
}
