import { MenuItem, ProgressSpinnerModule } from 'primeng/primeng';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { ProgressBarModule } from 'primeng/progressbar';
import { StepsModule } from 'primeng/steps';
import { FieldsetModule } from 'primeng/fieldset';
import { TabViewModule } from 'primeng/tabview';
import { EditorModule } from 'primeng/editor';
import { CarouselModule } from 'primeng/carousel';

import {InputMaskModule} from 'primeng/inputmask';

import {SpinnerModule} from 'primeng/spinner';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';


const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,

} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { DipendentiComponent } from './views/dipendenti/dipendenti.component';
import { DocentiComponent } from './views/docenti/docenti.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DocentiCreateComponent } from './views/docenti/docenti-create/docenti-create.component';
import { ButtonModule } from 'primeng/button';
import { PaginatorModule } from 'primeng/paginator';
import { AccordionModule } from 'primeng/accordion';
import { UpdateDocenteComponent } from './views/docenti/update-docente/update-docente.component';
import { DialogModule } from 'primeng/dialog';
import { SplitButtonModule } from 'primeng/splitbutton';
import { CardModule } from 'primeng/card';
import { ToastModule } from 'primeng/toast';
import { CorsoListaComponent } from './views/corsi/corso-lista/corso-lista.component';
import { CorsoInserimentoComponent } from './views/corsi/corso-inserimento/corso-inserimento.component';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CalendarModule } from 'primeng/calendar';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { SceltaDocentiDipendentiComponent } from './views/corsi/scelta-docenti-dipendenti/scelta-docenti-dipendenti.component';
import { DropdownModule } from 'primeng/dropdown';
import { SelectButtonModule } from 'primeng/selectbutton';
import { CorsoModificaComponent } from './views/corsi/corso-modifica/corso-modifica.component';
import { SidebarModule } from 'primeng';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { ValutazioniComponent } from './views/valutazioni/valutazioni.component';
import { CorsiValutazioniComponent } from './views/valutazioni/corsi-valutazioni/corsi-valutazioni.component';
import { AlunniValutazioniComponent } from './views/valutazioni/alunni-valutazioni/alunni-valutazioni.component';
import { DocentiValutazioniComponent } from './views/valutazioni/docenti-valutazioni/docenti-valutazioni.component';
import {AlertModule} from "ngx-bootstrap/alert";
import {KeyFilterModule} from 'primeng/keyfilter';
import {RatingModule} from 'primeng/rating';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    TableModule,
    ContextMenuModule,
    ButtonModule,
    ReactiveFormsModule,
    PaginatorModule,
    AccordionModule,
    SplitButtonModule,
    CardModule,
    ToastModule,
    InputSwitchModule,
    CalendarModule,
    InputTextareaModule,
    ProgressBarModule,
    StepsModule,
    FieldsetModule,
    TabViewModule,
    EditorModule,
    DialogModule,
    CarouselModule,
    DropdownModule,
    SelectButtonModule,
    SidebarModule,
    FullCalendarModule,
    RatingModule,
    KeyFilterModule,
    SpinnerModule,
    AlertModule,
    InputMaskModule,



  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    DipendentiComponent,
    DocentiComponent,
    DocentiCreateComponent,
    UpdateDocenteComponent,
    CorsoListaComponent,
    CorsoInserimentoComponent,
    SceltaDocentiDipendentiComponent,
    CorsoModificaComponent,
    ValutazioniComponent,
    CorsiValutazioniComponent,
    AlunniValutazioniComponent,
    DocentiValutazioniComponent,


  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
