export interface ColOption {
    header : string;
    field : string;
}

export interface DataTableOptions {
    campo : ColOption[];
}