package it.its.MMMM.Project.Work.dto;

import java.sql.Date;
import java.util.HashMap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValutazioniDocentiDto {
	
	private Integer idValutazioneDocente;
	private Integer corso;
	private Integer docente;
	private String criterio;
	private HashMap<String, Object> valore;

	//Docente
	private String titolo;
	private String ragione_sociale;
	private String localita;
	private String telefono;
	
	//Corso
	private String descrizione;
	private String ente;
	private String luogo;
	private String note;
	private String durata;
	private Date data_erogazione;
	private Date data_chiusura;
	private Date data_censimento;
	
}
