package it.its.MMMM.Project.Work.dao;

import java.util.HashMap;

import javax.persistence.AttributeConverter;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Converter;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table (name = "valutazioni_utenti")
@Data
public class ValutazioniUtentiDao {
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY) //ci deve andare
	 @Column (name = "valutazioni_utenti_id")
	 private Integer idValutazioneUtente;
	 
	 @Column (name = "corso")
	 private Integer corso;
	 
	 @Column (name = "dipendente")
	 private Integer dipendente;

	 @Column (name = "criterio")
	 private String criterio;
	 
	 @Column (name = "valore")
	 private String valore;
	 
	 @ManyToOne 
	 @EqualsAndHashCode.Exclude
	 @JoinColumn(name = "corso", referencedColumnName = "corso_id", insertable = false, updatable = false)
	 @JsonBackReference
	 private CorsiDao corsiDao;
	 
	 @ManyToOne 
	 @EqualsAndHashCode.Exclude
	 @JoinColumn(name = "dipendente", referencedColumnName = "dipendenti_id", insertable = false, updatable = false)
	 @JsonBackReference
	 private DipendenteDao dipendenteDao;
	 
	 

}
