package it.its.MMMM.Project.Work.implServices;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.MMMM.Project.Work.dao.MisureDao;
import it.its.MMMM.Project.Work.dto.MisureDto;
import it.its.MMMM.Project.Work.repository.MisureRepository;
import it.its.MMMM.Project.Work.services.MisureService;

@Service
@Transactional
public class MisureServiceImpl implements MisureService{
	
	@Autowired
	MisureRepository misureRepository;
	
	//GET ALL
	@Override
	public List<MisureDto> getAll() {
		List<MisureDao> lista = misureRepository.findAll();
		List<MisureDto> listaDto = new ArrayList<MisureDto>();
		for (MisureDao u : lista) {
			MisureDto dto = new MisureDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

	//GET ALL Pagination
	@Override
	public List<MisureDto> getAll(int pagina, String ordina) {
		List<MisureDto> t = new ArrayList<>();
		for (MisureDao dao : misureRepository.findAll(PageRequest.of(pagina, 10))) {
			MisureDto dto = new MisureDto();
			daoToDto(dao, dto);
			t.add(dto);
		}
		return t;
	}

	// DELETE
	@Override
	public Boolean delete(int id) {
		try {
			misureRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// MODIFY
	@Override
	public Boolean modify(MisureDto dto, int id) {
		MisureDao dao = misureRepository.getOne(id);
		dtoToDao(dto, dao);
		try {
			misureRepository.save(dao);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// INSERT
	@Override
	public Boolean insert(MisureDto dto) {
		MisureDao dao = new MisureDao();
		try {
			dtoToDao(dto, dao);
			misureRepository.save(dao);
			return true;
		} catch (Exception e) {

			return false;
		}
	}

	// GET BY ID
	@Override
	public MisureDto getById(int id) {
		try {
			MisureDto dto = new MisureDto();
			daoToDto(misureRepository.getOne(id), dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}
	

	// helpMethod dto to dao
	@Override
	public void dtoToDao(MisureDto dto, MisureDao dao) {
		dao.setMisura_id(dto.getMisura_id());
		dao.setDescrizione(dto.getDescrizione());
		
	}
	// helpMethod dao to dto
	@Override
	public void daoToDto(MisureDao dao, MisureDto dto) {
		dto.setMisura_id(dao.getMisura_id());
		dto.setDescrizione(dao.getDescrizione());
		
	}

}
