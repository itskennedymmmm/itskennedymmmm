package it.its.MMMM.Project.Work.implServices;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONObject;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.MMMM.Project.Work.dao.CorsiUtentiDao;
import it.its.MMMM.Project.Work.dao.ValutazioniUtentiDao;
import it.its.MMMM.Project.Work.dto.CorsiUtentiDto;
import it.its.MMMM.Project.Work.dto.ValutazioniUtentiDto;
import it.its.MMMM.Project.Work.repository.ValutazioniUtentiRepository;
import it.its.MMMM.Project.Work.services.ValutazioniUtentiService;

@Service
@Transactional
public class ValutazioniUtentiServiceImpl implements ValutazioniUtentiService {

	@Autowired
	ValutazioniUtentiRepository valutazioniUtentiRepository;

	// GET Pagination
	@Override
	public List<ValutazioniUtentiDto> getAll(int pagina, String ordina) {
		List<ValutazioniUtentiDto> lista = new ArrayList<>();
		for (ValutazioniUtentiDao d : valutazioniUtentiRepository.findAll(PageRequest.of(pagina, 10))) {
			ValutazioniUtentiDto dto = new ValutazioniUtentiDto();
			daoToDto(d, dto);
			lista.add(dto);
		}
		return lista;

	}

	// GET ALL
	@Override
	public List<ValutazioniUtentiDto> getAll() {
		List<ValutazioniUtentiDao> lista = valutazioniUtentiRepository.findAll();
		List<ValutazioniUtentiDto> listaDto = new ArrayList<ValutazioniUtentiDto>();
		for (ValutazioniUtentiDao u : lista) {
			ValutazioniUtentiDto dto = new ValutazioniUtentiDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

	// DELETE
	@Override
	public Boolean delete(int id) {
		try {
			valutazioniUtentiRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// MODIFY
	@Override
	public Boolean modify(ValutazioniUtentiDto dto, int id) {
		ValutazioniUtentiDao dao = valutazioniUtentiRepository.getOne(id);
		dtoToDao(dto, dao);
		try {
			valutazioniUtentiRepository.save(dao);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// INSERT
	@Override
	public Boolean insert(ValutazioniUtentiDto dto) {
		ValutazioniUtentiDao dao = new ValutazioniUtentiDao();
		try {
			dtoToDao(dto, dao);
			valutazioniUtentiRepository.save(dao);
			return true;
		} catch (Exception e) {

			return false;
		}
	}

	// GETBYID
	@Override
	public ValutazioniUtentiDto getById(int id) {
		try {
			ValutazioniUtentiDto dto = new ValutazioniUtentiDto();
			daoToDto(valutazioniUtentiRepository.getOne(id), dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void dtoToDao(ValutazioniUtentiDto dto, ValutazioniUtentiDao dao) {
		dao.setIdValutazioneUtente(dto.getIdValutazioneUtente());
		dao.setCorso(dto.getCorso());
		dao.setDipendente(dto.getDipendente());
		dao.setCriterio(dto.getCriterio());
	    dao.setValore(dto.getValore());

		// Attributi visualizzati dei dipendenti
		if (dao.getDipendenteDao() != null) {

			dao.getDipendenteDao().setMatricola(dto.getMatricola());
			dao.getDipendenteDao().setCognome(dto.getCognome());
			dao.getDipendenteDao().setNome(dto.getNome());
			dao.getDipendenteDao().setTitolo_studio(dto.getTitolo_studio());

		}
		// Attributi visualizzati dei corsi
		if (dao.getCorsiDao() != null) {
			dao.getCorsiDao().setDescrizione(dto.getDescrizione());
		}
	}

	@Override
	public void daoToDto(ValutazioniUtentiDao dao, ValutazioniUtentiDto dto) {
		dto.setIdValutazioneUtente(dao.getIdValutazioneUtente());
		dto.setCorso(dao.getCorso());
		dto.setDipendente(dao.getDipendente());
		dto.setCriterio(dao.getCriterio());
		dto.setValore(dao.getValore());

		// Attributi visualizzati dei dipendenti
		if (dao.getDipendenteDao() != null) {

			dto.setMatricola(dao.getDipendenteDao().getMatricola());
			dto.setCognome(dao.getDipendenteDao().getCognome());
			dto.setNome(dao.getDipendenteDao().getNome());
			dto.setTitolo_studio(dao.getDipendenteDao().getTitolo_studio());
		}
		// Attributi visualizzati dei corsi
		if (dao.getCorsiDao() != null) {
			dto.setDescrizione(dao.getCorsiDao().getDescrizione());
		}

	}

	@Override
	public List<ValutazioniUtentiDto> findByDipendente(Integer numero) {
		List<ValutazioniUtentiDao> lista = valutazioniUtentiRepository.findByDipendenteIs(numero);
		List<ValutazioniUtentiDto> listaDto = new ArrayList<ValutazioniUtentiDto>();
		for (ValutazioniUtentiDao u : lista) {
			ValutazioniUtentiDto dto = new ValutazioniUtentiDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;			
	}

}
