package it.its.MMMM.Project.Work.implServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.MMMM.Project.Work.dao.ValutazioniDocentiDao;
import it.its.MMMM.Project.Work.dto.ValutazioniDocentiDto;
import it.its.MMMM.Project.Work.repository.ValutazioniDocentiRepository;
import it.its.MMMM.Project.Work.services.ValutazioniDocentiService;

@Service
@Transactional
public class ValutazioniDocentiServiceImpl implements ValutazioniDocentiService {

	@Autowired
	ValutazioniDocentiRepository valutazioniDocentiRepository;

	// GET Pagination
	@Override
	public List<ValutazioniDocentiDto> getAll(int pagina, String ordina) {
		List<ValutazioniDocentiDto> lista = new ArrayList<>();
		for (ValutazioniDocentiDao d : valutazioniDocentiRepository.findAll(PageRequest.of(pagina, 10))) {
			ValutazioniDocentiDto dto = new ValutazioniDocentiDto();
			daoToDto(d, dto);
			lista.add(dto);
		}
		return lista;

	}

	// GET ALL
	@Override
	public List<ValutazioniDocentiDto> getAll() {
		List<ValutazioniDocentiDao> lista = valutazioniDocentiRepository.findAll();
		List<ValutazioniDocentiDto> listaDto = new ArrayList<ValutazioniDocentiDto>();
		for (ValutazioniDocentiDao u : lista) {
			ValutazioniDocentiDto dto = new ValutazioniDocentiDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

	// DELETE
	@Override
	public Boolean delete(int id) {
		try {
			valutazioniDocentiRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// MODIFY
	@Override
	public Boolean modify(ValutazioniDocentiDto dto, int id) {
		ValutazioniDocentiDao dao = valutazioniDocentiRepository.getOne(id);
		dtoToDao(dto, dao);
		try {
			valutazioniDocentiRepository.save(dao);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// INSERT
	@Override
	public Boolean insert(ValutazioniDocentiDto dto) {
		ValutazioniDocentiDao dao = new ValutazioniDocentiDao();
		try {
			dtoToDao(dto, dao);
			valutazioniDocentiRepository.save(dao);
			return true;
		} catch (Exception e) {

			return false;
		}
	}

	// GETBYID
	@Override
	public ValutazioniDocentiDto getById(int id) {
		try {
			ValutazioniDocentiDto dto = new ValutazioniDocentiDto();
			daoToDto(valutazioniDocentiRepository.getOne(id), dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	//Help Method DTO to DAO
	@Override
	public void dtoToDao(ValutazioniDocentiDto dto, ValutazioniDocentiDao dao) {

		dao.setIdValutazioneDocente(dto.getIdValutazioneDocente());
		dao.setCorso(dto.getCorso());
		dao.setDocente(dto.getDocente());
		dao.setCriterio(dto.getCriterio());
		JSONObject json = new JSONObject(dto.getValore());
	    dao.setValore(json.toString());

	}

	//Help Method DAO to DTO
	@Override
	public void daoToDto(ValutazioniDocentiDao dao, ValutazioniDocentiDto dto) {

		dto.setIdValutazioneDocente(dao.getIdValutazioneDocente());
		dto.setCorso(dao.getCorso());
		dto.setDocente(dao.getDocente());
		dto.setCriterio(dao.getCriterio());
		dto.setValore((HashMap<String,Object>) new JSONObject(dao.getValore()).toMap());

		// Attributi visualizzati dei docenti
		if (dao.getDocenteDao() != null) {

			dto.setTitolo(dao.getDocenteDao().getTitolo());
			dto.setRagione_sociale(dao.getDocenteDao().getRagione_sociale());
			dto.setLocalita(dao.getDocenteDao().getLocalita());
			dto.setTelefono(dao.getDocenteDao().getTelefono());
		}

		// Attributi visualizzati dei corsi
		if (dao.getCorsiDao() != null) {
			dto.setDescrizione(dao.getCorsiDao().getDescrizione());
			dto.setEnte(dao.getCorsiDao().getEnte());
			dto.setLuogo(dao.getCorsiDao().getLuogo());
			dto.setNote(dao.getCorsiDao().getNote());
			dto.setDurata(dao.getCorsiDao().getDurata());
			dto.setData_erogazione(dao.getCorsiDao().getData_erogazione());
			dto.setData_chiusura(dao.getCorsiDao().getData_chiusura());
			dto.setData_censimento(dao.getCorsiDao().getData_censimento());
		}
	}
	
	@Override
	public List<ValutazioniDocentiDto> findByCorso(Integer numero) {
		List<ValutazioniDocentiDao> lista = valutazioniDocentiRepository.findByCorsoIs(numero);
		List<ValutazioniDocentiDto> listaDto = new ArrayList<ValutazioniDocentiDto>();
		for (ValutazioniDocentiDao u : lista) {
			ValutazioniDocentiDto dto = new ValutazioniDocentiDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;			
	}

}
