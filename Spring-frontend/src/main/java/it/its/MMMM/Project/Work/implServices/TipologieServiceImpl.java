package it.its.MMMM.Project.Work.implServices;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.MMMM.Project.Work.dao.TipologieDao;
import it.its.MMMM.Project.Work.dto.TipologieDto;
import it.its.MMMM.Project.Work.repository.TipologieRepository;
import it.its.MMMM.Project.Work.services.TipologieService;

@Service
@Transactional
public class TipologieServiceImpl implements TipologieService{
	
	@Autowired
	TipologieRepository tipologieRepository;
	
	//GET ALL
	@Override
	public List<TipologieDto> getAll() {
		List<TipologieDao> lista = tipologieRepository.findAll();
		List<TipologieDto> listaDto = new ArrayList<TipologieDto>();
		for (TipologieDao u : lista) {
			TipologieDto dto = new TipologieDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

	//GET ALL Pagination
	@Override
	public List<TipologieDto> getAll(int pagina, String ordina) {
		List<TipologieDto> t = new ArrayList<>();
		for (TipologieDao dao : tipologieRepository.findAll(PageRequest.of(pagina, 10))) {
			TipologieDto dto = new TipologieDto();
			daoToDto(dao, dto);
			t.add(dto);
		}
		return t;
	}

	// helpMethod dto to dao
	@Override
	public void dtoToDao(TipologieDto dto, TipologieDao dao) {
		dao.setTipologia_id(dto.getTipologia_id());
		dao.setDescrizione(dto.getDescrizione());
		
	}
	// helpMethod dao to dto
	@Override
	public void daoToDto(TipologieDao dao, TipologieDto dto) {
		dto.setTipologia_id(dao.getTipologia_id());
		dto.setDescrizione(dao.getDescrizione());
		
	}

	// DELETE
	@Override
	public Boolean delete(int id) {
		try {
			tipologieRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// MODIFY
	@Override
	public Boolean modify(TipologieDto dto, int id) {
		TipologieDao dao = tipologieRepository.getOne(id);
		dtoToDao(dto, dao);
		try {
			tipologieRepository.save(dao);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// INSERT
	@Override
	public Boolean insert(TipologieDto dto) {
		TipologieDao dao = new TipologieDao();
		try {
			dtoToDao(dto, dao);
			tipologieRepository.save(dao);
			return true;
		} catch (Exception e) {

			return false;
		}
	}

	// GET BY ID
	@Override
	public TipologieDto getById(int id) {
		try {
			TipologieDto dto = new TipologieDto();
			daoToDto(tipologieRepository.getOne(id), dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

}
