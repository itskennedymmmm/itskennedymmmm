package it.its.MMMM.Project.Work.services;

import java.util.List;
import it.its.MMMM.Project.Work.dao.CorsiDao;
import it.its.MMMM.Project.Work.dto.CorsiDto;

public interface CorsiService {
	
	public List<CorsiDto> getAll();
	public List<CorsiDto> getAll(int pagina ,String ordina);
	public Boolean deleteCorsi(int id);
	public Boolean modifyCorsi(CorsiDto dto, int id);
	public long insertCorsi(CorsiDto dto);
	public CorsiDto getCorsoById(int id);
	void dtoToDao(CorsiDto dto, CorsiDao dao);
	public void daoToDto(CorsiDao dao, CorsiDto dto);
	
	public long getCountCorsi();
}
