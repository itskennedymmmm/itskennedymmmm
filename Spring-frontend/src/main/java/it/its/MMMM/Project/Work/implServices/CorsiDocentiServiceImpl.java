package it.its.MMMM.Project.Work.implServices;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionException;

import it.its.MMMM.Project.Work.dao.CorsiDocentiDao;
import it.its.MMMM.Project.Work.dto.CorsiDocentiDto;
import it.its.MMMM.Project.Work.repository.CorsiDocentiRepository;
import it.its.MMMM.Project.Work.services.CorsiDocentiService;

@Service
@Transactional
public class CorsiDocentiServiceImpl implements CorsiDocentiService {

	@Autowired
	CorsiDocentiRepository corsiDocentiRepository;

	// GET Pagination
	@Override
	public List<CorsiDocentiDto> getAll(int pagina, String ordina) {
		List<CorsiDocentiDto> lista = new ArrayList<>();
		for (CorsiDocentiDao d : corsiDocentiRepository.findAll(PageRequest.of(pagina, 10))) {
			CorsiDocentiDto dto = new CorsiDocentiDto();
			daoToDto(d, dto);
			lista.add(dto);
		}
		return lista;

	}

	// GET ALL
	@Override
	public List<CorsiDocentiDto> getAll() {
		List<CorsiDocentiDao> lista = corsiDocentiRepository.findAll();
		List<CorsiDocentiDto> listaDto = new ArrayList<CorsiDocentiDto>();
		for (CorsiDocentiDao u : lista) {
			CorsiDocentiDto dto = new CorsiDocentiDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}

		return listaDto;
	}

	// DELETE
	@Override
	public Boolean delete(int id) {
		try {
			corsiDocentiRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// MODIFY
	@Override
	public Boolean modify(CorsiDocentiDto dto, int id) {
		CorsiDocentiDao dao = corsiDocentiRepository.getOne(id);
		dtoToDao(dto, dao);
		try {
			corsiDocentiRepository.save(dao);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// INSERT
	@Override
	public Boolean insert(CorsiDocentiDto dto) {
		System.out.println("DTo get interno " + dto.getInterno());
		CorsiDocentiDao dao = new CorsiDocentiDao();
		try {
			dtoToDao(dto, dao);
			corsiDocentiRepository.save(dao);
			return true;
		} catch (TransactionException e) {

			return false;
		}
	}

	// GET BY ID
	@Override
	public CorsiDocentiDto getById(int id) {
		try {
			CorsiDocentiDto dto = new CorsiDocentiDto();
			daoToDto(corsiDocentiRepository.getOne(id), dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	// Help Method DtoToDao
	@Override
	public void dtoToDao(CorsiDocentiDto dto, CorsiDocentiDao dao) {

		dao.setCorsi_docenti_id(dto.getCorsi_docenti_id());
		dao.setCorso(dto.getCorso());
		dao.setDocente(dto.getDocente());
		dao.setInterno(dto.getInterno());
		// Attributi visualizzati dei dipendenti
		if (dao.getDipendenteDao() != null) {

			dao.getDipendenteDao().setMatricola(dto.getMatricola());
			dao.getDipendenteDao().setCognome(dto.getCognome());
			dao.getDipendenteDao().setNome(dto.getNome());
			dao.getDipendenteDao().setTitolo_studio(dto.getTitolo_studio());

		}

		// Attributi visualizzati dei docenti
		if (dao.getDocenteDao() != null) {

			dao.getDocenteDao().setTitolo(dto.getTitolo());
			dao.getDocenteDao().setRagione_sociale(dto.getRagione_sociale());
			dao.getDocenteDao().setLocalita(dto.getLocalita());
			dao.getDocenteDao().setTelefono(dto.getTelefono());

		}

	}

	// Help Method DaoToDto
	@Override
	public void daoToDto(CorsiDocentiDao dao, CorsiDocentiDto dto) {

		dto.setCorsi_docenti_id(dao.getCorsi_docenti_id());
		dto.setCorso(dao.getCorso());
		dto.setDocente(dao.getDocente());
		dto.setInterno(dao.getInterno());
		dto.setDescrizione(dao.getCorsiDao().getDescrizione());

		// Attributi visualizzati dei dipendenti
		if (dao.getDipendenteDao() != null) {

			dto.setMatricola(dao.getDipendenteDao().getMatricola());
			dto.setCognome(dao.getDipendenteDao().getCognome());
			dto.setNome(dao.getDipendenteDao().getNome());
			dto.setTitolo_studio(dao.getDipendenteDao().getTitolo_studio());
		}

		// Attributi visualizzati dei docenti
		if (dao.getDocenteDao() != null) {

			dto.setTitolo(dao.getDocenteDao().getTitolo());
			dto.setRagione_sociale(dao.getDocenteDao().getRagione_sociale());
			dto.setLocalita(dao.getDocenteDao().getLocalita());
			dto.setTelefono(dao.getDocenteDao().getTelefono());
		}

		// Attributi visualizzati dei corsi
		if (dao.getCorsiDao() != null) {
			dto.setDescrizione(dao.getCorsiDao().getDescrizione());
			dto.setDurata(dao.getCorsiDao().getDurata());
			dto.setLuogo(dao.getCorsiDao().getLuogo());
			dto.setEnte(dao.getCorsiDao().getEnte());
			dto.setData_erogazione(dao.getCorsiDao().getData_erogazione());
		}

	}

	// Find Docente
	@Override
	public List<CorsiDocentiDto> findByDocente(Integer numero) {
		List<CorsiDocentiDao> lista = corsiDocentiRepository.findByDocenteIs(numero);
		List<CorsiDocentiDto> listaDto = new ArrayList<CorsiDocentiDto>();
		for (CorsiDocentiDao u : lista) {
			CorsiDocentiDto dto = new CorsiDocentiDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

	// Find Interno
	@Override
	public List<CorsiDocentiDto> findByInterno(Integer numero) {
		List<CorsiDocentiDao> lista = corsiDocentiRepository.findByInternoIs(numero);
		List<CorsiDocentiDto> listaDto = new ArrayList<CorsiDocentiDto>();
		for (CorsiDocentiDao u : lista) {
			CorsiDocentiDto dto = new CorsiDocentiDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

	// Find Corso
	@Override
	public List<CorsiDocentiDto> findByCorso(Integer numero) {
		List<CorsiDocentiDao> lista = corsiDocentiRepository.findByCorsoIs(numero);
		List<CorsiDocentiDto> listaDto = new ArrayList<CorsiDocentiDto>();
		for (CorsiDocentiDao u : lista) {
			CorsiDocentiDto dto = new CorsiDocentiDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}
	
	// Conta corsisti partecipanti
	@Override
	public long getCountCorsisti(Integer corso) {
		return corsiDocentiRepository.getCountCorsisti(corso);
	}

}
