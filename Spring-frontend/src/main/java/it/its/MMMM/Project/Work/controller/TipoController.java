/* -> Sviluppatori : Mirco Pastorino - Mattia Zogno - Matteo Solivo - Marco Fichera <-
 * 
 * La classe TipoController ha lo scopo di esporre i servizi necessari per il nostro applicattivo in relazione alla gestione dei tipi. 
 * 
 * Metodi presenti : 
 * getAll            --> ritorna una lista di oggetti tipi (gestito con pagination)
 * list              --> ritorna una lista di oggetti tipi (senza pagination)
 * readById          --> (getTipiById)
 * updateById        --> (MODIFICA)
 * insert            --> (SAVE)
 * deleteById        --> (DELETE)
 *  
 *  */
package it.its.MMMM.Project.Work.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.MMMM.Project.Work.dao.TipoDao;
import it.its.MMMM.Project.Work.dto.BaseResponseDto;
import it.its.MMMM.Project.Work.dto.TipoDto;
import it.its.MMMM.Project.Work.services.TipoService;

@RestController
@RequestMapping(value = "project/tipi")
public class TipoController {
	
	@Autowired
	TipoService tipoService;
	
	// GET Pagination 
			 @GetMapping(produces = "application/json",path = "/page/{pagina}")
			    public BaseResponseDto<List<TipoDto>> getAllDocenti(
			    		@PathVariable int pagina, @RequestParam(required = false) String ordina)
			    {
					BaseResponseDto<List<TipoDto>> response = new BaseResponseDto<>();

					List<TipoDto> tipi;

						if(ordina == null) {
							ordina = "tipo_id";
						}
			
					tipi = tipoService.getAll(pagina, ordina);

					response.setTimestamp(new Date());
					response.setStatus(HttpStatus.OK.value());
					response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

					
					response.setResponse(tipi);
					return response;
			    }
			 
			  //  GET
				@GetMapping(value = "/list", produces = "application/json")
				public BaseResponseDto<List<TipoDto>> list() { 
					
					List<TipoDto> corsi = tipoService.getAll();
					BaseResponseDto<List<TipoDto>> res = new BaseResponseDto<List<TipoDto>>();
					
					res.setTimestamp(new Date());
					res.setStatus(HttpStatus.OK.value());
					res.setMessage("ESEGUITO");
					res.setResponse(corsi);
					
					return res;
				}
				
				  // GET BY ID
				@GetMapping(produces = "application/json", path="/{idT}")
				public BaseResponseDto<TipoDao> readById(@PathVariable int idT) {
					BaseResponseDto<TipoDao> response = new BaseResponseDto<>();
					
					TipoDto tipo = tipoService.getById(idT);
					
					response.setTimestamp(new Date());
					response.setStatus(HttpStatus.OK.value());
					response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
					response.setResponse(tipo);
					
					return response;
				}
		
				//	MODIFICA
				@PatchMapping(consumes = "application/json", produces = "application/json", path = "/update/{idT}")
				public BaseResponseDto<Boolean> updateById(@RequestBody TipoDto corsoDto, @PathVariable int idT) {

					BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
					risposta.setStatus(HttpStatus.OK.value());
					risposta.setMessage("PATCH_SUCCESFUL");
					risposta.setResponse(tipoService.modify(corsoDto, idT));

					return risposta;
				}
				
				//	INSERT
				@PostMapping(consumes = "application/json", produces = "application/json", path = "/save") // input = consumes + output = produces
				public BaseResponseDto<Boolean> inserisci(@RequestBody TipoDto dto) {

					BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
					risposta.setStatus(HttpStatus.OK.value()); //200
					risposta.setMessage("POST_SUCCESFUL");
					risposta.setResponse(tipoService.insert(dto));

					return risposta;
				}
				
				// DELETE
				@DeleteMapping(produces = "application/json", path = "/delete/{idT}")
				public BaseResponseDto<Boolean> deleteById(@PathVariable int idT) {

					BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
					risposta.setStatus(HttpStatus.OK.value()); //200
					risposta.setMessage("DELETE_SUCCESFUL");
					risposta.setResponse(tipoService.delete(idT));

					return risposta;
				}

}
