package it.its.MMMM.Project.Work.implServices;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.MMMM.Project.Work.dao.CorsiDao;
import it.its.MMMM.Project.Work.dto.CorsiDto;
import it.its.MMMM.Project.Work.repository.CorsiRepository;
import it.its.MMMM.Project.Work.services.CorsiService;

@Service
@Transactional
public class CorsiServiceImpl implements CorsiService {

	@Autowired
	CorsiRepository corsiRepository;

	// GET Pagination
	@Override
	public List<CorsiDto> getAll(int pagina, String ordina) {
		List<CorsiDto> lista = new ArrayList<>();
		for (CorsiDao d : corsiRepository.findAll(PageRequest.of(pagina, 10))) {
			CorsiDto dto = new CorsiDto();
			daoToDto(d, dto);
			lista.add(dto);
		}
		return lista;

	}

	// GET ALL
	@Override
	public List<CorsiDto> getAll() {
		List<CorsiDao> lista = corsiRepository.findAll();
		List<CorsiDto> listaDto = new ArrayList<CorsiDto>();
		for (CorsiDao u : lista) {
			CorsiDto dto = new CorsiDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

	// DELETE
	@Override
	public Boolean deleteCorsi(int id) {
		try {
			corsiRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// MODIFY
	@Override
	public Boolean modifyCorsi(CorsiDto dto, int id) {
		CorsiDao dao = corsiRepository.getOne(id);
		dtoToDao(dto, dao);
		try {
			corsiRepository.save(dao);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// INSERT
	@Override
	public long insertCorsi(CorsiDto dto) {
		CorsiDao dao = new CorsiDao();
		try {
			dtoToDao(dto, dao);
			corsiRepository.save(dao);
			return dao.getCorso_id();
		} catch (Exception e) {
			return 100;
		}
	}

	// GET CORSO BY ID
	@Override
	public CorsiDto getCorsoById(int id) {
		try {
			CorsiDto dto = new CorsiDto();
			daoToDto(corsiRepository.getOne(id), dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public long getCountCorsi() {
		return corsiRepository.getCountCorsi();
	}

	// HelpMethod dao to dto
	@Override
	public void daoToDto(CorsiDao dao, CorsiDto dto) {

		dto.setCorso_id(dao.getCorso_id());
		dto.setTipo(dao.getTipo());
		dto.setTipologia(dao.getTipologia());
		dto.setMisura(dao.getMisura());
		dto.setDescrizione(dao.getDescrizione());
		dto.setEdizione(dao.getEdizione());
		dto.setPrevisto(dao.getPrevisto());
		dto.setErogato(dao.getErogato());
		dto.setDurata(dao.getDurata());
		dto.setNote(dao.getNote());
		dto.setLuogo(dao.getLuogo());
		dto.setEnte(dao.getEnte());
		dto.setData_erogazione(dao.getData_erogazione());
		dto.setData_chiusura(dao.getData_chiusura());
		dto.setData_censimento(dao.getData_censimento());
		dto.setInterno(dao.getInterno());

     	// Attributi visualizzati dei tipi
		if (dao.getTipoDao() != null) {
			dto.setDescrizioneTipo(dao.getTipoDao().getDescrizione());
		}
		
		// Attributi visualizzati delle tipologie
		if (dao.getTipologiaDao() != null) {
			dto.setDescrizioneTipologia(dao.getTipologiaDao().getDescrizione());
	    }
		
		// Attributi visualizzati delle misure
		if (dao.getMisuraDao() != null) {
			dto.setDescrizioneMisura(dao.getMisuraDao().getDescrizione());
	    }
	}

	@Override
	public void dtoToDao(CorsiDto dto, CorsiDao dao) {

		dao.setCorso_id(dto.getCorso_id());
		dao.setTipo(dto.getTipo());
		dao.setTipologia(dto.getTipologia());
		dao.setMisura(dto.getMisura());
		dao.setDescrizione(dto.getDescrizione());
		dao.setEdizione(dto.getEdizione());
		dao.setPrevisto(dto.getPrevisto());
		dao.setErogato(dto.getErogato());
		dao.setDurata(dto.getDurata());
		dao.setNote(dto.getNote());
		dao.setLuogo(dto.getLuogo());
		dao.setEnte(dto.getEnte());
		dao.setData_erogazione(dto.getData_erogazione());
		dao.setData_chiusura(dto.getData_chiusura());
		dao.setData_censimento(dto.getData_censimento());
		dao.setInterno(dto.getInterno());

	}

}
