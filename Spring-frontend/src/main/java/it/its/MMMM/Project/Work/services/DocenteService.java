package it.its.MMMM.Project.Work.services;

import java.util.List;

import it.its.MMMM.Project.Work.dao.DocenteDao;
import it.its.MMMM.Project.Work.dto.DocenteDto;


public interface DocenteService {
	
	public List<DocenteDto> getAll();
	public List<DocenteDto> getAll(int pagina ,String ordina);
	void dtoToDao(DocenteDto dto, DocenteDao dao);
	void daoToDto(DocenteDao dao, DocenteDto dto);
	public Boolean deleteDocente(int id);
	public Boolean modify(DocenteDto dto, int id);
	public Boolean insert(DocenteDto dto);
	public DocenteDto getById(int id);
	
	public List<DocenteDto> findBySomething(String parola);
	public long getCountDocenti();
}
