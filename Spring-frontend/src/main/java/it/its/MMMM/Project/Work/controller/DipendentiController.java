/* -> Sviluppatori : Mirco Pastorino - Mattia Zogno - Matteo Solivo - Marco Fichera <-
 * 
 * La classe DipendentiController ha lo scopo di esporre i servizi necessari per il nostro applicattivo in relazione ai dipendenti. 
 * 
 * Metodi presenti : 
 * getAll            --> ritorna una lista di oggetti dipendenti(gestito con pagination)
 * readById          --> (getCorsoById)
 * getCount          --> ritorna il numero totale di dipendenti
 *  */
package it.its.MMMM.Project.Work.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.MMMM.Project.Work.dto.BaseResponseDto;
import it.its.MMMM.Project.Work.dto.DipendenteDto;
import it.its.MMMM.Project.Work.services.DipendenteService;

@RestController
@RequestMapping(value = "project/dipendenti")
public class DipendentiController {

	@Autowired
	DipendenteService dipendenteService;

	// GET Pagination
	@GetMapping(produces = "application/json", path = "/page/{pagina}")
	public BaseResponseDto<List<DipendenteDto>> getAllDocenti(@PathVariable int pagina,
			@RequestParam(required = false) String ordina) {
		BaseResponseDto<List<DipendenteDto>> response = new BaseResponseDto<>();

		List<DipendenteDto> corsi;

		if (ordina == null) {
			ordina = "dipendente_id";
		}

		corsi = dipendenteService.getAll(pagina, ordina);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		response.setResponse(corsi);
		return response;
	}

	// GET BY ID
	@GetMapping(produces = "application/json", path = "/getById/{idD}")
	public BaseResponseDto<DipendenteDto> readById(@PathVariable int idD) {
		BaseResponseDto<DipendenteDto> response = new BaseResponseDto<>();

		DipendenteDto dip = dipendenteService.getById(idD);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(dip);

		return response;
	}

	// GET COUNT
	@GetMapping(value = "/getCount", produces = "application/json")
	public BaseResponseDto<Long> getCount() {
		long dipendenti = dipendenteService.getCountDipendenti();

		BaseResponseDto<Long> res = new BaseResponseDto<Long>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("ESEGUITO");
		res.setResponse(dipendenti);

		return res;
	}

}