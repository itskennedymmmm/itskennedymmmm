/* -> Sviluppatori : Mirco Pastorino - Mattia Zogno - Matteo Solivo - Marco Fichera <-
 * 
 * La classe DocentiController ha lo scopo di esporre i servizi necessari per il nostro applicattivo in relazione alla gestione dei docenti. 
 * 
 * Metodi presenti : 
 * getAll            --> ritorna una lista di oggetti docenti (gestito con pagination)
 * list              --> ritorna una lista di oggetti docenti (senza pagination)
 * readById          --> (getDocenteDocenteById)
 * findBySomthing    --> ricerca per una parametro specificato
 * updateById        --> (MODIFICA)
 * insert            --> (SAVE)
 * deleteById        --> (DELETE)
 * getCount          --> ritorna il numero di docenti
 *  */
package it.its.MMMM.Project.Work.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.MMMM.Project.Work.dto.BaseResponseDto;
import it.its.MMMM.Project.Work.dto.DocenteDto;
import it.its.MMMM.Project.Work.services.DocenteService;

@RestController
@RequestMapping(value = "project/docenti")
public class DocentiController {

	@Autowired
	DocenteService docenteService;

	// ---------------------------save
	@GetMapping("/ping")
	public String save() {
		return "PROVA DI PING...AVVENUTA CON SUCCESSO";
	}

	// GET Pagination
	@GetMapping(produces = "application/json", path = "/page/{pagina}")
	public BaseResponseDto<List<DocenteDto>> getAllDocenti(@PathVariable int pagina,
			@RequestParam(required = false) String ordina) {
		BaseResponseDto<List<DocenteDto>> response = new BaseResponseDto<>();

		List<DocenteDto> docenti;

		if (ordina == null) {
			ordina = "docente_id";
		}

		docenti = docenteService.getAll(pagina, ordina);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		response.setResponse(docenti);
		return response;
	}

	// GET
	@GetMapping(value = "/list", produces = "application/json")
	public BaseResponseDto<List<DocenteDto>> list() {
		List<DocenteDto> docenti = docenteService.getAll();

		BaseResponseDto<List<DocenteDto>> res = new BaseResponseDto<List<DocenteDto>>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("ESEGUITO");
		res.setResponse(docenti);

		return res;
	}

	// GET BY ID
	@GetMapping(produces = "application/json", path = "/{id}")
	public BaseResponseDto<DocenteDto> readById(@PathVariable int id) {
		BaseResponseDto<DocenteDto> response = new BaseResponseDto<>();

		DocenteDto docente = docenteService.getById(id);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docente);

		return response;
	}

	// Query http://localhost:8090/project/docenti/find?parola=
	@GetMapping(produces = "application/json", value = "/find")
	public BaseResponseDto<List<DocenteDto>> findBySomething(@RequestParam("parola") String parola) {

		BaseResponseDto<List<DocenteDto>> response = new BaseResponseDto<List<DocenteDto>>();
		List<DocenteDto> docente = docenteService.findBySomething(parola);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docente);

		return response;
	}

	// MODIFICA
	@PatchMapping(consumes = "application/json", produces = "application/json", path = "/update/{idD}")
	public BaseResponseDto<Boolean> updateById(@RequestBody DocenteDto docenteDto, @PathVariable int idD) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value());
		risposta.setMessage("PATCH_SUCCESFUL");
		risposta.setResponse(docenteService.modify(docenteDto, idD));

		return risposta;
	}

	// INSERT
	@PostMapping(consumes = "application/json", produces = "application/json", path = "/save") // input = consumes +
																								// output = produces
	public BaseResponseDto<Boolean> inserisci(@RequestBody DocenteDto dto) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("POST_SUCCESFUL");
		risposta.setResponse(docenteService.insert(dto));

		return risposta;
	}

	// DELETE
	@DeleteMapping(produces = "application/json", path = "/delete/{idD}")
	public BaseResponseDto<Boolean> deleteById(@PathVariable int idD) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("DELETE_SUCCESFUL");
		risposta.setResponse(docenteService.deleteDocente(idD));

		return risposta;
	}

	// GET COUNT
	@GetMapping(value = "/getCount", produces = "application/json")
	public BaseResponseDto<Long> getCount() {
		long docenti = docenteService.getCountDocenti();

		BaseResponseDto<Long> res = new BaseResponseDto<Long>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("ESEGUITO");
		res.setResponse(docenti);

		return res;
	}
}
