package it.its.MMMM.Project.Work.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CorsiDto {
	
	private Integer corso_id;
	private Integer tipo;
	private Integer tipologia;
	private Integer misura;
	private String descrizione;
	private String edizione;
	private String previsto;
	private String erogato;
	private String durata;
	private String note;
	private String luogo;
	private String ente;
	private Date data_erogazione;
	private Date data_chiusura;
	private Date data_censimento;
	private Integer interno;
	
	private String descrizioneTipo;
	private String descrizioneTipologia;
	private String descrizioneMisura;
}
