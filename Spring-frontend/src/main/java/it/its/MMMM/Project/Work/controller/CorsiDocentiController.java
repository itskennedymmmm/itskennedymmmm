/* -> Sviluppatori : Mirco Pastorino - Mattia Zogno - Matteo Solivo - Marco Fichera <-
 * 
 * La classe CorsiDocentiController ha lo scopo di esporre i servizi necessari per il nostro applicattivo in relazione ai corsi tenuti dai docenti. 
 * 
 * Metodi presenti : 
 * getAll            --> ritorna una lista di oggetti corsiDocenti (gestito con pagination)
 * list              --> ritorna una lista di oggetti corsiDocenti (senza pagination)
 * readById          --> (getCorsoDocenteById)
 * updateById        --> (MODIFICA)
 * insert            --> (SAVE)
 * deleteById        --> (DELETE)
 * getCountCorsisti(idCorso) --> ritorna il numero di corsisti per quel corso
 *  */
package it.its.MMMM.Project.Work.controller;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import it.its.MMMM.Project.Work.dto.BaseResponseDto;
import it.its.MMMM.Project.Work.dto.CorsiDocentiDto;
import it.its.MMMM.Project.Work.services.CorsiDocentiService;

@RestController
@RequestMapping(value = "project/corsiDocenti")
public class CorsiDocentiController {

	@Autowired
	CorsiDocentiService corsiDocentiService;

	// GET Pagination
	@GetMapping(produces = "application/json", path = "/page/{pagina}")
	public BaseResponseDto<List<CorsiDocentiDto>> getAll(@PathVariable int pagina,
			@RequestParam(required = false) String ordina) {
		BaseResponseDto<List<CorsiDocentiDto>> response = new BaseResponseDto<>();

		List<CorsiDocentiDto> corsi;

		if (ordina == null) {
			ordina = "idCorsoDocente";
		}

		corsi = corsiDocentiService.getAll(pagina, ordina);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		response.setResponse(corsi);
		return response;
	}

	// GET
	@GetMapping(value = "/list", produces = "application/json")
	public BaseResponseDto<List<CorsiDocentiDto>> list() {

		List<CorsiDocentiDto> corsi = corsiDocentiService.getAll();
		BaseResponseDto<List<CorsiDocentiDto>> res = new BaseResponseDto<List<CorsiDocentiDto>>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("ESEGUITO");
		res.setResponse(corsi);

		return res;
	}

	// GET BY ID
	@GetMapping(produces = "application/json", path = "/{idC}")
	public BaseResponseDto<CorsiDocentiDto> readById(@PathVariable int idC) {
		BaseResponseDto<CorsiDocentiDto> response = new BaseResponseDto<>();

		CorsiDocentiDto corso = corsiDocentiService.getById(idC);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corso);

		return response;
	}

	// MODIFICA
	@PatchMapping(consumes = "application/json", produces = "application/json", path = "/update/{idC}")
	public BaseResponseDto<Boolean> updateById(@RequestBody CorsiDocentiDto corsoDocentiDto, @PathVariable int idC) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value());
		risposta.setMessage("PATCH_SUCCESFUL");
		risposta.setResponse(corsiDocentiService.modify(corsoDocentiDto, idC));

		return risposta;
	}

	// INSERT
	@PostMapping(consumes = "application/json", produces = "application/json", path = "/save") // input = consumes +
																								// output = produces
	public BaseResponseDto<Boolean> inserisci(@RequestBody CorsiDocentiDto dto) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("POST_SUCCESFUL");
		risposta.setResponse(corsiDocentiService.insert(dto));

		return risposta;
	}

	// DELETE
	@DeleteMapping(produces = "application/json", path = "/delete/{idC}")
	public BaseResponseDto<Boolean> deleteById(@PathVariable int idC) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("DELETE_SUCCESFUL");
		risposta.setResponse(corsiDocentiService.delete(idC));

		return risposta;
	}
	
	
	//Query   http://localhost:8090/project/corsiDocenti/findD?parola=1
	@GetMapping( produces = "application/json", value="/findD")
	public BaseResponseDto<List<CorsiDocentiDto>> findByDocente(@RequestParam("parola") Integer numero){
		
		BaseResponseDto<List<CorsiDocentiDto>> response = new BaseResponseDto<List<CorsiDocentiDto>>();
		List<CorsiDocentiDto> docente = corsiDocentiService.findByDocente(numero);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docente);

		return response;
	}
	
	//Query   http://localhost:8090/project/docenti/findI?parola=2
	@GetMapping( produces = "application/json", value="/findI")
	public BaseResponseDto<List<CorsiDocentiDto>> findByInterno(@RequestParam("parola") Integer numero){
		
		BaseResponseDto<List<CorsiDocentiDto>> response = new BaseResponseDto<List<CorsiDocentiDto>>();
		List<CorsiDocentiDto> dipendente = corsiDocentiService.findByInterno(numero);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(dipendente);

		return response;
	}
	
	//Query  http://localhost:8090/project/corsiDocenti/findC?parola=1    FIND By Corso
	@GetMapping( produces = "application/json", value="/findC")
	public BaseResponseDto<List<CorsiDocentiDto>> findByCorso(@RequestParam("parola") Integer numero){
		
		BaseResponseDto<List<CorsiDocentiDto>> response = new BaseResponseDto<List<CorsiDocentiDto>>();
		List<CorsiDocentiDto> docente = corsiDocentiService.findByCorso(numero);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docente);

		return response;
	}

	// GET COUNT ----> conta i partecipanti a un determinato corso
	@GetMapping(value = "/getCount/{corsoId}", produces = "application/json")
	public BaseResponseDto<Long> getCountCorsisti(@PathVariable Integer corsoId) {
		long dipendenti = corsiDocentiService.getCountCorsisti(corsoId);

		BaseResponseDto<Long> res = new BaseResponseDto<Long>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("ESEGUITO");
		res.setResponse(dipendenti);

		return res;
	}

}
