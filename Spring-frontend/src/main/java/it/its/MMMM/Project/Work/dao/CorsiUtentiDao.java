package it.its.MMMM.Project.Work.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table (name = "corsi_utenti")
@Data
public class CorsiUtentiDao {
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY) //ci deve andare
	 @Column (name = "corsi_utenti_id")
	 private Integer corsi_utenti_id;
	 
	 @Column (name = "corso")
	 private Integer corso;

	 @Column (name = "dipendente")
	 private Integer dipendente;
	 
	 @Column (name = "durata")
	 private Integer durata;
	 
	 @ManyToOne (fetch = FetchType.EAGER)
	 @EqualsAndHashCode.Exclude
	 @JoinColumn(name = "corso", referencedColumnName = "corso_id", insertable = false, updatable = false)
	 @JsonBackReference
	 private CorsiDao corsiDao;
	 
	 @ManyToOne (fetch = FetchType.EAGER)
	 @EqualsAndHashCode.Exclude
	 @JoinColumn(name = "dipendente", referencedColumnName = "dipendenti_id", insertable = false, updatable = false)
	 @JsonBackReference
	 private DipendenteDao dipendenteDao;
	 
	 
}
