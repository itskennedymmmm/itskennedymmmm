package it.its.MMMM.Project.Work.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "societa") // Nome tabella
@Data
public class SocietaDao {
	
	@Id
	@Column(name = "societa_id")
	private int societa_id;

	@Column(name = "ragione_sociale")
	private String ragione_sociale;

	@Column(name = "indirizzo")
	private String indirizzo;

	@Column(name = "localita")
	private String localita;

	@Column(name = "provincia")
	private String provincia;

	@Column(name = "nazione")
	private String nazione;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "fax")
	private String fax;

	@Column(name = "partita_iva")
	private String partita_iva;
	
	@OneToMany(targetEntity =DipendenteDao.class, mappedBy = "societaDao", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<DipendenteDao> societaDao = new ArrayList<>();

}
