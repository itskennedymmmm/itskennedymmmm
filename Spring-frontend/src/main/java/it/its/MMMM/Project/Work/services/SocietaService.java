package it.its.MMMM.Project.Work.services;

import java.util.List;

import it.its.MMMM.Project.Work.dao.SocietaDao;
import it.its.MMMM.Project.Work.dto.SocietaDto;

public interface SocietaService {
	
	public List<SocietaDto> getAll();
	public List<SocietaDto> getAll(int pagina ,String ordina);
	void dtoToDao(SocietaDto dto, SocietaDao dao);
	void daoToDto(SocietaDao dao, SocietaDto dto);
	public Boolean delete(int id);
	public Boolean modify(SocietaDto dto, int id);
	public Boolean insert(SocietaDto dto);
	public SocietaDto getById(int id);

}
