package it.its.MMMM.Project.Work.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table (name = "dipendenti")
@Data
public class DipendenteDao {
	
	 @Id
	 @Column (name = "dipendenti_id")
	 private Integer dipendenti_id;
	 
	 @Column (name = "matricola")
	 private Integer matricola;
	 
	 @Column (name = "societa")
	 private Integer societa;
	 
	 @Column (name = "cognome")
	 private String cognome;
	 
	 @Column (name = "nome")
	 private String nome;
	 
	 @Column (name = "sesso")
	 private String sesso;
	 
	 @Column (name = "data_nascita")
	 private Date data_nascita ;
	 
	 @Column (name = "luogo_nascita")
	 private String luogo_nascita;
	 
	 @Column (name = "stato_civile")
	 private String stato_civile;
	 
	 @Column (name = "titolo_studio")
	 private String titolo_studio;
	
	 @Column (name = "conseguito_presso")
	 private String conseguito_presso;
	 
	 @Column (name = "anno_conseguimento")
	 private Integer anno_conseguimento;
	 
	 @Column (name = "tipo_dipendente")
	 private String tipo_dipendente;
	 
	 @Column (name = "qualifica")
	 private String qualifica;
	 
	 @Column (name = "livello")
	 private String livello;
	 
	 @Column (name = "data_assunzione")
	 private Date data_assunzione;
	 
	 @Column (name = "responsabile_risorsa")
	 private Integer responsabile_risorsa;
	 
	 @Column (name = "responsabile_area")
	 private Integer responsabile_area;
	 
	 @Column (name = "data_fine_rapporto")
	 private Date data_fine_rapporto;
	 
	 @Column (name = "data_scadenza_contratto")
	 private Date data_scadenza_contratto;
	 
	 @ManyToOne
	 @EqualsAndHashCode.Exclude
	 @JoinColumn(name = "societa", referencedColumnName = "societa_id", insertable = false, updatable = false)
	 @JsonBackReference
	 private SocietaDao societaDao;
	 
	 @OneToMany(targetEntity=CorsiDocentiDao.class, mappedBy="dipendenteDao",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	 private List<CorsiDocentiDao> corsiDocentiDao = new ArrayList<>();
	 
	 @OneToMany(targetEntity=CorsiUtentiDao.class, mappedBy="dipendenteDao",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	 private List<CorsiUtentiDao> corsiUtentiDao = new ArrayList<>();
	 
	 @OneToMany(targetEntity=ValutazioniUtentiDao.class, mappedBy="dipendenteDao",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	 private List<ValutazioniDocentiDao> valutazioniUtentiDao = new ArrayList<>();

	 
	 
}
