package it.its.MMMM.Project.Work.services;

import java.util.List;

import it.its.MMMM.Project.Work.dao.CorsiDocentiDao;
import it.its.MMMM.Project.Work.dto.CorsiDocentiDto;

public interface CorsiDocentiService {
	
	public List<CorsiDocentiDto> getAll(int pagina ,String ordina);
	public List<CorsiDocentiDto> getAll();
	public Boolean delete(int id);
	public Boolean modify(CorsiDocentiDto dto, int id);
	public Boolean insert(CorsiDocentiDto dto);
	public CorsiDocentiDto getById(int id);
	void dtoToDao(CorsiDocentiDto dto, CorsiDocentiDao dao);
	public void daoToDto(CorsiDocentiDao dao, CorsiDocentiDto dto);
	
	public List<CorsiDocentiDto> findByDocente(Integer parola);
	public List<CorsiDocentiDto> findByInterno(Integer parola);
	public List<CorsiDocentiDto> findByCorso(Integer parola);
	
	public long getCountCorsisti(Integer corso);
}
