package it.its.MMMM.Project.Work.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValutazioniUtentiDto {
	
	private Integer idValutazioneUtente;
	private Integer corso;
	private Integer dipendente;
	private String criterio;
	private String valore;
	
	// Dipendente
	private Integer matricola;
	private String cognome;
	private String nome;
	private String titolo_studio;

	// Corso
	private String descrizione;
	private String ente;
	private String luogo;
	private String note;
	private String durata;
	private Date data_erogazione;
	private Date data_chiusura;
	private Date data_censimento;





}
