package it.its.MMMM.Project.Work.implServices;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.MMMM.Project.Work.dao.CorsiUtentiDao;
import it.its.MMMM.Project.Work.dto.CorsiUtentiDto;
import it.its.MMMM.Project.Work.repository.CorsiUtentiRepository;
import it.its.MMMM.Project.Work.services.CorsiUtentiService;

@Service
@Transactional
public class CorsiUtentiServiceImpl implements CorsiUtentiService {

	@Autowired
	CorsiUtentiRepository corsiUtentiRepository;

	// GET Pagination
	@Override
	public List<CorsiUtentiDto> getAll(int pagina, String ordina) {
		List<CorsiUtentiDto> lista = new ArrayList<>();
		for (CorsiUtentiDao d : corsiUtentiRepository.findAll(PageRequest.of(pagina, 10))) {
			CorsiUtentiDto dto = new CorsiUtentiDto();
			daoToDto(d, dto);
			lista.add(dto);
		}
		return lista;

	}

	// GET ALL
	@Override
	public List<CorsiUtentiDto> getAll() {
		List<CorsiUtentiDao> lista = corsiUtentiRepository.findAll();
		List<CorsiUtentiDto> listaDto = new ArrayList<CorsiUtentiDto>();
		for (CorsiUtentiDao u : lista) {
			CorsiUtentiDto dto = new CorsiUtentiDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

	// DELETE
	@Override
	public Boolean delete(int id) {
		try {
			corsiUtentiRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// MODIFY
	@Override
	public Boolean modify(CorsiUtentiDto dto, int id) {
		CorsiUtentiDao dao = corsiUtentiRepository.getOne(id);
		dtoToDao(dto, dao);
		try {
			corsiUtentiRepository.save(dao);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// INSERT
	@Override
	public Boolean insert(CorsiUtentiDto dto) {
		CorsiUtentiDao dao = new CorsiUtentiDao();
		try {
			dtoToDao(dto, dao);
			corsiUtentiRepository.save(dao);
			return true;
		} catch (Exception e) {

			return false;
		}
	}

	// GETBYID
	@Override
	public CorsiUtentiDto getById(int id) {
		try {
			CorsiUtentiDto dto = new CorsiUtentiDto();
			daoToDto(corsiUtentiRepository.getOne(id), dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void dtoToDao(CorsiUtentiDto dto, CorsiUtentiDao dao) {
		dao.setCorsi_utenti_id(dto.getCorsi_utenti_id());
		dao.setCorso(dto.getCorso());
		dao.setDipendente(dto.getDipendente());
		dao.setDurata(dto.getDurata());

		// Attributi visualizzati dei dipendenti
		if (dao.getDipendenteDao() != null) {

			dao.getDipendenteDao().setMatricola(dto.getMatricola());
			dao.getDipendenteDao().setCognome(dto.getCognome());
			dao.getDipendenteDao().setNome(dto.getNome());
			dao.getDipendenteDao().setTitolo_studio(dto.getTitolo_studio());

		}

	}

	@Override
	public void daoToDto(CorsiUtentiDao dao, CorsiUtentiDto dto) {
		dto.setCorsi_utenti_id(dao.getCorsi_utenti_id());
		dto.setCorso(dao.getCorso());
		dto.setDipendente(dao.getDipendente());
		dto.setDurata(dao.getDurata());

		// Attributi visualizzati dei dipendenti
		if (dao.getDipendenteDao() != null) {

			dto.setMatricola(dao.getDipendenteDao().getMatricola());
			dto.setCognome(dao.getDipendenteDao().getCognome());
			dto.setNome(dao.getDipendenteDao().getNome());
			dto.setTitolo_studio(dao.getDipendenteDao().getTitolo_studio());
		}
		
		// Attributi visualizzati dei corsi
		if (dao.getCorsiDao() != null) {
			dto.setDescrizione(dao.getCorsiDao().getDescrizione());
			dto.setNote(dao.getCorsiDao().getNote());
			dto.setEnte(dao.getCorsiDao().getEnte());
		}
	}
	
	
	//Trova il docente dato il corso
	@Override
	public List<CorsiUtentiDto> findByDipendente(Integer numero) {
		List<CorsiUtentiDao> lista = corsiUtentiRepository.findByCorsoIs(numero);
		List<CorsiUtentiDto> listaDto = new ArrayList<CorsiUtentiDto>();
		for (CorsiUtentiDao u : lista) {
			CorsiUtentiDto dto = new CorsiUtentiDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

}
