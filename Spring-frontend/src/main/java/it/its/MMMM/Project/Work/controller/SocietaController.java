/* -> Sviluppatori : Mirco Pastorino - Mattia Zogno - Matteo Solivo - Marco Fichera <-
 * 
 * La classe SocietàController ha lo scopo di esporre i servizi necessari per il nostro applicattivo in relazione alla gestione delle società. 
 * 
 * Metodi presenti : 
 * getAll            --> ritorna una lista di oggetti società (gestito con pagination)
 * list              --> ritorna una lista di oggetti società (senza pagination)
 * readById          --> (getSocietàById)
 * updateById        --> (MODIFICA)
 * insert            --> (SAVE)
 * deleteById        --> (DELETE)
 *  
 *  */
package it.its.MMMM.Project.Work.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.MMMM.Project.Work.dto.BaseResponseDto;
import it.its.MMMM.Project.Work.dto.SocietaDto;
import it.its.MMMM.Project.Work.services.SocietaService;

@RestController
@RequestMapping(value = "project/societa")
public class SocietaController {

	@Autowired
	SocietaService societaService;

	// ---------------------------save
	@GetMapping("/ping")
	public String save() {
		return "PROVA DI PING...AVVENUTA CON SUCCESSO";
	}

	// GET Pagination
	@GetMapping(produces = "application/json", path = "/page/{pagina}")
	public BaseResponseDto<List<SocietaDto>> getAllDocenti(@PathVariable int pagina,
			@RequestParam(required = false) String ordina) {
		BaseResponseDto<List<SocietaDto>> response = new BaseResponseDto<>();

		List<SocietaDto> docenti;

		if (ordina == null) {
			ordina = "societa_id";
		}

		docenti = societaService.getAll(pagina, ordina);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		response.setResponse(docenti);
		return response;
	}

	// GET
	@GetMapping(value = "/list", produces = "application/json")
	public BaseResponseDto<List<SocietaDto>> list() {
		List<SocietaDto> docenti = societaService.getAll();

		BaseResponseDto<List<SocietaDto>> res = new BaseResponseDto<List<SocietaDto>>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("ESEGUITO");
		res.setResponse(docenti);

		return res;
	}

	// GET BY ID
	@GetMapping(produces = "application/json", path = "/{id}")
	public BaseResponseDto<SocietaDto> readById(@PathVariable int id) {
		BaseResponseDto<SocietaDto> response = new BaseResponseDto<>();

		SocietaDto Societa = societaService.getById(id);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(Societa);

		return response;
	}

	// MODIFICA
	@PatchMapping(consumes = "application/json", produces = "application/json", path = "/update/{id}")
	public BaseResponseDto<Boolean> updateById(@RequestBody SocietaDto SocietaDto, @PathVariable int id) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value());
		risposta.setMessage("PATCH_SUCCESFUL");
		risposta.setResponse(societaService.modify(SocietaDto, id));

		return risposta;
	}

	// INSERT
	@PostMapping(consumes = "application/json", produces = "application/json", path = "/save") // input = consumes +
																								// output = produces
	public BaseResponseDto<Boolean> inserisci(@RequestBody SocietaDto dto) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("POST_SUCCESFUL");
		risposta.setResponse(societaService.insert(dto));

		return risposta;
	}

	// DELETE
	@DeleteMapping(produces = "application/json", path = "/delete/{id}")
	public BaseResponseDto<Boolean> deleteById(@PathVariable int id) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("DELETE_SUCCESFUL");
		risposta.setResponse(societaService.delete(id));

		return risposta;
	}

}
