package it.its.MMMM.Project.Work.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.its.MMMM.Project.Work.dao.DocenteDao;


@Repository
public interface DocenteRepository extends JpaRepository<DocenteDao, Integer>
{

	List<DocenteDao> findByTitoloIsContaining(String titolo);
	
	@Query("SELECT COUNT(d) FROM DocenteDao d")
	long getCountDocenti();
}