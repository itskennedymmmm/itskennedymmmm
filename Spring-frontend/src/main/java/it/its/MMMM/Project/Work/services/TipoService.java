package it.its.MMMM.Project.Work.services;

import java.util.List;

import it.its.MMMM.Project.Work.dao.TipoDao;
import it.its.MMMM.Project.Work.dto.TipoDto;

public interface TipoService {

	public List<TipoDto> getAll();
	public List<TipoDto> getAll(int pagina ,String ordina);
	public Boolean delete(int id);
	public Boolean modify(TipoDto dto, int id);
	public Boolean insert(TipoDto dto);
	public TipoDto getById(int id);
	void dtoToDao(TipoDto dto, TipoDao dao);
	void daoToDto(TipoDao dao, TipoDto dto);
}
