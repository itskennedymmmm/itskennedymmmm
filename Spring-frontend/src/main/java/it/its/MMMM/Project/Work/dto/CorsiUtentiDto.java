package it.its.MMMM.Project.Work.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CorsiUtentiDto {
	
	private Integer corsi_utenti_id;
	private Integer corso;
	private Integer dipendente;
	private Integer durata;
	
	//Dipendente
	private Integer matricola;
	private String cognome;
	private String nome;
	private String titolo_studio;
	
	//Corso
	private String descrizione;
	private String note;
	private String ente;
	
	

}
