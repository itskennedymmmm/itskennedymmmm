package it.its.MMMM.Project.Work.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.its.MMMM.Project.Work.dao.CorsiUtentiDao;
import it.its.MMMM.Project.Work.dao.ValutazioniUtentiDao;

@Repository
public interface ValutazioniUtentiRepository extends JpaRepository<ValutazioniUtentiDao, Integer>{

	
	//trova per dipendente
	List<ValutazioniUtentiDao> findByDipendenteIs(Integer numero);//trova per dipendente
}
