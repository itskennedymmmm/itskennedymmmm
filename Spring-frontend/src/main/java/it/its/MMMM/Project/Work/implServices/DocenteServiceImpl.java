package it.its.MMMM.Project.Work.implServices;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.MMMM.Project.Work.dao.DocenteDao;
import it.its.MMMM.Project.Work.dto.DocenteDto;
import it.its.MMMM.Project.Work.repository.DocenteRepository;
import it.its.MMMM.Project.Work.services.DocenteService;

@Service
@Transactional
public class DocenteServiceImpl implements DocenteService {

	@Autowired
	DocenteRepository docenteRepository;

	// GET Pagination
	@Override
	public List<DocenteDto> getAll(int pagina, String ordina) {
		List<DocenteDto> t = new ArrayList<>();
		for (DocenteDao d : docenteRepository.findAll(PageRequest.of(pagina, 10))) {
			DocenteDto dto = new DocenteDto();
			daoToDto(d, dto);
			t.add(dto);
		}
		return t;

	}

//	//GET ALL
	@Override
	public List<DocenteDto> getAll() {
		List<DocenteDao> lista = docenteRepository.findAll();
		List<DocenteDto> listaDto = new ArrayList<DocenteDto>();
		for (DocenteDao u : lista) {
			DocenteDto dto = new DocenteDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

	// GET BY ID
	public DocenteDto getById(int id) {
		try {
			DocenteDto dto = new DocenteDto();
			daoToDto(docenteRepository.getOne(id), dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	// DELETE
	@Override
	public Boolean deleteDocente(int id) {
		try {
			docenteRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// MODIFY
	@Override
	public Boolean modify(DocenteDto dto, int idP) {
		DocenteDao dao = docenteRepository.getOne(idP);
		dtoToDao(dto, dao);
		try {
			docenteRepository.save(dao);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// INSERT
	@Override
	public Boolean insert(DocenteDto dto) {
		DocenteDao dao = new DocenteDao();
		try {
			dtoToDao(dto, dao);
			docenteRepository.save(dao);
			return true;
		} catch (Exception e) {

			return false;
		}
	}


	@Override
	public List<DocenteDto> findBySomething(String parola) {
		
		List<DocenteDao> lista = docenteRepository.findByTitoloIsContaining(parola);
		List<DocenteDto> listaDto = new ArrayList<DocenteDto>();
		for (DocenteDao u : lista) {
			DocenteDto dto = new DocenteDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

	
	// helpMethod dao to dto
	@Override
	public void daoToDto(DocenteDao dao, DocenteDto dto) {
		dto.setDocente_id(dao.getDocente_id());
		dto.setTitolo(dao.getTitolo());
		dto.setRagione_sociale(dao.getRagione_sociale());
		dto.setIndirizzo(dao.getIndirizzo());
		dto.setLocalita(dao.getLocalita());
		dto.setProvincia(dao.getProvincia());
		dto.setNazione(dao.getNazione());
		dto.setTelefono(dao.getTelefono());
		dto.setFax(dao.getFax());
		dto.setPartita_iva(dao.getPartita_iva());
		dto.setReferente(dao.getReferente());
	}

	// helpMethod dto to dao
	@Override
	public void dtoToDao(DocenteDto dto, DocenteDao dao) { // metodo DTO TO DAO
		dao.setDocente_id(dto.getDocente_id());
		dao.setTitolo(dto.getTitolo());
		dao.setRagione_sociale(dto.getRagione_sociale());
		dao.setIndirizzo(dto.getIndirizzo());
		dao.setLocalita(dto.getLocalita());
		dao.setProvincia(dto.getProvincia());
		dao.setNazione(dto.getNazione());
		dao.setTelefono(dto.getTelefono());
		dao.setFax(dto.getFax());
		dao.setPartita_iva(dto.getPartita_iva());
		dao.setReferente(dto.getReferente());

		// dao.getDocenteDao().get

	}
	@Override
	public long getCountDocenti() {
		System.out.println(docenteRepository.getCountDocenti());
		return docenteRepository.getCountDocenti();
	}


}
