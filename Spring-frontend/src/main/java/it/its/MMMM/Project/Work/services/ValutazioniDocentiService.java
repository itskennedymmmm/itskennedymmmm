package it.its.MMMM.Project.Work.services;

import java.util.List;

import it.its.MMMM.Project.Work.dao.ValutazioniDocentiDao;
import it.its.MMMM.Project.Work.dto.ValutazioniDocentiDto;

public interface ValutazioniDocentiService {
	
	public List<ValutazioniDocentiDto> getAll(int pagina ,String ordina);
	public List<ValutazioniDocentiDto> getAll();
	public Boolean delete(int id);
	public Boolean modify(ValutazioniDocentiDto dto, int id);
	public Boolean insert(ValutazioniDocentiDto dto);
	public ValutazioniDocentiDto getById(int id);
	void dtoToDao(ValutazioniDocentiDto dto, ValutazioniDocentiDao dao);
	public void daoToDto(ValutazioniDocentiDao dao, ValutazioniDocentiDto dto);
	
	public List<ValutazioniDocentiDto>  findByCorso(Integer numero);

}
