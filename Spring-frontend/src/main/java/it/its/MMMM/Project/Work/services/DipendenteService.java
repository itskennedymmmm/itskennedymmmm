package it.its.MMMM.Project.Work.services;

import java.util.List;

import it.its.MMMM.Project.Work.dao.DipendenteDao;
import it.its.MMMM.Project.Work.dto.DipendenteDto;

public interface DipendenteService {
	
//	public List<DipendenteDto> getAll(Integer pageNo, Integer pageSize, String sortBy);
	public List<DipendenteDto> getAll(int pagina ,String ordina);
	public void daoToDto(DipendenteDao dao, DipendenteDto dto);
	public DipendenteDto getById(int id);
	
	public long getCountDipendenti();

}
