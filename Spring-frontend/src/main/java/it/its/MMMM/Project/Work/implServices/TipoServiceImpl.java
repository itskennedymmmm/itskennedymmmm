package it.its.MMMM.Project.Work.implServices;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.MMMM.Project.Work.dao.TipoDao;
import it.its.MMMM.Project.Work.dto.TipoDto;
import it.its.MMMM.Project.Work.repository.TipoRepository;
import it.its.MMMM.Project.Work.services.TipoService;

@Service
@Transactional
public class TipoServiceImpl implements TipoService{

	@Autowired
	TipoRepository tipoRepository;
	
	//GET ALL
	@Override
	public List<TipoDto> getAll() {
		List<TipoDao> lista = tipoRepository.findAll();
		List<TipoDto> listaDto = new ArrayList<TipoDto>();
		for (TipoDao u : lista) {
			TipoDto dto = new TipoDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

	//GET ALL Pagination
	@Override
	public List<TipoDto> getAll(int pagina, String ordina) {
		List<TipoDto> t = new ArrayList<>();
		for (TipoDao dao : tipoRepository.findAll(PageRequest.of(pagina, 10))) {
			TipoDto dto = new TipoDto();
			daoToDto(dao, dto);
			t.add(dto);
		}
		return t;
	}

	// DELETE
	@Override
	public Boolean delete(int id) {
		try {
			tipoRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// MODIFY
	@Override
	public Boolean modify(TipoDto dto, int id) {
		TipoDao dao = tipoRepository.getOne(id);
		dtoToDao(dto, dao);
		try {
			tipoRepository.save(dao);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// INSERT
	@Override
	public Boolean insert(TipoDto dto) {
		TipoDao dao = new TipoDao();
		try {
			dtoToDao(dto, dao);
			tipoRepository.save(dao);
			return true;
		} catch (Exception e) {

			return false;
		}
	}

	// GET BY ID
	@Override
	public TipoDto getById(int id) {
		try {
			TipoDto dto = new TipoDto();
			daoToDto(tipoRepository.getOne(id), dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}
	

	// helpMethod dto to dao
	@Override
	public void dtoToDao(TipoDto dto, TipoDao dao) {
		dao.setTipo_id(dto.getTipo_id());
		dao.setDescrizione(dto.getDescrizione());
		
	}
	// helpMethod dao to dto
	@Override
	public void daoToDto(TipoDao dao, TipoDto dto) {
		dto.setTipo_id(dao.getTipo_id());
		dto.setDescrizione(dao.getDescrizione());
		
	}

}
