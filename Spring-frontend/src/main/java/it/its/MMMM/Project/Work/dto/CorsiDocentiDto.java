package it.its.MMMM.Project.Work.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CorsiDocentiDto {
	
	private Integer corsi_docenti_id;
	private Integer corso;
	private Integer docente;
	private Integer interno;
	
	// corsi
	private String descrizione;
	private String durata;
	private String luogo;
	private String ente;
	private Date data_erogazione;
	
	
	// docente
	private String titolo;
	private String ragione_sociale;
	private String localita;
	private String telefono;
	
	// dipendente
	private Integer matricola;
	private String cognome;
	private String nome;
	private String titolo_studio;

}
