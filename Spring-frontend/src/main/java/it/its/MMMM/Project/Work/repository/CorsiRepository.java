package it.its.MMMM.Project.Work.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import it.its.MMMM.Project.Work.dao.CorsiDao;

@Repository
public interface CorsiRepository extends JpaRepository<CorsiDao, Integer>{

	@Query("SELECT COUNT(d) FROM CorsiDao d")
	long getCountCorsi();
}
