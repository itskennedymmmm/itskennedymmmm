package it.its.MMMM.Project.Work.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DipendenteDto {

	private Integer dipendenti_id;
	private Integer matricola;
	private Integer societa;
	private String cognome;
	private String nome;
	private String sesso;
	private Date data_nascita ;
	private String luogo_nascita;
	private String stato_civile;
	private String titolo_studio;
	private String conseguito_presso;
	private Integer anno_conseguimento;
	private String tipo_dipendente;
	private String qualifica;
	private String livello;
	private Date data_assunzione;
	private Integer responsabile_risorsa;
	private Integer responsabile_area;
	private Date data_fine_rapporto;
	private Date data_scadenza_contratto;
	
	//SOCIETA
	private String ragione_sociale;
	private String localita;
	private String telefono;

}
