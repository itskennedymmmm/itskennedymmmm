/* -> Sviluppatori : Mirco Pastorino - Mattia Zogno - Matteo Solivo - Marco Fichera <-
 * 
 * La classe CorsiUtentiController ha lo scopo di esporre i servizi necessari per il nostro applicattivo in relazione alla gestione dei corsisti. 
 * 
 * Metodi presenti : 
 * getAll            --> ritorna una lista di oggetti corsiUtenti (gestito con paginatio)
 * list              --> ritorna una lista di oggetti corsiUtenti (senza pagination)
 * readById          --> (getCorsoById)
 * updateById        --> (MODIFICA)
 * insert            --> (SAVE)
 * deleteById        --> (DELETE)
 * findByInterno     --> ritorna il docente del corso se interno
 *  */
package it.its.MMMM.Project.Work.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import it.its.MMMM.Project.Work.dao.CorsiUtentiDao;
import it.its.MMMM.Project.Work.dto.BaseResponseDto;
import it.its.MMMM.Project.Work.dto.CorsiUtentiDto;
import it.its.MMMM.Project.Work.services.CorsiUtentiService;

@RestController
@RequestMapping(value = "project/corsiUtenti")
public class CorsiUtentiController {

	@Autowired
	CorsiUtentiService corsiUtentiService;

	// GET Pagination
	@GetMapping(produces = "application/json", path = "/page/{pagina}")
	public BaseResponseDto<List<CorsiUtentiDto>> getAll(@PathVariable int pagina,
			@RequestParam(required = false) String ordina) {
		BaseResponseDto<List<CorsiUtentiDto>> response = new BaseResponseDto<>();

		List<CorsiUtentiDto> corsi;

		if (ordina == null) {
			ordina = "corsi_utenti_id";
		}

		corsi = corsiUtentiService.getAll(pagina, ordina);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		response.setResponse(corsi);
		return response;
	}

	// GET
	@GetMapping(value = "/list", produces = "application/json")
	public BaseResponseDto<List<CorsiUtentiDto>> list() {

		List<CorsiUtentiDto> corsi = corsiUtentiService.getAll();
		BaseResponseDto<List<CorsiUtentiDto>> res = new BaseResponseDto<List<CorsiUtentiDto>>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("ESEGUITO");
		res.setResponse(corsi);

		return res;
	}

	// GET BY ID
	@GetMapping(produces = "application/json", path = "/{idC}")
	public BaseResponseDto<CorsiUtentiDao> getById(@PathVariable int idC) {

		BaseResponseDto<CorsiUtentiDao> response = new BaseResponseDto<>();
		CorsiUtentiDto corso = corsiUtentiService.getById(idC);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corso);

		return response;
	}

	// MODIFICA
	@PatchMapping(consumes = "application/json", produces = "application/json", path = "/update/{idC}")
	public BaseResponseDto<Boolean> updateById(@RequestBody CorsiUtentiDto corsoUtentiDto, @PathVariable int idC) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value());
		risposta.setMessage("PATCH_SUCCESFUL");
		risposta.setResponse(corsiUtentiService.modify(corsoUtentiDto, idC));

		return risposta;
	}

	// INSERT
	@PostMapping(consumes = "application/json", produces = "application/json", path = "/save") // input = consumes +
																								// output = produces
	public BaseResponseDto<Boolean> insert(@RequestBody CorsiUtentiDto dto) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("POST_SUCCESFUL");
		risposta.setResponse(corsiUtentiService.insert(dto));

		return risposta;
	}

	// DELETE
	@DeleteMapping(produces = "application/json", path = "/delete/{idC}")
	public BaseResponseDto<Boolean> deleteById(@PathVariable int idC) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("DELETE_SUCCESFUL");
		risposta.setResponse(corsiUtentiService.delete(idC));

		return risposta;
	}
	
	//Query   http://localhost:8090/project/valutazioni/utenti/findD?parola=1
	@GetMapping( produces = "application/json", value="/findD")
	public BaseResponseDto<List<CorsiUtentiDto>> findByInterno(@RequestParam("parola") Integer numero){
		
		BaseResponseDto<List<CorsiUtentiDto>> response = new BaseResponseDto<List<CorsiUtentiDto>>();
		List<CorsiUtentiDto> docente= corsiUtentiService.findByDipendente(numero);
		
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docente);
		
		return response;
	}
}
