package it.its.MMMM.Project.Work.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.its.MMMM.Project.Work.dao.TipologieDao;

@Repository
public interface TipologieRepository extends JpaRepository<TipologieDao, Integer> {

}
