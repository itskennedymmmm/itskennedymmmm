package it.its.MMMM.Project.Work.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TipologieDto {

	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 @Column (name = "tipologia_id") 
	 private Integer tipologia_id;
	 
	 @Column (name = "descrizione")
	 private String descrizione;
}
