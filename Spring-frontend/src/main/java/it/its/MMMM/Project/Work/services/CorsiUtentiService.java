package it.its.MMMM.Project.Work.services;

import java.util.List;
import it.its.MMMM.Project.Work.dao.CorsiUtentiDao;
import it.its.MMMM.Project.Work.dto.CorsiUtentiDto;

public interface CorsiUtentiService {
	
	public List<CorsiUtentiDto> getAll(int pagina ,String ordina);
	public List<CorsiUtentiDto> getAll();
	public Boolean delete(int id);
	public Boolean modify(CorsiUtentiDto dto, int id);
	public Boolean insert(CorsiUtentiDto dto);
	public CorsiUtentiDto getById(int id);
	void dtoToDao(CorsiUtentiDto dto, CorsiUtentiDao dao);
	public void daoToDto(CorsiUtentiDao dao, CorsiUtentiDto dto);
	
	public List<CorsiUtentiDto> findByDipendente(Integer parola);

}
