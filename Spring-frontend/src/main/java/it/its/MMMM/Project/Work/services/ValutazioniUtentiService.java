package it.its.MMMM.Project.Work.services;

import java.util.List;

import it.its.MMMM.Project.Work.dao.ValutazioniUtentiDao;
import it.its.MMMM.Project.Work.dto.CorsiUtentiDto;
import it.its.MMMM.Project.Work.dto.ValutazioniUtentiDto;

public interface ValutazioniUtentiService {
	
	public List<ValutazioniUtentiDto> getAll(int pagina ,String ordina);
	public List<ValutazioniUtentiDto> getAll();
	public Boolean delete(int id);
	public Boolean modify(ValutazioniUtentiDto dto, int id);
	public Boolean insert(ValutazioniUtentiDto dto);
	public ValutazioniUtentiDto getById(int id);
	void dtoToDao(ValutazioniUtentiDto dto, ValutazioniUtentiDao dao);
	public void daoToDto(ValutazioniUtentiDao dao, ValutazioniUtentiDto dto);
	
	public List<ValutazioniUtentiDto> findByDipendente(Integer numero);

}
