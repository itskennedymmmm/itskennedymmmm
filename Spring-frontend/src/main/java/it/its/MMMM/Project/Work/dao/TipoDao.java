package it.its.MMMM.Project.Work.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import lombok.Data;

@Entity
@Table (name = "tipi")
@Data
public class TipoDao {

	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 @Column (name = "tipo_id") 
	 private Integer tipo_id;
	 
	 @Column (name = "descrizione")
	 private String descrizione;

	 @OneToMany(targetEntity = CorsiDao.class, mappedBy = "tipoDao", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	 private List<CorsiDao> tipoDao = new ArrayList<>();
	 
	
}
