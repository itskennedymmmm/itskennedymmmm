package it.its.MMMM.Project.Work.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.its.MMMM.Project.Work.dao.CorsiDocentiDao;

@Repository
public interface CorsiDocentiRepository extends JpaRepository<CorsiDocentiDao, Integer>{
	
	List<CorsiDocentiDao> findByDocenteIs(Integer titolo);//trova Docenti 
	List<CorsiDocentiDao> findByInternoIs(Integer titolo);//trova Interno 
	List<CorsiDocentiDao> findByCorsoIs(Integer titolo);//trova Interno 
	
	@Query("SELECT COUNT(d) FROM CorsiUtentiDao d where d.corso = ?1")
	long getCountCorsisti(Integer corso);
}
