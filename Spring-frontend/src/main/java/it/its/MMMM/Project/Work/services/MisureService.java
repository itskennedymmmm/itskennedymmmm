package it.its.MMMM.Project.Work.services;

import java.util.List;

import it.its.MMMM.Project.Work.dao.MisureDao;
import it.its.MMMM.Project.Work.dto.MisureDto;

public interface MisureService {
	
	public List<MisureDto> getAll();
	public List<MisureDto> getAll(int pagina ,String ordina);
	public Boolean delete(int id);
	public Boolean modify(MisureDto dto, int id);
	public Boolean insert(MisureDto dto);
	public MisureDto getById(int id);
    void dtoToDao(MisureDto dto, MisureDao dao);
	void daoToDto(MisureDao dao, MisureDto dto);
}
