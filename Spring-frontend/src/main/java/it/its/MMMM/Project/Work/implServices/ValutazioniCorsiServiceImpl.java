package it.its.MMMM.Project.Work.implServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.MMMM.Project.Work.dao.ValutazioniCorsiDao;
import it.its.MMMM.Project.Work.dao.ValutazioniUtentiDao;
import it.its.MMMM.Project.Work.dto.ValutazioniCorsiDto;
import it.its.MMMM.Project.Work.dto.ValutazioniUtentiDto;
import it.its.MMMM.Project.Work.repository.ValutazioniCorsiRepository;
import it.its.MMMM.Project.Work.services.ValutazioniCorsiService;

@Service
@Transactional
public class ValutazioniCorsiServiceImpl implements ValutazioniCorsiService {

	@Autowired
	ValutazioniCorsiRepository valutazioniCorsiRepository;

	// GET Pagination
	@Override
	public List<ValutazioniCorsiDto> getAll(int pagina, String ordina) {
		List<ValutazioniCorsiDto> lista = new ArrayList<>();
		for (ValutazioniCorsiDao d : valutazioniCorsiRepository.findAll(PageRequest.of(pagina, 10))) {
			ValutazioniCorsiDto dto = new ValutazioniCorsiDto();
			daoToDto(d, dto);
			lista.add(dto);
		}
		return lista;

	}

	// GET ALL
	@Override
	public List<ValutazioniCorsiDto> getAll() {
		List<ValutazioniCorsiDao> lista = valutazioniCorsiRepository.findAll();
		List<ValutazioniCorsiDto> listaDto = new ArrayList<ValutazioniCorsiDto>();
		for (ValutazioniCorsiDao u : lista) {
			ValutazioniCorsiDto dto = new ValutazioniCorsiDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

	// DELETE
	@Override
	public Boolean delete(int id) {
		try {
			valutazioniCorsiRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// MODIFY
	@Override
	public Boolean modify(ValutazioniCorsiDto dto, int id) {
		ValutazioniCorsiDao dao = valutazioniCorsiRepository.getOne(id);
		dtoToDao(dto, dao);
		try {
			valutazioniCorsiRepository.save(dao);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// INSERT
	@Override
	public Boolean insert(ValutazioniCorsiDto dto) {
		ValutazioniCorsiDao dao = new ValutazioniCorsiDao();
		try {
			dtoToDao(dto, dao);
			valutazioniCorsiRepository.save(dao);
			return true;
		} catch (Exception e) {

			return false;
		}
	}

	// GETBYID
	@Override
	public ValutazioniCorsiDto getById(int id) {
		try {
			ValutazioniCorsiDto dto = new ValutazioniCorsiDto();
			daoToDto(valutazioniCorsiRepository.getOne(id), dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void dtoToDao(ValutazioniCorsiDto dto, ValutazioniCorsiDao dao) {

		dao.setIdValutazioneCorso(dto.getIdValutazioneCorso());
		dao.setCorso(dto.getCorso());
		dao.setCriterio(dto.getCriterio());
		JSONObject json = new JSONObject(dto.getValore());
	    dao.setValore(json.toString());

		// Attributi visualizzati dei corsi
		if (dao.getCorsiDao() != null) {
			dao.getCorsiDao().setDescrizione(dto.getDescrizione());
		}

	}

	@Override
	public void daoToDto(ValutazioniCorsiDao dao, ValutazioniCorsiDto dto) {

		dto.setIdValutazioneCorso(dao.getIdValutazioneCorso());
		dto.setCorso(dao.getCorso());
		dto.setCriterio(dao.getCriterio());
		dto.setValore((HashMap<String,Object>) new JSONObject(dao.getValore()).toMap());

		// Attributi visualizzati dei corsi
		if (dao.getCorsiDao() != null) {
			dto.setDescrizione(dao.getCorsiDao().getDescrizione());
			dto.setEnte(dao.getCorsiDao().getEnte());
			dto.setLuogo(dao.getCorsiDao().getLuogo());
			dto.setNote(dao.getCorsiDao().getNote());
			dto.setDurata(dao.getCorsiDao().getDurata());
			dto.setData_erogazione(dao.getCorsiDao().getData_erogazione());
			dto.setData_chiusura(dao.getCorsiDao().getData_chiusura());
			dto.setData_censimento(dao.getCorsiDao().getData_censimento());
		}
	}
	
	@Override
	public List<ValutazioniCorsiDto> findByCorso(Integer numero) {
		List<ValutazioniCorsiDao> lista = valutazioniCorsiRepository.findByCorsoIs(numero);
		List<ValutazioniCorsiDto> listaDto = new ArrayList<ValutazioniCorsiDto>();
		for (ValutazioniCorsiDao u : lista) {
			ValutazioniCorsiDto dto = new ValutazioniCorsiDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;			
	}

}
