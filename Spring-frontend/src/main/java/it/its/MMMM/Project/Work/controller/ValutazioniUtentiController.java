/* -> Sviluppatori : Mirco Pastorino - Mattia Zogno - Matteo Solivo - Marco Fichera <-
 * 
 * La classe ValutazioniDocentiController ha lo scopo di esporre i servizi necessari per il nostro applicattivo in relazione alla gestione delle valutazioni 
 * degli utenti. 
 * 
 * Metodi presenti : 
 * getAll            --> ritorna una lista di oggetti valutazioniUtenti (gestito con pagination)
 * list              --> ritorna una lista di oggetti valutazioniUtenti (senza pagination)
 * readById          --> (getValutazioniUtentiById)
 * updateById        --> (MODIFICA)
 * insert            --> (SAVE)
 * deleteById        --> (DELETE)
 *  
 *  */
package it.its.MMMM.Project.Work.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.MMMM.Project.Work.dao.ValutazioniUtentiDao;
import it.its.MMMM.Project.Work.dto.BaseResponseDto;
import it.its.MMMM.Project.Work.dto.ValutazioniUtentiDto;
import it.its.MMMM.Project.Work.services.ValutazioniUtentiService;

@RestController
@RequestMapping(value = "project/valutazioni/utenti")
public class ValutazioniUtentiController {

	@Autowired
	ValutazioniUtentiService valutazioniUtentiService;

	@GetMapping("/ping")
	public String save() {
		return "PROVA DI PING...AVVENUTA CON SUCCESSO";
	}

	// GET Pagination
	@GetMapping(produces = "application/json", path = "/page/{pagina}")
	public BaseResponseDto<List<ValutazioniUtentiDto>> getAll(@PathVariable int pagina,
			@RequestParam(required = false) String ordina) {
		BaseResponseDto<List<ValutazioniUtentiDto>> response = new BaseResponseDto<>();

		List<ValutazioniUtentiDto> list;

		if (ordina == null) {
			ordina = "idValutazioneUtente";
		}

		list = valutazioniUtentiService.getAll(pagina, ordina);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		response.setResponse(list);
		return response;
	}

	// GET
	@GetMapping(value = "/list", produces = "application/json")
	public BaseResponseDto<List<ValutazioniUtentiDto>> list() {
		List<ValutazioniUtentiDto> docenti = valutazioniUtentiService.getAll();

		BaseResponseDto<List<ValutazioniUtentiDto>> res = new BaseResponseDto<List<ValutazioniUtentiDto>>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("ESEGUITO");
		res.setResponse(docenti);

		return res;
	}

	// GET BY ID
	@GetMapping(produces = "application/json", path = "/{id}")
	public BaseResponseDto<ValutazioniUtentiDao> readById(@PathVariable int id) {
		BaseResponseDto<ValutazioniUtentiDao> response = new BaseResponseDto<>();

		ValutazioniUtentiDto docente = valutazioniUtentiService.getById(id);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docente);

		return response;
	}

	// MODIFICA
	@PatchMapping(consumes = "application/json", produces = "application/json", path = "/update/{idD}")
	public BaseResponseDto<Boolean> updateById(@RequestBody ValutazioniUtentiDto valutazioniUtentiDto,
			@PathVariable int idD) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value());
		risposta.setMessage("PATCH_SUCCESFUL");
		risposta.setResponse(valutazioniUtentiService.modify(valutazioniUtentiDto, idD));

		return risposta;
	}

	// INSERT
	@PostMapping(consumes = "application/json", produces = "application/json", path = "/save") // input = consumes +
																								// output = produces
	public BaseResponseDto<Boolean> inserisci(@RequestBody ValutazioniUtentiDto dto) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("POST_SUCCESFUL");
		risposta.setResponse(valutazioniUtentiService.insert(dto));

		return risposta;
	}

	// DELETE
	@DeleteMapping(produces = "application/json", path = "/delete/{idD}")
	public BaseResponseDto<Boolean> deleteById(@PathVariable int id) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("DELETE_SUCCESFUL");
		risposta.setResponse(valutazioniUtentiService.delete(id));

		return risposta;
	}

	// Query http://localhost:8090/project/
	@GetMapping(produces = "application/json", value = "/findD")
	public BaseResponseDto<List<ValutazioniUtentiDto>> findByDipendendente(@RequestParam("parola") Integer numero) {

		BaseResponseDto<List<ValutazioniUtentiDto>> response = new BaseResponseDto<List<ValutazioniUtentiDto>>();
		List<ValutazioniUtentiDto> valUtente = valutazioniUtentiService.findByDipendente(numero);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(valUtente);

		return response;
	}

}
