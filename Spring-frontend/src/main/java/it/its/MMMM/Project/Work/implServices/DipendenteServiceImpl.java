package it.its.MMMM.Project.Work.implServices;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.MMMM.Project.Work.dao.DipendenteDao;
import it.its.MMMM.Project.Work.dto.DipendenteDto;
import it.its.MMMM.Project.Work.repository.DipendenteRepository;
import it.its.MMMM.Project.Work.services.DipendenteService;

@Service
@Transactional
public class DipendenteServiceImpl implements DipendenteService {

	@Autowired
	DipendenteRepository dipendenteRepository;

	// GET Pagination
	@Override
	public List<DipendenteDto> getAll(int pagina, String ordina) {
		List<DipendenteDto> lista = new ArrayList<>();
		for (DipendenteDao d : dipendenteRepository.findAll(PageRequest.of(pagina, 10))) {
			DipendenteDto dto = new DipendenteDto();
			daoToDto(d, dto);
			lista.add(dto);
		}
		return lista;

	}

	// HelpMethod dao to dto
	@Override
	public void daoToDto(DipendenteDao dao, DipendenteDto dto) {

		dto.setDipendenti_id(dao.getDipendenti_id());
		dto.setMatricola(dao.getMatricola());
		dto.setSocieta(dao.getSocieta());
		dto.setCognome(dao.getCognome());
		dto.setNome(dao.getNome());
		dto.setSesso(dao.getSesso());
		dto.setData_nascita(dao.getData_nascita());
		dto.setLuogo_nascita(dao.getLuogo_nascita());
		dto.setStato_civile(dao.getStato_civile());
		dto.setTitolo_studio(dao.getTitolo_studio());
		dto.setConseguito_presso(dao.getConseguito_presso());
		dto.setAnno_conseguimento(dao.getAnno_conseguimento());
		dto.setTipo_dipendente(dao.getTipo_dipendente());
		dto.setQualifica(dao.getQualifica());
		dto.setLivello(dao.getLivello());
		dto.setData_assunzione(dao.getData_assunzione());
		dto.setResponsabile_risorsa(dao.getResponsabile_risorsa());
		dto.setResponsabile_area(dao.getResponsabile_area());
		dto.setData_fine_rapporto(dao.getData_fine_rapporto());
		dto.setData_scadenza_contratto(dao.getData_scadenza_contratto());
		
		//Attributi visualizzati delle societa
		if(dao.getSocietaDao() !=null) {
			dto.setRagione_sociale(dao.getSocietaDao().getRagione_sociale());
			dto.setLocalita(dao.getSocietaDao().getLocalita());
			dto.setTelefono(dao.getSocietaDao().getTelefono());
		}

	}

	// GET BY ID
	@Override
	public DipendenteDto getById(int id) {
		try {
			DipendenteDto dto = new DipendenteDto();
			daoToDto(dipendenteRepository.getOne(id), dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public long getCountDipendenti() {
		return dipendenteRepository.getCountDipendenti();
	}


}
