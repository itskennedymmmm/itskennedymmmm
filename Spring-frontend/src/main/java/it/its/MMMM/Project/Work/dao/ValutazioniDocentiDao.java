package it.its.MMMM.Project.Work.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table (name = "valutazioni_docenti")
@Data
public class ValutazioniDocentiDao {
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY) //ci deve andare
	 @Column (name = "valutazioni_docenti_id")
	 private Integer idValutazioneDocente;
	 
	 @Column (name = "corso")
	 private Integer corso;
	 
	 @Column (name = "docente")
	 private Integer docente;

	 @Column (name = "criterio")
	 private String criterio;
	 
	 @Column (name = "valore")
	 private String valore;
	 
	 @ManyToOne
	 @EqualsAndHashCode.Exclude
	 @JoinColumn(name = "corso", referencedColumnName = "corso_id", insertable = false, updatable = false)
	 @JsonBackReference
	 private CorsiDao corsiDao;
	 
	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "docente", referencedColumnName = "docente_id", insertable = false, updatable = false)
	@JsonBackReference
	private DocenteDao docenteDao;
}
