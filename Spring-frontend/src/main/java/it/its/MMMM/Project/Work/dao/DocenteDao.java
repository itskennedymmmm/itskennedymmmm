package it.its.MMMM.Project.Work.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "docenti") // Nome tabella
@Data
public class DocenteDao { // I nomi dei campi e delle tabelle sono IDENTICI a quelli di questa classe.

	@Id
	@Column(name = "docente_id")
	private int docente_id;

	@Column(name = "titolo")
	private String titolo;

	@Column(name = "ragione_sociale")
	private String ragione_sociale;

	@Column(name = "indirizzo")
	private String indirizzo;

	@Column(name = "localita")
	private String localita;

	@Column(name = "provincia")
	private String provincia;

	@Column(name = "nazione")
	private String nazione;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "fax")
	private String fax;

	@Column(name = "partita_iva")
	private String partita_iva;

	@Column(name = "referente")
	private String referente;

	@OneToMany(targetEntity = CorsiDocentiDao.class, mappedBy = "docenteDao", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<CorsiDocentiDao> docenteDao = new ArrayList<>();

	@OneToMany(targetEntity = ValutazioniDocentiDao.class, mappedBy = "docenteDao", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ValutazioniDocentiDao> valutazioneDocenteDao = new ArrayList<>();

}