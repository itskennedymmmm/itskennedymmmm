package it.its.MMMM.Project.Work.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.its.MMMM.Project.Work.dao.ValutazioniDocentiDao;

@Repository
public interface ValutazioniDocentiRepository extends JpaRepository<ValutazioniDocentiDao, Integer>{

	//trova per corso
	List<ValutazioniDocentiDao> findByCorsoIs(Integer numero);
}
