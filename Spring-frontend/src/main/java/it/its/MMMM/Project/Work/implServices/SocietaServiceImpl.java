package it.its.MMMM.Project.Work.implServices;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.MMMM.Project.Work.dao.SocietaDao;
import it.its.MMMM.Project.Work.dto.SocietaDto;
import it.its.MMMM.Project.Work.repository.SocietaRepository;
import it.its.MMMM.Project.Work.services.SocietaService;

@Service
@Transactional
public class SocietaServiceImpl implements SocietaService{
	
	@Autowired
	SocietaRepository societaRepository;

	// GET Pagination
	@Override
	public List<SocietaDto> getAll(int pagina, String ordina) {
		List<SocietaDto> t = new ArrayList<>();
		for (SocietaDao d : societaRepository.findAll(PageRequest.of(pagina, 10))) {
			SocietaDto dto = new SocietaDto();
			daoToDto(d, dto);
			t.add(dto);
		}
		return t;

	}

//	//GET ALL
	@Override
	public List<SocietaDto> getAll() {
		List<SocietaDao> lista = societaRepository.findAll();
		List<SocietaDto> listaDto = new ArrayList<SocietaDto>();
		for (SocietaDao u : lista) {
			SocietaDto dto = new SocietaDto();
			daoToDto(u, dto);
			listaDto.add(dto);
		}
		return listaDto;
	}

	// GET BY ID
	public SocietaDto getById(int id) {
		try {
			SocietaDto dto = new SocietaDto();
			daoToDto(societaRepository.getOne(id), dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	// DELETE
	@Override
	public Boolean delete(int id) {
		try {
			societaRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// MODIFY
	@Override
	public Boolean modify(SocietaDto dto, int idP) {
		SocietaDao dao = societaRepository.getOne(idP);
		dtoToDao(dto, dao);
		try {
			societaRepository.save(dao);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// INSERT
	@Override
	public Boolean insert(SocietaDto dto) {
		SocietaDao dao = new SocietaDao();
		try {
			dtoToDao(dto, dao);
			societaRepository.save(dao);
			return true;
		} catch (Exception e) {

			return false;
		}
	}

	// helpMethod dao to dto
	@Override
	public void daoToDto(SocietaDao dao, SocietaDto dto) {
		dto.setSocieta_id(dao.getSocieta_id());
		dto.setRagione_sociale(dao.getRagione_sociale());
		dto.setIndirizzo(dao.getIndirizzo());
		dto.setLocalita(dao.getLocalita());
		dto.setProvincia(dao.getProvincia());
		dto.setNazione(dao.getNazione());
		dto.setTelefono(dao.getTelefono());
		dto.setFax(dao.getFax());
		dto.setPartita_iva(dao.getPartita_iva());
	}

	// helpMethod dto to dao
	@Override
	public void dtoToDao(SocietaDto dto, SocietaDao dao) {
		dao.setSocieta_id(dto.getSocieta_id());
		dao.setRagione_sociale(dto.getRagione_sociale());
		dao.setIndirizzo(dto.getIndirizzo());
		dao.setLocalita(dto.getLocalita());
		dao.setProvincia(dto.getProvincia());
		dao.setNazione(dto.getNazione());
		dao.setTelefono(dto.getTelefono());
		dao.setFax(dto.getFax());
		dao.setPartita_iva(dto.getPartita_iva());

	}

}
