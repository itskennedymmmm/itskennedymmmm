package it.its.MMMM.Project.Work.services;

import java.util.List;

import it.its.MMMM.Project.Work.dao.ValutazioniCorsiDao;
import it.its.MMMM.Project.Work.dto.ValutazioniCorsiDto;
import it.its.MMMM.Project.Work.dto.ValutazioniUtentiDto;

public interface ValutazioniCorsiService {

	public List<ValutazioniCorsiDto> getAll(int pagina ,String ordina);
	public List<ValutazioniCorsiDto> getAll();
	public Boolean delete(int id);
	public Boolean modify(ValutazioniCorsiDto dto, int id);
	public Boolean insert(ValutazioniCorsiDto dto);
	public ValutazioniCorsiDto getById(int id);
	void dtoToDao(ValutazioniCorsiDto dto, ValutazioniCorsiDao dao);
	public void daoToDto(ValutazioniCorsiDao dao, ValutazioniCorsiDto dto);
	
	public List<ValutazioniCorsiDto> findByCorso(Integer numero);
}
