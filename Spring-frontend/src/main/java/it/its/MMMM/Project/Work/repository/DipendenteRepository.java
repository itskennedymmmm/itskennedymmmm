package it.its.MMMM.Project.Work.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.its.MMMM.Project.Work.dao.DipendenteDao;

@Repository
public interface DipendenteRepository extends JpaRepository<DipendenteDao, Integer>{

	@Query("SELECT COUNT(d) FROM DipendenteDao d")
	long getCountDipendenti();

}
