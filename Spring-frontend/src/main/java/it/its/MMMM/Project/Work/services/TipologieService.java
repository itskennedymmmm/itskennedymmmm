package it.its.MMMM.Project.Work.services;

import java.util.List;

import it.its.MMMM.Project.Work.dao.TipologieDao;
import it.its.MMMM.Project.Work.dto.TipologieDto;

public interface TipologieService {
	
	public List<TipologieDto> getAll();
	public List<TipologieDto> getAll(int pagina ,String ordina);
	void dtoToDao(TipologieDto dto, TipologieDao dao);
	void daoToDto(TipologieDao dao, TipologieDto dto);
	public Boolean delete(int id);
	public Boolean modify(TipologieDto dto, int id);
	public Boolean insert(TipologieDto dto);
	public TipologieDto getById(int id);

}
