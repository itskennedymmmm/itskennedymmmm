/* -> Sviluppatori : Mirco Pastorino - Mattia Zogno - Matteo Solivo - Marco Fichera <-
 * 
 * La classe MisureController ha lo scopo di esporre i servizi necessari per il nostro applicattivo in relazione alla gestione delle misure. 
 * 
 * Metodi presenti : 
 * getAll            --> ritorna una lista di oggetti misure (gestito con pagination)
 * list              --> ritorna una lista di oggetti misure (senza pagination)
 * readById          --> (getMisuraById)
 * updateById        --> (MODIFICA)
 * insert            --> (SAVE)
 * deleteById        --> (DELETE)
 *  
 *  */
package it.its.MMMM.Project.Work.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.MMMM.Project.Work.dao.MisureDao;
import it.its.MMMM.Project.Work.dto.BaseResponseDto;
import it.its.MMMM.Project.Work.dto.MisureDto;
import it.its.MMMM.Project.Work.services.MisureService;

@RestController
@RequestMapping(value = "project/misure")
public class MisureController {

	@Autowired
	MisureService misureService;

	// GET Pagination
	@GetMapping(produces = "application/json", path = "/page/{pagina}")
	public BaseResponseDto<List<MisureDto>> getAllDocenti(@PathVariable int pagina,
			@RequestParam(required = false) String ordina) {
		BaseResponseDto<List<MisureDto>> response = new BaseResponseDto<>();

		List<MisureDto> tipi;

		if (ordina == null) {
			ordina = "misura_id";
		}

		tipi = misureService.getAll(pagina, ordina);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		response.setResponse(tipi);
		return response;
	}

	// GET
	@GetMapping(value = "/list", produces = "application/json")
	public BaseResponseDto<List<MisureDto>> list() {

		List<MisureDto> corsi = misureService.getAll();
		BaseResponseDto<List<MisureDto>> res = new BaseResponseDto<List<MisureDto>>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("ESEGUITO");
		res.setResponse(corsi);

		return res;
	}

	// GET BY ID
	@GetMapping(produces = "application/json", path = "/{id}")
	public BaseResponseDto<MisureDao> readById(@PathVariable int id) {
		BaseResponseDto<MisureDao> response = new BaseResponseDto<>();

		MisureDto Misure = misureService.getById(id);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(Misure);

		return response;
	}

	// MODIFICA
	@PatchMapping(consumes = "application/json", produces = "application/json", path = "/update/{id}")
	public BaseResponseDto<Boolean> updateById(@RequestBody MisureDto corsoDto, @PathVariable int id) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value());
		risposta.setMessage("PATCH_SUCCESFUL");
		risposta.setResponse(misureService.modify(corsoDto, id));

		return risposta;
	}

	// INSERT
	@PostMapping(consumes = "application/json", produces = "application/json", path = "/save") // input = consumes +
																								// output = produces
	public BaseResponseDto<Boolean> inserisci(@RequestBody MisureDto dto) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("POST_SUCCESFUL");
		risposta.setResponse(misureService.insert(dto));

		return risposta;
	}

	// DELETE
	@DeleteMapping(produces = "application/json", path = "/delete/{id}")
	public BaseResponseDto<Boolean> deleteById(@PathVariable int id) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("DELETE_SUCCESFUL");
		risposta.setResponse(misureService.delete(id));

		return risposta;
	}

}
