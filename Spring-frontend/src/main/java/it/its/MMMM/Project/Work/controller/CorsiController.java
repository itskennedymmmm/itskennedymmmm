/* -> Sviluppatori : Mirco Pastorino - Mattia Zogno - Matteo Solivo - Marco Fichera <-
 * 
 * La classe CorsiController ha lo scopo di esporre i servizi necessari per il nostro applicattivo in relazione alla gestione dei corsi. 
 * 
 * Metodi presenti : 
 * getAll            --> ritorna una lista di oggetti corsi (gestito con pagination)
 * list              --> ritorna una lista di oggetti corsi (senza pagination)
 * readById          --> (getCorsoById)
 * updateById        --> (MODIFICA)
 * insert            --> (SAVE)
 * deleteById        --> (DELETE)
 * getCount(idCorso) --> ritorna il numero di corsi totali 
 *  
 *  */

package it.its.MMMM.Project.Work.controller;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.MMMM.Project.Work.dao.CorsiDao;
import it.its.MMMM.Project.Work.dto.BaseResponseDto;
import it.its.MMMM.Project.Work.dto.CorsiDto;
import it.its.MMMM.Project.Work.services.CorsiService;

@RestController
@RequestMapping(value = "project/corsi")
public class CorsiController {

	@Autowired
	CorsiService corsiService;

	// GET Pagination
	@GetMapping(produces = "application/json", path = "/page/{pagina}")
	public BaseResponseDto<List<CorsiDto>> getAll(@PathVariable int pagina,
			@RequestParam(required = false) String ordina) {
		BaseResponseDto<List<CorsiDto>> response = new BaseResponseDto<>();

		List<CorsiDto> corsi;

		if (ordina == null) {
			ordina = "docente_id";
		}

		corsi = corsiService.getAll(pagina, ordina);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

		response.setResponse(corsi);
		return response;
	}

	// GET
	@GetMapping(value = "/list", produces = "application/json")
	public BaseResponseDto<List<CorsiDto>> list() {

		List<CorsiDto> corsi = corsiService.getAll();
		BaseResponseDto<List<CorsiDto>> res = new BaseResponseDto<List<CorsiDto>>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("ESEGUITO");
		res.setResponse(corsi);

		return res;
	}

	// GET BY ID
	@GetMapping(produces = "application/json", path = "/{idC}")
	public BaseResponseDto<CorsiDao> readById(@PathVariable int idC) {
		BaseResponseDto<CorsiDao> response = new BaseResponseDto<>();

		CorsiDto corso = corsiService.getCorsoById(idC);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corso);

		return response;
	}

	// MODIFICA
	@PatchMapping(consumes = "application/json", produces = "application/json", path = "/update/{idC}")
	public BaseResponseDto<Boolean> updateById(@RequestBody CorsiDto corsoDto, @PathVariable int idC) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value());
		risposta.setMessage("PATCH_SUCCESFUL");
		risposta.setResponse(corsiService.modifyCorsi(corsoDto, idC));

		return risposta;
	}

	// INSERT
	@PostMapping(consumes = "application/json", produces = "application/json", path = "/save")
	public BaseResponseDto<Boolean> insert(@RequestBody CorsiDto dto) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("POST_SUCCESFUL");
		risposta.setResponse(corsiService.insertCorsi(dto));

		return risposta;
	}

	// DELETE
	@DeleteMapping(produces = "application/json", path = "/delete/{idC}")
	public BaseResponseDto<Boolean> deleteById(@PathVariable int idC) {

		BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
		risposta.setStatus(HttpStatus.OK.value()); // 200
		risposta.setMessage("DELETE_SUCCESFUL");
		risposta.setResponse(corsiService.deleteCorsi(idC));

		return risposta;
	}

	// GET COUNT CORSI
	@GetMapping(value = "/getCount", produces = "application/json")
	public BaseResponseDto<Long> getCount() {
		long dipendenti = corsiService.getCountCorsi();

		BaseResponseDto<Long> res = new BaseResponseDto<Long>();

		res.setTimestamp(new Date());
		res.setStatus(HttpStatus.OK.value());
		res.setMessage("ESEGUITO");
		res.setResponse(dipendenti);

		return res;
	}

}
