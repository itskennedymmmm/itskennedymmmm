/* -> Sviluppatori : Mirco Pastorino - Mattia Zogno - Matteo Solivo - Marco Fichera <-
 * 
 * La classe TipologieController ha lo scopo di esporre i servizi necessari per il nostro applicattivo in relazione alla gestione delle tipologie. 
 * 
 * Metodi presenti : 
 * getAll            --> ritorna una lista di oggetti tipologia (gestito con pagination)
 * list              --> ritorna una lista di oggetti tipologia (senza pagination)
 * readById          --> (getTipologiaById)
 * updateById        --> (MODIFICA)
 * insert            --> (SAVE)
 * deleteById        --> (DELETE)
 *  
 *  */
package it.its.MMMM.Project.Work.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.MMMM.Project.Work.dao.TipologieDao;
import it.its.MMMM.Project.Work.dto.BaseResponseDto;
import it.its.MMMM.Project.Work.dto.TipologieDto;
import it.its.MMMM.Project.Work.services.TipologieService;

@RestController
@RequestMapping(value = "project/tipologie")
public class TipologieController {
	
	@Autowired
	TipologieService tipologieService;
	
	// GET Pagination 
			 @GetMapping(produces = "application/json",path = "/page/{pagina}")
			    public BaseResponseDto<List<TipologieDto>> getAllDocenti(
			    		@PathVariable int pagina, @RequestParam(required = false) String ordina)
			    {
					BaseResponseDto<List<TipologieDto>> response = new BaseResponseDto<>();

					List<TipologieDto> tipi;

						if(ordina == null) {
							ordina = "tipologia_id";
						}
			
					tipi = tipologieService.getAll(pagina, ordina);

					response.setTimestamp(new Date());
					response.setStatus(HttpStatus.OK.value());
					response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");

					
					response.setResponse(tipi);
					return response;
			    }
			 
			  //  GET
				@GetMapping(value = "/list", produces = "application/json")
				public BaseResponseDto<List<TipologieDto>> list() { 
					
					List<TipologieDto> corsi = tipologieService.getAll();
					BaseResponseDto<List<TipologieDto>> res = new BaseResponseDto<List<TipologieDto>>();
					
					res.setTimestamp(new Date());
					res.setStatus(HttpStatus.OK.value());
					res.setMessage("ESEGUITO");
					res.setResponse(corsi);
					
					return res;
				}
				
				  // GET BY ID
				@GetMapping(produces = "application/json", path="/{idT}")
				public BaseResponseDto<TipologieDao> readById(@PathVariable int idT) {
					BaseResponseDto<TipologieDao> response = new BaseResponseDto<>();
					
					TipologieDto Tipologie = tipologieService.getById(idT);
					
					response.setTimestamp(new Date());
					response.setStatus(HttpStatus.OK.value());
					response.setMessage("SERVIZIO_ELABORATO_CORRETTAMENTE");
					response.setResponse(Tipologie);
					
					return response;
				}
		
				//	MODIFICA
				@PatchMapping(consumes = "application/json", produces = "application/json", path = "/update/{idT}")
				public BaseResponseDto<Boolean> updateById(@RequestBody TipologieDto corsoDto, @PathVariable int idT) {

					BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
					risposta.setStatus(HttpStatus.OK.value());
					risposta.setMessage("PATCH_SUCCESFUL");
					risposta.setResponse(tipologieService.modify(corsoDto, idT));

					return risposta;
				}
				
				//	INSERT
				@PostMapping(consumes = "application/json", produces = "application/json", path = "/save") // input = consumes + output = produces
				public BaseResponseDto<Boolean> inserisci(@RequestBody TipologieDto dto) {

					BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
					risposta.setStatus(HttpStatus.OK.value()); //200
					risposta.setMessage("POST_SUCCESFUL");
					risposta.setResponse(tipologieService.insert(dto));

					return risposta;
				}
				
				// DELETE
				@DeleteMapping(produces = "application/json", path = "/delete/{idT}")
				public BaseResponseDto<Boolean> deleteById(@PathVariable int idT) {

					BaseResponseDto<Boolean> risposta = new BaseResponseDto<>();
					risposta.setStatus(HttpStatus.OK.value()); //200
					risposta.setMessage("DELETE_SUCCESFUL");
					risposta.setResponse(tipologieService.delete(idT));

					return risposta;
				}

}
