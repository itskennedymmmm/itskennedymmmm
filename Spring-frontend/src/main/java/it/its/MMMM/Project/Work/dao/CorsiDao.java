package it.its.MMMM.Project.Work.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table (name = "corsi") //Nome tabella
@Data
public class CorsiDao {
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY) //ci deve andare
	 @Column (name = "corso_id") 
	 private Integer corso_id;
	 
	 @Column (name = "tipo")
	 private Integer tipo;
	 
	 @Column (name = "tipologia")
	 private Integer tipologia;
	 
	 @Column (name = "misura")
	 private Integer misura;
	 
	 @Column (name = "descrizione")
	 private String descrizione;
	 
	 @Column (name = "edizione")
	 private String edizione;
	 
	 @Column (name = "previsto")
	 private String previsto;
	 
	 @Column (name = "erogato")
	 private String erogato;
	 
	 @Column (name = "durata")
	 private String durata;
	 
	 @Column (name = "note")
	 private String note;
	 
	 @Column (name = "luogo")
	 private String luogo;
	 
	 @Column (name = "ente")
	 private String ente;
	 
	 @Column (name = "data_erogazione")
	 private Date data_erogazione;
	 
	 @Column (name = "data_chiusura")
	 private Date data_chiusura;
	 
	 @Column (name = "data_censimento")
	 private Date data_censimento;
	 
	 @Column (name = "interno")
	 private Integer interno;
	 
/* Join tabella TIPI	*/	 
	 @ManyToOne
	 @EqualsAndHashCode.Exclude
	 @JoinColumn(name = "tipo", referencedColumnName = "tipo_id", insertable = false, updatable = false)
	 @JsonBackReference
	 private TipoDao tipoDao;

/* Join tabella TIPOLOGIE	*/	 
	 @ManyToOne
	 @EqualsAndHashCode.Exclude
	 @JoinColumn(name = "tipologia", referencedColumnName = "tipologia_id", insertable = false, updatable = false)
	 @JsonBackReference
	 private TipologieDao tipologiaDao;

/* Join tabella MISURE	*/
	 @ManyToOne
	 @EqualsAndHashCode.Exclude
	 @JoinColumn(name = "misura", referencedColumnName = "misura_id", insertable = false, updatable = false)
	 @JsonBackReference
	 private MisureDao misuraDao;

/* Join tabella CORSI_DOCENTI	*/	 
	 @OneToMany(targetEntity=CorsiDocentiDao.class, mappedBy="corsiDao",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	 private List<CorsiDocentiDao> corsiDocenti = new ArrayList<>();
	 
/* Join tabella CORSI_UTENTI	*/	 
	 @OneToMany(targetEntity=CorsiUtentiDao.class, mappedBy="corsiDao",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	 private List<CorsiUtentiDao> corsiUtenti = new ArrayList<>();
	 
/* Join tabella VALUTAZIONI_UTENTI	*/	 
	 @OneToMany(targetEntity=ValutazioniDocentiDao.class, mappedBy="corsiDao",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	 private List<ValutazioniDocentiDao> valutazioniDocenti = new ArrayList<>();
	 
/* Join tabella VALUTAZIONI_CORSI	*/
	 @OneToMany(targetEntity=ValutazioniCorsiDao.class, mappedBy="corsiDao",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	 private List<ValutazioniCorsiDao> valutazioniCorsi = new ArrayList<>();
	 
/* Join tabella VALUTAZIONI_UTENTI	*/
	 @OneToMany(targetEntity=ValutazioniUtentiDao.class, mappedBy="corsiDao",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	 private List<ValutazioniUtentiDao> valutazioniUtenti = new ArrayList<>();

	 
	 
}
