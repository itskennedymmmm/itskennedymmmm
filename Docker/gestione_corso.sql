﻿CREATE DATABASE IF NOT EXISTS gestione_corso;
USE gestione_corso;

CREATE TABLE societa (
  societa_id integer AUTO_INCREMENT PRIMARY KEY,
  ragione_sociale varchar(50) not null,
  indirizzo varchar(50) not null,
  localita varchar(50) not null,
  provincia varchar(50) not null,
  nazione varchar(50) not null,
  telefono varchar(50) not null,
  fax varchar(50) not null,
  partita_iva varchar(50) not null
);

CREATE TABLE dipendenti (
  dipendenti_id integer AUTO_INCREMENT PRIMARY KEY,
  matricola integer,
  societa integer not null,
  cognome varchar(50) not null,
  nome varchar(50) not null,
  sesso varchar(50),
  data_nascita date not null,
  luogo_nascita varchar(50) not null,
  stato_civile varchar(50) not null,
  titolo_studio varchar(50) not null,
  conseguito_presso varchar(50),
  anno_conseguimento integer,
  tipo_dipendente varchar(50),
  qualifica varchar(50),
  livello varchar(50),
  data_assunzione date,
  responsabile_risorsa integer,
  responsabile_area integer,
  data_fine_rapporto date,
  data_scadenza_contratto date,
  FOREIGN KEY (societa) REFERENCES societa(societa_id)
);

CREATE TABLE tipi (
  tipo_id integer AUTO_INCREMENT PRIMARY KEY,
  descrizione varchar(50) not null
);

CREATE TABLE tipologie (
  tipologia_id integer AUTO_INCREMENT PRIMARY KEY,
  descrizione varchar(50) not null
);

CREATE TABLE misure (
  misura_id integer AUTO_INCREMENT PRIMARY KEY,
  descrizione varchar(50) not null
);

CREATE TABLE corsi (
  corso_id integer AUTO_INCREMENT PRIMARY KEY,
  tipo integer not null,
  tipologia integer not null,
  misura integer not null,
  descrizione varchar(50),
  edizione varchar(50),
  previsto enum('Si','No') default 'No',
  erogato enum('Si','No') default 'No',
  durata varchar(50) ,
  note varchar(100),
  luogo varchar(50),
  ente varchar(50),
  data_erogazione date,
  data_chiusura date,
  data_censimento date,
  interno int,
  FOREIGN KEY (tipo) REFERENCES tipi(tipo_id) on update cascade on delete cascade,
  FOREIGN KEY (tipologia) REFERENCES tipologie(tipologia_id) on update cascade on delete cascade,
  FOREIGN KEY (misura) REFERENCES misure(misura_id) on update cascade on delete cascade
);

CREATE TABLE corsi_docenti (
  corsi_docenti_id integer AUTO_INCREMENT PRIMARY KEY,
  corso integer not null,
  docente int,
  interno int,
  FOREIGN KEY (corso) REFERENCES corsi(corso_id)

);

CREATE TABLE corsi_utenti (
  corsi_utenti_id integer AUTO_INCREMENT PRIMARY KEY,
  corso integer not null,
  dipendente integer not null,
  durata int,
  FOREIGN KEY (corso) REFERENCES corsi(corso_id),
  FOREIGN KEY (dipendente) REFERENCES dipendenti(dipendenti_id)
);

CREATE TABLE valutazioni_utenti (
  valutazioni_utenti_id integer AUTO_INCREMENT PRIMARY KEY,
  corso integer not null,
  dipendente integer not null,
  criterio varchar(100) not null,
  valore varchar(100) not null,
  FOREIGN KEY (corso) REFERENCES corsi(corso_id),
  FOREIGN KEY (dipendente) REFERENCES dipendenti(dipendenti_id)
);

CREATE TABLE docenti (
  docente_id integer AUTO_INCREMENT PRIMARY KEY,
  titolo varchar(50) not null,
  ragione_sociale varchar(50) not null,
  indirizzo varchar(50) not null,
  localita varchar(50) not null,
  provincia varchar(50),
  nazione varchar(50),
  telefono varchar(50),
  fax varchar(50),
  partita_iva varchar(50),
  referente varchar(50)
);

CREATE TABLE valutazioni_docenti (
  valutazioni_docenti_id int AUTO_INCREMENT PRIMARY KEY,
  corso integer not null,
  docente integer not null,
  criterio varchar(100) not null,
  valore json not null,
  FOREIGN KEY (corso) REFERENCES corsi(corso_id),
  FOREIGN KEY (docente) REFERENCES docenti(docente_id)
);

CREATE TABLE valutazioni_corsi (
  valutazioni_corsi_id integer AUTO_INCREMENT PRIMARY KEY,
  corso integer not null,
  criterio varchar(100) not null,
  valore json not null,
  FOREIGN KEY (corso) REFERENCES corsi(corso_id)
);


INSERT INTO societa VALUES (null, "Corvallis", "Via Savelli", "Padova", "PD", "Italia", "0498434511", "0498434555", "02070900283");
INSERT INTO societa VALUES (null, "Google", "Via Google 42st", "Cupertino", "CA", "USA", "0224643552", "0564834611", "335349067283");
INSERT INTO societa VALUES (null, "Ibs", "Via Ibs", "Assago", "MI", "Italia", "45261214311", "052334611", "0201232123903");
INSERT INTO societa VALUES (null, "IBM", "Via IBM", "Broome", "PA", "USa", "049833377711", "05213613324", "02053453077");
INSERT INTO societa VALUES (null, "Nvidia", "Via Nvidia", "Santa Clara", "CA", "USA", "049823546511", "05644744322", "0200322131423");

INSERT INTO dipendenti VALUES (null, 001, 1, "Rossi", "Mario", "Maschio", '1990-01-04', "Rho", "Celibe", "ingegnere informatico", "UNIBOCC", 2017, "Tecnico", "Quadro", "2", '2018-04-26', 1, 2, '2024-03-10', '2024-03-26');
INSERT INTO dipendenti VALUES (null, 002, 2, "Blu", "Luca", "Maschio", '1995-08-09', "Bologna", "Celibe", "ingegnere elettronico", "UNIBO", 2019, "Commercialista", "Quadro", "2", '2020-02-10', 2, 3, '2026-02-09', '2026-02-17');
INSERT INTO dipendenti VALUES (null, 003, 3, "Verdi", "Giovanna", "Femmina", '1986-06-17', "Bergamo", "Nubile", "ingegnere aerospaziale", "UNIMI", 2011, "Responsabile", "Impiegata", "3", '2013-10-14', 2, 1, '2021-12-20', '2022-01-15');
INSERT INTO dipendenti VALUES (null, 004, 4, "Gialli", "Marco", "Maschio", '1965-04-10', "Vicenza", "Celibe", "avvocato", "UNIVR", 1997, "Manager", "Dirigente", "1", '2000-09-28', 1, 4, '2020-01-15', '2020-01-24');
INSERT INTO dipendenti VALUES (null, 005, 5, "Arancioni", "Martina", "Femmina", '1985-11-26', "Terni", "Nubile", "infermiere", "UNICATT", 2012, "Spazzino", "Operaio", "6", '2013-05-30', 3, 4, '2023-03-26', '2023-04-07');
INSERT INTO dipendenti VALUES (null, 006, 5, "Speranza", "Diego", "Maschio", '1991-12-30', "Lecce", "Celibe", "ingegnere elettrofisico", "UNIBO", 2007, "Tecnico", "Operaio", "2", '2013-05-22', 3, 4, '2023-03-16', '2023-04-10');
INSERT INTO dipendenti VALUES (null, 007, 4, "Rossi", "Martina", "Femmina", '1990-04-04', "Napoli", "Nubile", "chirurgo", "UNIUR", 2017, "Manager", "Dirigente", "6", '2018-05-30', 1, 2, '2023-08-26', '2023-10-07');
INSERT INTO dipendenti VALUES (null, 008, 3, "Arancioni", "Peppa", "Femmina", '1987-01-17', "Scampia", "Nubile", "tuttofare", "UNILE", 2015, "Commercialista", "Operaio", "6", '2015-07-18', 4, 3, '2021-09-10', '2021-11-10');
INSERT INTO dipendenti VALUES (null, 009, 2, "Grigio", "Lucrezia", "Femmina", '1983-09-11', "Busto Arsizio", "Nubile", "perito meccanico", "UCSC", 2005, "Responsabile", "Operaio", "6", '2005-05-21', 1, 1, '2020-04-12', '2020-05-10');
INSERT INTO dipendenti VALUES (null, 010, 1, "Celeste", "Celestina", "Femmina", '1989-07-12', "Secondigliano", "Nubile", "ingegnere nucleare", "UNICT", 2014, "Spazzino", "Operaio", "6", '2014-07-30', 2, 3, '2021-06-22', '2021-07-11');
INSERT INTO dipendenti VALUES (null, 009, 2, "Sorge", "Francesco", "Maschio", '1983-09-11', "Padova", "Nubile", "Agente", "UCSC", 2005, "Responsabile", "master", "6", '2005-05-21', 1, 1, '2020-04-12', '2020-05-10');
INSERT INTO dipendenti VALUES (null, 010, 1, "Zogno", "Mattia", "Maschio", '1989-07-12', "Abano Terme", "Sposato", "ingegnere nucleare", "UNICT", 2014, "Manager", "Dirigente", "6", '2014-07-30', 2, 3, '2021-06-22', '2021-07-11');
INSERT INTO dipendenti VALUES (null, 009, 2, "Zogno", "Stefano", "Maschio", '1983-09-11', "Abano Terme", "Sposato", "perito meccanico", "UCSC", 2005, "Responsabile", "Dirigente", "6", '2005-05-21', 1, 1, '2020-04-12', '2020-05-10');
INSERT INTO dipendenti VALUES (null, 010, 1, "Cella", "Bluetta", "Femmina", '1989-07-12', "Secondigliano", "Nubile", "ingegnere nucleare", "UNICT", 2014, "Spazzino", "Operaio", "6", '2014-07-30', 2, 3, '2021-06-22', '2021-07-11');
INSERT INTO dipendenti VALUES (null, 009, 2, "Marco", "Marchetto", "Maschio", '1983-09-11', "Busto Arsizio", "Nubile", "perito meccanico", "UCSC", 2005, "Responsabile", "Operaio", "6", '2005-05-21', 1, 1, '2020-04-12', '2020-05-10');
INSERT INTO dipendenti VALUES (null, 010, 1, "Phil", "Colson", "Maschio", '1989-07-12', "Secondigliano", "Nubile", "ingegnere nucleare", "UNICT", 2014, "Spazzino", "Operaio", "6", '2014-07-30', 2, 3, '2021-06-22', '2021-07-11');
INSERT INTO dipendenti VALUES (null, 009, 2, "Lana", "Del Ray", "Femmina", '1983-09-11', "Busto Arsizio", "Nubile", "perito meccanico", "UCSC", 2005, "Responsabile", "Operaio", "6", '2005-05-21', 1, 1, '2020-04-12', '2020-05-10');
INSERT INTO dipendenti VALUES (null, 010, 1, "Ayeye", "Brazorf", "Femmina", '1989-07-12', "Secondigliano", "Nubile", "ingegnere nucleare", "UNICT", 2014, "Spazzino", "Operaio", "6", '2014-07-30', 2, 3, '2021-06-22', '2021-07-11');
INSERT INTO dipendenti VALUES (null, 009, 2, "Steve", "Bobby", "Maschio", '1983-09-11', "Busto Arsizio", "Nubile", "perito meccanico", "UCSC", 2005, "Responsabile", "Operaio", "6", '2005-05-21', 1, 1, '2020-04-12', '2020-05-10');
INSERT INTO dipendenti VALUES (null, 010, 1, "Cocco", "Bianco", "Maschio", '1989-07-12', "Secondigliano", "Nubile", "ingegnere nucleare", "UNICT", 2014, "Spazzino", "Operaio", "6", '2014-07-30', 2, 3, '2021-06-22', '2021-07-11');



INSERT INTO tipi (tipo_id, descrizione) VALUES
(1,'Java'),
(2,'Internet'),
(3,'Client-Server'),
(4,'Programmazione Mainframe'),
(5,'RPG'),
(6,'Cobol'),
(7,'Programmazione di Sistemi EDP'),
(8,'Object-Oriented'),
(9,'Sistema di Gestione Qualità'),
(10,'Privacy'),
(11,'Analisi e Programmazione'),
(12,'Programmazione PC'),
(13,'Project Management'),
(14,'Programmazione'),
(15,'CICS'),
(16,'DB2'),
(17,'Linguaggio C'),
(18,'Data Base'),
(19,'Introduzione all''Informatica'),
(20,'Office'),
(21,'JCL'),
(22,'Marketing'),
(23,'Oracle'),
(24,'Formazione Sicurezza (626/94; 81/2008)'),
(25,'Inglese'),
(26,'Amministrazione/Finanza'),
(27,'Windows'),
(28,'Word'),
(29,'Office'),
(30,'Excel'),
(31,'Easytrieve'),
(32,'Comunicazione'),
(33,'Access'),
(34,'Formazione per Formatori'),
(35,'Tecnica Bancaria'),
(36,'Lotus Notes'),
(37,'Visualage'),
(38,'Websphere'),
(39,'Reti/Elettronica'),
(40,'SAS'),
(41,'UML'),
(42,'ViaSoft'),
(43,'Visual Basic'),
(44,'Sistemi Informativi'),
(45,'Cartografia Vettoriale'),
(46,'Cartografia Raster'),
(47,'Gestione Elettronica Documenti-Modulistica'),
(48,'Territorio e Ambiente'),
(49,'Sistemi Informativi Territoriali'),
(50,'Editoria'),
(51,'VARIE'),
(52,'AS/400'),
(53,'Gestione Progetti'),
(55,'Organizzazione'),
(56,'Funzionale Normativo'),
(57,'Funzionale'),
(58,'Manageriale'),
(60,'Metodologia'),
(61,'Prodotti'),
(62,'Business Object'),
(63,'Sistemi di Business Intelligence'),
(64,'Formazione Apprendisti'),
(66,'Primavera'),
(67,'XML'),
(68,'Cattolica – Servizi in ambito Sinistri'),
(69,'Ambito Assicurativo'),
(70,'Gestione della Sicurezza delle Informazioni (SGSI)'),
(71,'PHP'),
(72,'CMS/E-commerce'),
(73,'Microsoft Azure'),
(75,'Eurotech IoT'),
(79,'Infrastrutture'),
(80,'Infrastrutture'),
(81,'Android'),
(82,'SQL'),
(83,'Risorse Umane'),
(74,'Middleware - Sistemi Distribuiti'),
(76,'Blockchain'),
(77,'Angular'),
(78,'Soft Skill');

INSERT INTO tipologie (tipologia_id, descrizione) VALUES
(1,'Ingresso'),
(2,'Aggiornamento'),
(3,'Seminario');

INSERT INTO misure (misura_id, descrizione) VALUES
(1,'Ore'),
(2,'Giorni');

INSERT INTO corsi VALUES (null, 1, 1, 1, "corso di java", "prima edizione", 'si', 'si', "30 ore", "uso di tomcat", "Padova", "Corvallis", '2020-04-20', '2020-04-27', '2020-04-30', 1);
INSERT INTO corsi VALUES (null, 2, 2, 2, "corso di internet", "prima edizione", 'no', 'no', "7 giorni", "spiegazione generale", "Vicenza", "Forema", '2020-01-12', '2020-10-12', '2020-10-20', 2);
INSERT INTO corsi VALUES (null, 3, 3, 2, "corso di client-server", "terza edizione", 'si', 'no', "15 giorni", "pila iso-osi", "Verona", "Accademia informatica", '2020-01-26', '2020-04-26', '2020-04-30', 3);
INSERT INTO corsi VALUES (null, 4, 1, 2, "corso di programmazione mainframe", "seconda edizione", 'si', 'si', "15 giorni", "sistemi operativi e server distribuiti", "Belluno", "Scuola Design", '2020-02-21', '2020-03-21', '2020-03-24', 4);
INSERT INTO corsi VALUES (null, 5, 2, 1, "corso di RPG", "sesta edizione", 'si', 'si', "20 ore", "file specification", "Trieste", "Oracle", '2020-05-04', '2020-10-04', '2020-10-11', 5);
INSERT INTO corsi VALUES (null, 6, 2, 2, "corso di Cobol", "seconda edizione", 'si', 'no', "50 giorni", "divisione dei sorgenti", "Bolzano", "Tech lab", '2020-07-04', '2020-09-10', '2020-09-16', 6);
INSERT INTO corsi VALUES (null, 7, 1, 1, "corso di Programmazione di Sistemi EDP", "terza edizione", 'no', 'no', "40 ore", "trattamento informazioni", "Bologna", "Silver Academy", '2020-03-15', '2020-04-04', '2020-04-07', 7);
INSERT INTO corsi VALUES (null, 8, 2, 2, "corso di Object-Oriented", "prima edizione", 'si', 'si', "20 giorni", "ereditarieta e polimorfismo", "Roma", "Abaco srl", '2020-10-17', '2020-10-07', '2020-10-11', 8);
INSERT INTO corsi VALUES (null, 9, 2, 1, "corso di Gestione Qualità", "seconda edizione", 'si', 'no', "30 ore", "documentazione e conformita", "Cosenza", "Spot agency", '2020-06-08', '2020-06-26', '2020-06-30', 9);
INSERT INTO corsi VALUES (null, 10, 3, 1, "corso di AWS", "settima edizione", 'no', 'no', "50 ore", "Servizi Disponibili", "Lecce", "Web Global Solution", '2020-03-20', '2020-04-15', '2020-04-20', 6);
INSERT INTO corsi VALUES (null, 11, 2, 1, "corso di C", "settima edizione", 'no', 'no', "50 ore", "Puntatori", "Lecce", "Web Global Solution", '2020-03-20', '2020-04-15', '2020-04-20', 7);
INSERT INTO corsi VALUES (null, 12, 3, 2, "corso di C++", "settima edizione", 'no', 'no', "50 ore", "Gestione Puntatori", "Lecce", "Web Global Solution", '2020-03-20', '2020-04-15', '2020-04-20', 10);

INSERT INTO corsi_docenti VALUES (null, 1, 1, null);
INSERT INTO corsi_docenti VALUES (null, 2, null, 2);
INSERT INTO corsi_docenti VALUES (null, 3, 3, null);
INSERT INTO corsi_docenti VALUES (null, 4, null, 4);
INSERT INTO corsi_docenti VALUES (null, 5, 5, null);
INSERT INTO corsi_docenti VALUES (null, 6, null, 6);
INSERT INTO corsi_docenti VALUES (null, 7, 7, null);
INSERT INTO corsi_docenti VALUES (null, 8, null, 8);
INSERT INTO corsi_docenti VALUES (null, 9, 9, null);
INSERT INTO corsi_docenti VALUES (null, 10, null, 10);
INSERT INTO corsi_docenti VALUES (null, 11, 11, null);
INSERT INTO corsi_docenti VALUES (null, 12, null, 12);

INSERT INTO corsi_utenti VALUES (null, 1, 1, 1);
INSERT INTO corsi_utenti VALUES (null, 2, 2, 2);
INSERT INTO corsi_utenti VALUES (null, 3, 4, 1);
INSERT INTO corsi_utenti VALUES (null, 4, 5, 2);
INSERT INTO corsi_utenti VALUES (null, 5, 6, 1);
INSERT INTO corsi_utenti VALUES (null, 6, 7, 2);
INSERT INTO corsi_utenti VALUES (null, 7, 8, 1);
INSERT INTO corsi_utenti VALUES (null, 8, 9, 2);
INSERT INTO corsi_utenti VALUES (null, 9, 8, 1);
INSERT INTO corsi_utenti VALUES (null, 10, 9, 2);
INSERT INTO corsi_utenti VALUES (null, 11, 8, 1);
INSERT INTO corsi_utenti VALUES (null, 12, 9, 2);





INSERT INTO docenti VALUES (null, "Professore", "Marco Bianchi", "Via Giuseppe Mazzini 4", "Padova", "PD", "Italia", "049224789", "04925551", "IT 11359591002", "SAS");
INSERT INTO docenti VALUES (null, "Ingegnere", "Luca Blu", "Via Brombeis 18", "Napoli", "NA", "Italia", "081224788", "081254217", "IT 1154811002", "SAS");
INSERT INTO docenti VALUES (null, "Dottore", "Giovanna Bianchi", "Via Giacomo Leopardi 3", "Padova", "PD", "Italia", "049247126", "04927482", "IT 1174856352", "SAS");
INSERT INTO docenti VALUES (null, "Avvocato", "Roberto Grigio", "Via Fittizzia 8", "Milano", "MI", "Italia", "04912587", "04915454", "IT 1135959272", "SAS");
INSERT INTO docenti VALUES (null, "Dottore", "Elena Ricci", "Via Boscoverde 26", "Padova", "PD", "Italia", "049254871", "04941232", "IT 1135471395", "SAS");
INSERT INTO docenti VALUES (null, "Professoressa", "Martina Ricciola", "Via cal del gua 30", "Verona", "VR", "Italia", "049555871", "04945418232", "IT 17120071395", "SAS");
INSERT INTO docenti VALUES (null, "Dottoressa", "Marta Martina", "Via dei frati 80", "Padova", "PD", "Italia", "0492543371", "0494134332", "IT 113732431395", "SAS");
INSERT INTO docenti VALUES (null, "Avvocato", "Marco Campagnolo", "Via gualdi", "Vicenza", "VI", "Italia", "04925433871", "049412314232", "IT 1453465471395", "SAS");
INSERT INTO docenti VALUES (null, "Ingegnere", "Ciccio Pasticcio", "Via ignota 1", "Padova", "PD", "Italia", "04567254871", "049443321232", "IT 1135456471395", "SAS");
INSERT INTO docenti VALUES (null, "Dottore", "Giovanni Peppino", "Via sconosciuta 55", "Bologna", "BO", "Italia", "049251234871", "04941123232", "IT 113354371395", "SAS");
INSERT INTO docenti VALUES (null, "Cuoco", "Mattia Zogno", "Via dei frati 80", "Padova", "PD", "Italia", "0492543371", "0494134332", "IT 113732431395", "SAS");
INSERT INTO docenti VALUES (null, "Insegnante", "Stefano Zogno", "Via gualdi", "Vicenza", "VI", "Italia", "04925433871", "049412314232", "IT 1453465471395", "SAS");
INSERT INTO docenti VALUES (null, "Specialista", "Phil Colson", "Via ignota 1", "Rovigo", "RV", "Italia", "04567254871", "049443321232", "IT 1135456471395", "SAS");
INSERT INTO docenti VALUES (null, "Archeologo", "Francesco Agente", "Via sconosciuta 55", "Trento", "TR", "Italia", "049251234871", "04941123232", "IT 113354371395", "SAS");
INSERT INTO docenti VALUES (null, "Geologo", "Paolo Accipicchia", "Via ignota 1", "Venezia", "VE", "Italia", "3489685145", "049443321232", "IT 1135456471395", "SAS");
INSERT INTO docenti VALUES (null, "Vulcanologo", "Vittoria Feltri", "Via sconosciuta 55", "Bologna", "BO", "Italia", "049251234871", "04941123232", "IT 113354371395", "SAS");

INSERT INTO valutazioni_utenti VALUES (null, 1, 1, "Logica", "1");
INSERT INTO valutazioni_utenti VALUES (null, 1, 1, "Impegno", "2");
INSERT INTO valutazioni_utenti VALUES (null, 1, 1, "Velocita'", "3");
INSERT INTO valutazioni_utenti VALUES (null, 1, 1, "Ordine e precisione", "4");
INSERT INTO valutazioni_utenti VALUES (null, 1, 1, "Apprendimento", "5");



INSERT INTO valutazioni_docenti VALUES (null, 1, 1, "Completezza", '{"1": "1", "2": "2" , "3": "2" , "4": "2", "5": "2"}');
INSERT INTO valutazioni_docenti VALUES (null, 1, 1, "Chiarezza Esposizione", '{"1": "1", "2": "2" , "3": "2" , "4": "2", "5": "2"}');
INSERT INTO valutazioni_docenti VALUES (null, 1, 1, "Disponibilita'", '{"1": "1", "2": "2" , "3": "2" , "4": "2", "5": "2"}');



INSERT INTO valutazioni_corsi VALUES (null, 1, "Utilità per la crescita personale", '{"1": "1", "2": "2" , "3": "2" , "4": "2", "5": "2"}');
INSERT INTO valutazioni_corsi VALUES (null, 1, "Utilità del corso nell'ambito lavorativo attuale", '{"1": "1", "2": "2" , "3": "2" , "4": "2", "5": "2"}');
INSERT INTO valutazioni_corsi VALUES (null, 1, "Scelta degli argomenti specifici", '{"1": "1", "2": "2" , "3": "2" , "4": "2", "5": "2"}');
INSERT INTO valutazioni_corsi VALUES (null, 1, "Livello di approfondimento degli argomenti", '{"1": "1", "2": "2" , "3": "2" , "4": "2", "5": "2"}');
INSERT INTO valutazioni_corsi VALUES (null, 1, "Durata del Corso", '{"1": "1", "2": "2" , "3": "2" , "4": "2", "5": "2"}');
INSERT INTO valutazioni_corsi VALUES (null, 1, "Corretta suddivisione tra teoria e pratica", '{"1": "1", "2": "2" , "3": "2" , "4": "2", "5": "2"}');
INSERT INTO valutazioni_corsi VALUES (null, 1, "Efficacia esercitazioni", '{"1": "1", "2": "2" , "3": "2" , "4": "2", "5": "2"}');


ALTER TABLE corsi_docenti
add FOREIGN KEY (corso) REFERENCES corsi(corso_id);


ALTER TABLE corsi_docenti
add FOREIGN KEY (docente) REFERENCES docenti(docente_id);

ALTER TABLE corsi_docenti
add FOREIGN KEY (interno) REFERENCES dipendenti(dipendenti_id);